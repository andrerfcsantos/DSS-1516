   MANUAL DE CANDIDATURA
A PRESIDENTE DA REPÚBLICA




                                     – PR 2016 –




LEI ELEITORAL para o PRESIDENTE DA REPÚBLICA (LEPR)
Decreto-Lei nº 319-A/76, de 3 de maio

Com as alterações introduzidas pelas pelos seguintes diplomas legais: Retificação de 07.06.1976, Declaração de
Retificação de 30.06.1976, Decretos-Leis n.ºs 377-A/76, de 19 de maio, 445-A/76, de 4 de junho, 456-A/76, de 8 de
junho, 472-A/76, de 15 de junho, 472-B/76, de 15 de junho, e 495-A/76, de 24 de junho, Lei n.º 143/85, de 26 de
novembro (Declaração de Retificação de 06.12.1985), Decreto-Lei n.º 55/88, de 26 de fevereiro, Leis n.ºs 31/91, de
20 de julho, 72/93, de 30 de novembro, 11/95, de 22 de abril, 35/95, de 18 de agosto, e 110/97, de 16 de setembro, e
Leis Orgânicas n.ºs 3/2000, de 24 de agosto, 2/2001, de 25 de agosto, 4/2005, de 8 de setembro, 5/2005, de 8 de
setembro, 3/2010, de 15 de dezembro, e 1/2011, de 30 de novembro.
                                                              ÍNDICE


1. INTRODUÇÃO .................................................................................................................... 3
     1.1 Elegibilidade ............................................................................................................. 3
     1.2 Direito à dispensa de funções profissionais ...................................................... 3
     1.3 Obrigatoriedade de suspensão de funções profissionais ................................ 3
     1.4 Imunidade.................................................................................................................. 3
2. ELEITORES ....................................................................................................................... 4
3.      CRITÉRIO DA ELEIÇÃO ....................................................................................................... 4

4. APRESENTAÇÃO DA CANDIDATURA................................................................................... 4
     4.1 Documentos necessários ....................................................................................... 4
     4.2 Elaboração da “Declaração dos proponentes” .................................................. 5
     4.3 Pedido de certidão de eleitor ................................................................................ 6
     4.4 Mandatário e representantes ................................................................................ 7
     4.5 Local e prazo de apresentação das candidaturas ............................................. 7
5. FINANCIAMENTO DA CAMPANHA ELEITORAL E PRESTAÇÃO DE CONTAS ............................ 7
6. MODELOS EXEMPLIFICATIVOS ........................................................................................... 7
     6.1 MODELO exemplificativo ........................................................................................... 8
     DECLARAÇÃO DOS PROPONENTES ....................................................................................... 8
     6.2 MODELO EXEMPLIFICATIVO ........................................................................................... 9
     PEDIDO DE CERTIDÃO DE ELEITOR ........................................................................................ 9




                                                                                                                        Pág. 2 de 9
1. INTRODUÇÃO



1.1 Elegibilidade

São elegíveis para a Presidência da República os cidadãos eleitores portugueses de origem,
maiores de 35 anos
                                                                        (artigo 4.º n.º 1 LEPR)

1.2 Direito à dispensa de funções profissionais

Desde a data da apresentação das candidaturas e até ao dia da eleição, os candidatos
têm direito à dispensa do exercício das respetivas funções profissionais, sejam públicas ou
privadas, contando esse tempo para todos os efeitos, incluindo o direito à remuneração, como
tempo de serviço efetivo.
                                                                        (artigo 6.º n.º 1 LEPR)


1.3 Obrigatoriedade de suspensão de funções profissionais

Os magistrados judiciais ou do Ministério Público em efetividade de serviço, os militares em
funções de comando e os diplomatas chefes de missão suspendem obrigatoriamente o
exercício das respetivas funções, desde a data da apresentação da candidatura até ao dia da
eleição.
                                                                        (artigo 6.º n.º 2 LEPR)


1.4 Imunidade

Nenhum candidato poderá ser sujeito a prisão preventiva, a não ser em caso de flagrante delito
de crime punível com pena superior a três anos.

Movido procedimento criminal contra algum candidato e indiciado este por despacho de
pronúncia ou equivalente, o processo só poderá prosseguir após a proclamação dos resultados
da eleição.
                                                                           (Artigo 24.º LEPR)




                                                                                    Pág. 3 de 9
2. ELEITORES

O Presidente da República é eleito por sufrágio universal, direto e secreto dos cidadãos:

   - Portugueses recenseados no território nacional;

   - Portugueses residentes no estrangeiro que se encontrem recenseados nos cadernos
      eleitorais;

   - Brasileiros residentes e recenseados no território nacional, que possuam o estatuto de
      igualdade de direitos políticos.
                                                                  (artigos 121.º CRP e 1.º LEPR)

Existe um só círculo eleitoral, com sede em Lisboa.
                                                                         (artigos 7.º e 8.º LEPR)



3. CRITÉRIO DA ELEIÇÃO

Será eleito o candidato que obtiver mais de metade dos votos válidos, não se considerando
como tal os votos em branco. Se nenhum dos candidatos obtiver esse número de votos,
proceder-se-á a um segundo sufrágio, ao qual concorrerão apenas os dois candidatos mais
votados.

                                                                              (artigo 10.º LEPR)



4. APRESENTAÇÃO DA CANDIDATURA

4.1 Documentos necessários

A apresentação de candidatura consiste na entrega dos seguintes documentos:

A. Declaração subscrita pelos cidadãos eleitores proponentes (v. 4.2).

B. Documentos que comprovem que o candidato:
 É maior de 35 anos: Certidão do assento de nascimento emitido pela Conservatória do
   Registo Civil (mesmo que não corresponda à Conservatória do Registo Civil da naturalidade
   do candidato);
 É português de origem: Certificado de nacionalidade portuguesa originária emitido pela
   Conservatória dos Registos Centrais;

                                                                                      Pág. 4 de 9
 Goza de todos os seus direitos civis: Certidão negativa do registo de tutela emitida pela
    Conservatória do Registo Civil (mesmo que não corresponda à Conservatória do Registo
    Civil da naturalidade do candidato);
 Goza de todos os seus direitos políticos: Certificado do Registo Criminal, emitido pela
    Direção de Serviços de Identificação Criminal (DSIC) que pertence à Direcção-Geral da
    Administração da Justiça;

 Está inscrito no recenseamento eleitoral: Certidão emitida pela Comissão Recenseadora
    da área da residência do candidato.
C. Declaração do candidato, da qual conste que não está abrangido pelas inelegibilidades
fixadas pelo artigo 5.º da lei Eleitoral do Presidente da República e de que aceita a
candidatura;
D. Declaração de património e rendimentos, feita através de impresso próprio a adquirir
junto da Imprensa Nacional-Casa da Moeda, S.A. ou preenchida online, através do formulário
eletrónico disponível em http://www.incm.pt/eforms/request?M=1649;
E. Declaração do candidato a designar o mandatário e indicar a respetiva morada em Lisboa
(v. 4.4) e, se assim o entender, os representantes distritais e/ou para cada área consular no
estrangeiro;
F. O Tribunal Constitucional poderá ainda solicitar uma cópia do cartão de cidadão (ou
bilhete de identidade) do candidato e do mandatário;
G. Relativamente a cada um dos proponentes, certidão que comprove que estão inscritos
no recenseamento (v. 4.3).
                                           [artigos 15.º e 16.º LEPR e artigo 4.º/2 b) Lei 4/83, 2 abril]




As certidões de eleitor são passadas gratuitamente no prazo de 3 dias pelas comissões
recenseadoras.
                                                                     (artigo 68.º Lei 13/99, 22 março)



4.2 Elaboração da “Declaração dos proponentes”

    As candidaturas só podem ser apresentadas por um mínimo de 7.500 e um máximo de
     15.000 proponentes;



                                                                                              Pág. 5 de 9
   A declaração deve indicar o nome e demais elementos de identificação do candidato
    proposto (idade; número, arquivo de identificação e data do bilhete de identidade / cartão
    de cidadão; filiação; profissão; naturalidade; e residência);

   A declaração deve conter, quanto a cada proponente, o nome completo, número, data e
    entidade emitente do cartão de cidadão/bilhete de identidade ou passaporte; número de
    eleitor e respetiva freguesia; e a assinatura;

   Cada cidadão eleitor só poderá ser proponente de uma única candidatura à Presidência
    da República.
                                                        (artigos 13.º,15.º/1, 4, 5 e 6 e 119.º LEPR)
Consulte o modelo exemplificativo em 6.1.



4.3 Pedido de certidão de eleitor

A certidão de eleitor é necessária (para o candidato e cada um dos proponentes).

O pedido de certidão de eleitor é feito através de requerimento apresentado em duplicado
(perante a respetiva comissão recenseadora, que funciona, em território nacional, na junta de
freguesia ou, no estrangeiro, nas representações diplomáticas) e deve indicar o nome do
candidato proposto (sendo o duplicado arquivado pela comissão recenseadora). Consulte o
modelo exemplificativo em 6.2.



Sobre quem pode efetuar o pedido, transcreve-se o entendimento da CNE (deliberação de 13-
12-2011):

   Qualquer cidadão pode pedir certidão da sua capacidade eleitoral, nomeadamente
    para fins de candidatura (candidato) ou para propositura daquela (proponente), podendo
    ser-lhe exigida a apresentação de documento de identificação;

   Se o pedido for formulado e subscrito por terceiro (nomeadamente mandatário ou
    representante da candidatura), pode ser-lhe exigido que comprove a sua legitimidade
    mediante a exibição de qualquer documento que contenha o seu nome e a qualidade em
    que intervém, designadamente a declaração de propositura ou declaração/procuração do
    candidato. Pode igualmente ser-lhe exigida a apresentação de documento de identificação.


                                                                                         Pág. 6 de 9
4.4 Mandatário e representantes

Cada candidato designa um mandatário para o representar nas operações de julgamento de
elegibilidade e subsequentes.

A morada do mandatário deve constar do processo de candidatura e, caso não resida em
Lisboa, deve aí escolher domicílio para efeitos de notificação.

O candidato poderá ainda designar representante seu em cada sede de distrito ou Região
Autónoma, no território nacional, ou em cada área consular, no estrangeiro.
                                                                                 (artigo 16.º LEPR)



4.5 Local e prazo de apresentação das candidaturas

A apresentação das candidaturas faz-se perante o Tribunal Constitucional até 30 dias antes
da data prevista para a eleição (Rua de O Século, nº 111, Lisboa / 4.ª Secção da Secretaria
Judicial do Tribunal - telefone: 213 233 634).
                                                                  (artigo 14.º e 159.º-A/3 LEPR)



5. FINANCIAMENTO DA CAMPANHA ELEITORAL E PRESTAÇÃO DE CONTAS

É obrigatória, entre outros, a constituição de conta bancária específica para a campanha, a
designação de mandatário financeiro e a apresentação do orçamento de campanha, bem como,
após o ato eleitoral, a prestação de contas perante o Tribunal Constitucional.

Para melhor esclarecimento, consultar os diplomas que regulam o financiamento das
campanhas (Lei n.º 19/2003, de 20 de junho, e Lei Orgânica n.º 2/2005, de 10 de janeiro) e
outras informações no sítio oficial na Internet da Entidade das Contas e Financiamento
Políticos, em www.tribunalconstitucional.pt/tc/contas.html.



6. MODELOS EXEMPLIFICATIVOS

É da exclusiva competência do Tribunal Constitucional verificar a regularidade dos processos
de candidatura. Os modelos que se anexam são assim meramente exemplificativos,
procurando apenas auxiliar os candidatos na elaboração e sistematização dos respetivos
processos de candidatura.

                                                                                        Pág. 7 de 9
6.1 MODELO exemplificativo
                              DECLARAÇÃO DOS PROPONENTES
      de apresentação da candidatura de _____________________________

                      à Eleição do Presidente da República 2016



Os abaixo assinados declaram, por sua honra, propor como candidato à eleição
para o Presidente da República _________________ (nome completo), ____(idade),
CC/BI n.º _______, do arquivo de identificação de __________, de ________ (data de
validade do CC ou data de emissão do BI),           filho de _____ e de _________;
__________(profissão),     natural     de     ____________,       com      residência      em
______________________________ (morada completa).



Proponentes:

(Nome completo) _________________________________________________________,
(CC/BI/Passaporte n.º) __________, ___-____-_____(data de validade do CC ou data de emissão do
BI/Passaporte), ________________, (entidade emitente)

(Nº de Eleitor) ____________, (freguesia e concelho do recenseamento) ________/___________,

Data: / / (Assinatura igual à do CC/BI) ________________________________________.



(Nome completo) _________________________________________________________,
(CC/BI/Passaporte n.º) __________, ___-____-_____(data de validade do CC ou data de emissão do
BI/Passaporte), ________________, (entidade emitente)

(Nº de Eleitor) ____________, (freguesia e concelho do recenseamento) ________/___________,

Data: / / (Assinatura igual à do CC/BI) ________________________________________.



                                              (E assim sucessivamente, em cada folha)


                                                                                    Pág. 8 de 9
6.2 MODELO EXEMPLIFICATIVO

                              PEDIDO DE CERTIDÃO DE ELEITOR

                       (Modelo para pedidos feitos por terceiros)



Exmo. Senhor

Presidente da Comissão Recenseadora de _______________



(nome completo) ______________________, portador do CC/BI n.º _____________,
com o n.º de eleitor ____________, recenseado na freguesia de _______________,
concelho de _______________, na qualidade de representante, mandatário do
candidato ______________(nome) para a eleição do Presidente da República de 2016,
requer a V. Ex.ª, ao abrigo do n.º 6 do artigo 15.º da Lei Eleitoral do Presidente da
República (DL n.º 319-A/76, de 3 de maio), a prova de capacidade eleitoral (certidão de
eleitor) do(s) seguinte(s) cidadão(s):

   (…)




 ___________________, _________de __________________de _____


                                    O(a) Requerente

               x______________________________________________
                         (assinatura igual à do bilhete de identidade)




                                                                             Pág. 9 de 9
