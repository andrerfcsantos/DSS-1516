Eleição da Assembleia da República 2015
CIRCULO ELEITORAL DE FARO

                         LISTAS DEFINITIVAMENTE ADMITIDAS

       PCTP/MRPP Partido Comunista dos Trabalhadores Portugueses

                                       Efetivo
   António João Costa Gamboa
   António Manuel Ferro Terramoto
   Maria Luísa Raposo Guerreiro da Silva
   Anália Boaventura Ramos Guerreiro (independente)
   Emílio José da Conceição Ferreira Rebelo
   Luís Filipe Martins Alexandre (independente)
   Dalila Maria Leite Gonçalves Bica
   Filipe Emílio Gonçalves Rebelo
   João Martins Varela Sancho

                                       Suplente
   Margarida Maria da Silva Macató
   António Serra dos Santos
   Maria João Almeida Curado (independente)
   Dâmaso Augusto Silva do Nascimento
   Fernando Manuel Martins Colaço




                            B.E. Bloco de Esquerda

                                      Efetivo
   João Manuel Duarte Vasconcelos
   Leónia Gonçalves Gramacho Norte
   José António de Sousa Moreira
   Manuela José Goes Ferreira da Silva
   José Manuel Modesto Dourado
   Andreia Lopes Branco Pais
   Jorge Manuel Albano da Encarnação Ramos
   Maria Celeste Rodrigues dos Santos
   José Manuel Dias Domingos

                                       Suplente
   Gilda Maria Sancho Gil
   Pedro Miguel Sousa da Mota
   Tatiana Moreira Palma Caldeirinha
                                                             Página 1 de 7
Tiago da Cruz Simão Grosso
Luís Miguel de Oliveira Catarino




                                   PTP-MAS Agir

                                    Efetivo
Liliana Marisa Carvalho Nóbrega
Paulo Carlos Franco Lopes
Carlos Daniel Vergueiro da Silva Tendeiro
Maria da Graça Afonso Baptista
Ana Catarina Tomé Fialho
Domingos Fonseca
Marta Isabel Ramos Salero Zita
José Miguel Figueira
Suzel Maria Correia Ramos Barrada

                                     Suplente
Pedro Miguel Figueiras Varela
Richard da Conceição Oliveira Lança
Maria de Fátima Caravela José Soares
Miguel Duarte Inocêncio




                     PNR Partido Nacional Renovador

                                      Efetivo
José Artur Gonçalves Antunes
Rolando José Gonçalves Mateus
Ana Paula Vieira Cardoso Lopes
Fábio José Rosa Fatal
José Carlos da Cruz Santos
Maria Filipa Matoso Leiria Pinto Simões de Carvalho
Francisco José Gonçalves dos Santos
José Reis de Carvalho
Maria Emília Ribeiro Delgado

                                     Suplente
Erica Tatiana Guerreiro Cardoso
João Miguel Bernardo Valério




                                                      Página 2 de 7
                            NC Nós, Cidadãos!

                                   Efetivo
Nuno Manuel Guerreiro Campos Inácio (independente)
Antonieta Paulino Felizardo Guerreiro
Nuno Miguel Matias Cabrita da Silva Alves
Paulo Jorge da Conceição Domingues (independente)
Maria Estela Mendes Pinto Lapa
João Luís Moreira Bárbara
Luís José da Fonseca dos Santos
Maria Teresa Sequeira Montes (independente)
Fernando Luís Nobre Correia Carapeto (independente)

                                 Suplente
Noélia Maria dos Ramos Fernandes Simão Falcão
José Manuel Duarte Landeiro Gamboa
António Domingos Barão Iria
Ana Patrícia Bernardo Pedreira
Telma Patrícia Gregório Sousa Martins (independente)




                           MPT Partido da Terra

                                   Efetivo
Rui Luís Freitas Amador Gomes
Liliana Margarida Ramos e Salvador
Daniel Octaviano Vieira da Fonseca
Ricardo Miguel Bentes dos Santos
Catarina Gomes Cereja
Raul Filipe Marques Luís
Dânia Jorge Marques Luís
Jorge Miguel Rodrigues da Silva
Luís Filipe da Silva Neto

                                 Suplente
Sara Filipa dos Santos Mimoso
Emanuel Albano Franco
Luísa Maria Martins Agosto
Alice Morgado de Sousa Calaça




                                                       Página 3 de 7
            PCP-PEV CDU - Coligação Democrática Unitária

                                   Efetivo
Paulo Miguel de Barros Pacheco Seara de Sá
Catarina Alexandra Matos Marques
Joana Catarina Quintanova Sanches
Eduardo Manuel Brasão Costa
António Filipe Parra Martins
Maria Paula Andrade Santos Vilallonga
Maria Luíza Medeiro Conduto Luís (independente)
Tiago Carneiro Jacinto
Luís Filipe Duarte Santos

                                 Suplente
Maria Matilde Barão Afonso Mendonça
José Raimundo Pereira Pedro
Manuel António Pereira
Ana Paula Pereira Viana (independente)
Ricardo Silva Martins




                    L/TDA LIVRE/Tempo de Avançar

                                   Efetivo
Anabela Custódio Afonso (independente)
Joaquim Manuel Mealha Costa (independente)
Maria de Guadalupe Miranda Simões (independente)
Fernando António Lopes Leitão Correia (independente)
Carla Sofia Natividade Emídio do Carmo
Carlos David da Loura Marques
Ana Maria Xavier Cavaco Gonçalves (independente)
Paulo Manuel Ribeiro Pereira
Andreia Afonso Rodrigues (independente)

                                 Suplente
Joaquim Guilherme Guerreiro Nunes (independente)
Maria Emília Lima Costa (independente)
Manuel Cândido Nunes Mariano
Júlio Gonzaga Vaz de Medeiros Andrade (independente)




                                                           Página 4 de 7
                      PAN Pessoas-Animais-Natureza

                                  Efetivo
Elza Maria Martins de Sousa Cunha
Armando António Mestre Frade
Rosa Maria Guerreiro de Sousa
Telma Marisa Fernandes Ferreira
Vasco Manuel Martins Reis
Rui Lopes Penha Pereira
Sílvia Maria Dias Dionísio
Maria Domingas Louzeiro Machadinho
Marina Gouveia Leonardo

                                    Suplente
Patrícia Isabel Guerreiro António
Paulo Sérgio de Jesus Baptista
Emma Charlotte Steele
Fábio Santos Guerreiro




                     PPM Partido Popular Monárquico

                                  Efetivo
Deolinda Rosa Machado Vieira Estevão
João António Mendes de Mendonça
Filipe Tadeu Hilário Alves
Tânia Trindade Roldão Geraldes Tomaz
Roberto Carlos Freitas da Silva
Cassiano Medeiros Proença
Maria de Fátima Hilário Alves
Edmundo António Pimentel
Rui Tadeu Garcia Pedras

                                    Suplente
Maria Aurora Avelar da Rocha
Fátima Pimentel Câmara Pereira




                                                      Página 5 de 7
                          PS Partido Socialista

                                 Efetivo
José Apolinário Nunes Portada
António Paulo Jacinto Eusébio
Jamila Bárbara Madeira e Madeira
Luís Miguel da Graça Nunes
Fernando José dos Santos Anastácio
Ana Lúcia Silva de Passos
José Pedro Henrique Cardoso
Josélia Maria Gomes Mestre Gonçalves
Paulo José Dias Morgado

                                Suplente
João Pedro da Conceição Rodrigues
Célia Paula Palmiro de Brito
Anabela Simão Correia Rocha
Ricardo José Madeira Cipriano
Maria de Lurdes Ferreira Cabral




                 PDR Partido Democrático Republicano

                                    Efetivo
Maria Isabel do Rosário Vaz da Silva Maia
Álvaro Alberto de Castro
Rui Cesar Figueiredo Ferreira
Maria Manuela Constantino da Silva Martins
Dário Manuel Policarpo Azinheira da Silva
Inês Isabel Gonçalves Agostinho
Manuel da Ponte Filipe
Dina Maria da Silva Caracol
Telma Rute Chaves da Costa Caramelo da Silva

                                Suplente
António José Clara
Paula Maria Pereira Ferro
Rosi Aparecida Lopes Baptista




                                                       Página 6 de 7
                   PPD/PSD.CDS-PP Portugal à Frente

                                  Efetivo
José Carlos Costa Barros (independente)
Cristóvão Duarte Nunes Guerreiro Norte
Teresa Margarida Figueiredo de Vasconcelos Caeiro
Pedro Alexandre Vicente de Araújo Lomba (independente)
Bruno Miguel Martins Inácio
Elsa Maria Simas Cordeiro
Francisco José Seno Paulino
Carlos Eduardo Gouveia Martins
Graça Maria da Palma Pereira

                                   Suplente
Nuno Filipe Carreiro Ferreira Serafim
José Pedro da Silva Caçorino
Lina Teresa Martins Amaro dos Santos Neto
Ana Paula dos Santos Lemos Artur
Rui Celestino dos Santos Cristina




           PURP Partido Unido dos Reformados e Pensionistas

                                  Efetivo
Fernando José Morais da Rocha
Alberto Sérgio Pinto Gonçalves
Elisa Manuela Pinto Gonçalves
Diana Filipa Barradas Correia
Francisco José Morais Ferreira César
Carlos Alberto Massapina da Silva
Arminda da Conceição Morais
Mónica Alexandra Ribeiro Monteiro Ferreira dos Mártires
Luís Miguel Cabrita Coelho

                                 Suplente
José António Fernandes Baptista
Patrícia Manuela Evaristo Marafuga
Ana Isabel Jorge Vicente




                                                              Página 7 de 7
