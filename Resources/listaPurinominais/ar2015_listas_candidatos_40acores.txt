Eleição da Assembleia da República 2015
CIRCULO ELEITORAL DA REGIÃO AUTÓNOMA DOS AÇORES

                         LISTAS DEFINITIVAMENTE ADMITIDAS



                          CDS-PP.PPM Aliança Açores

                                       Efetivo
   António Félix Flores Rodrigues
   Maria Leonor Faria e Maia de Oliveira Soares
   Lourdes da Conceição Arruda Costa Araújo da Ponte
   Roger Leonel Vieira de Sousa
   Manuel Humberto Lopes São João

                                     Suplente
   Sónia Cristina Pimentel da Rosa
   Ricardo Manuel Pontes Silveira
   João Filipe Lourenço da Silva
   Ângela Maria Freitas Câmara
   Aldino Machado de Melo




                     PDR Partido Democrático Republicano

                                     Efetivo
   Rui Guiherme da Costa Estrela
   Disidério Paulo de Araujo Machado
   Carina Pereira Barbosa
   Fernando António Figueiredo Martins
   José Manuel de Faria Correira

                                     Suplente
   Luis Rafael Simão Tavares
   Eulalia Catarina Furtado Costa
   João Manuel Tavares da Costa
   Vítor Hugo Soares Tavares
   Teresa de Fátima Pimentel V. Raposo




                                                            Página 1 de 6
             PCP-PEV CDU - Coligação Democrática Unitária

                                   Efetivo
Aníbal da Conceição Pires
Paula Rocha Peixoto Decq Mota
Paulo Filipe Pacheco Santos
Vera Mónica Pacheco Correia
Sérgio Manuel Goulart Gonçalves

                                  Suplente
Luísa Maria Valadão Corvelo
Joana Catarina Silva Fonseca
Marco José Moura Coelho
Ana Maria Rebelo Torres
Maria Alvarina da Silva Ferreira do Céu




     PCTP/MRPP Partido Comunista dos Trabalhadores Portugueses

                                    Efetivo
Pedro Albergaria Leite Pacheco
Pedro Miguel Machado Vultão
Sónia Patrícia da Silva Gomes (independente)
José Rocha Pereira Júnior (independente)
Hernâni Amorim Martins

                                  Suplente
Ana Isabel de Melo Vicente
Paulo Joaquim Tavares Pimentel (independente)
Rosa Margarida Cabral Pereira Pimentel (independente)
Joaquim Amorim Martins
Ludovina de Lurdes Correia da Silva Gomes (independente)




                           B.E. Bloco de Esquerda

                                   Efetivo
António Manuel Raposo Lima
Lúcia de Fátima Oliveira Arruda
Paulo José Maio Sousa Mendes
Mário Manuel de Castro Moniz

                                                            Página 2 de 6
Alexandra Patrícia Soares Manes

                                  Suplente
Paulo Manuel Besugo Sanona
Vera Lúcia Pinheiro Pires




                   PPD/PSD Partido Social Democrata

                                   Efetivo
Berta Maria Correia de Almeida de Melo Cabral
António Lima Cardoso Ventura
Rosa Maria Brasil Dart
José Joaquim Ferreira Machado
Eunice Maria Pinheiro Sousa

                                  Suplente
Bárbara Sofia Bettencourt Pereira
Fortunato Manuel de La Cerda Gomes e Garcia
Eulália Fernanda Pais Aguiar
Diana Marina Filipe Almeida Lopes
Rui Alexandre dos Reis Arruda




                           PS Partido Socialista

                                  Efetivo
Carlos Manuel Martins do Vale Cesar
Lara Fernandes Martinho
João Fernando Brum de Azevedo e Castro
Jorge Fernando Rodrigues Pereira
Madalena Moniz Faria Lobo San-Bento

                                  Suplente
Mónica Susana Viegas Alvernaz
João Manuel Vasconcelos Mendonça
Ana Ricarda Evangelista e Sá Pereira Da Costa
Ivan Marino Gomes Castro
Oscar Manuel Valentim da Rocha




                                                      Página 3 de 6
                               PTP-MAS Agir

                                     Efetivo
Eduardo Jorge Pimentel Rodrigues Pereira
Juliana Madruga Inácio
Ricardo Jorge Pavão de Matos
Anabela Marques Sobrinho
Patrícia Isabel da Silva Furtado Pavão de Matos

                                Suplente
João Carlos dos Santos Moreira
Miguel José Lima Castelhano
Marta Alexandra Surrador Vieira
José Carlos da Costa Nunes Santos
Ivo André Sousa de Almeida




                      PAN Pessoas-Animais-Natureza

                                   Efetivo
Marlene Susana Raposo Dâmaso
Nuno António Teixeira Pascoal
Maria João Pires Ribeiro Vieira Martins
Cândida Maria Teixeira Pascoal Ferreira
Maria de São Pedro Amorim Teixeira Pascoal

                                  Suplente
Luis Filipe Mota Viveiros
José Francisco Pereira




                      L/TDA LIVRE/Tempo de Avançar

                                   Efetivo
Pedro Mendes Alves (independente)
Florbela Martins do Carmo
José Manuel Viegas de Oliveira Neto Azevedo
João Ricardo Ponte Sousa Vasconcelos (independente)
Alexandra Patrícia Cardeal da Costa Lopes Ávila Trindade (independente)




                                                                     Página 4 de 6
                                   Suplente
Tiago Filipe Pereira Franco Ferreira (independente)
Jorge Correia dos Santos (independente)




                  PDA Partido Democrático do Atlântico

                                    Efetivo
Rui Jorge de Sousa Matos
João Carlos Pereira da Nóbrega
Sónia Patrícia Lopes Leça Peixoto
José Soares
Rogério Correiro Medeiros

                                 Suplente
Maria Eduarda Cordeiro Teves Peixoto
Sérgio Alexandre Teves Peixoto




                           MPT Partido da Terra

                                    Efetivo
Luís Filipe Menezes Ferreira
Rita da Graça Rodrigues Vieira Pereira
Yessica Maribel Jesus Gois Bettencourt
João Ricardo Abreu Fernandes
Sónia Filipa Sousa Ferraz Vieira

                                    Suplente
Sílvia Maria Ferraz Gouveia
Miguel Duarte Ferreira Vieira




                     PNR Partido Nacional Renovador

                                  Efetivo
Roque Manuel da Silva Gaio e Melo de Almeida
Maria Odete da Luz Pereira Melo do Patrocínio
Alexandre Manuel Pires Clara Reis
Carlos Miguel de Sousa Mesquita
Leonor de Goes Pinto Coelho

                                                         Página 5 de 6
                                   Suplente
Rui Fernando de Jesus Figueiredo
Ana Catarina Bernardo Valério




                            NC Nós, Cidadãos!

                                   Efetivo
Rúben Miguel Pacheco Correia
Alfredo Bulhões Gago da Câmara
Adriana Martins Falcão Rebelo (independente)
Bruno Cedros da Areia (independente)
Heloísa Araújo Malainho (independente)

                                   Suplente
Bruno Filipe Ferreira Corvelo (independente)
Bela Silveira Dutra (independente)
Paulo Alexandre Sousa Freitas (independente)




          PURP Partido Unido dos Reformados e Pensionistas

                                   Efetivo
Mário Félix do Couto
Adelino Manuel Jesus Lourenço Ferreira
Cláudia Teves Félix Couto
António Francisco Resendes Benjamim
Maria Madalena Teves de Almeida Costa

                                 Suplente
Hélia Maria Teves Rosa Félix
Gabriel Francisco Teves de Almeida
Astéria Maria Teves de Almeida




                                                             Página 6 de 6
