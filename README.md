# DSS-1516 #

![alttext](http://www.usa-it-company.com/img/Desktop%20Applications%20Development.jpg)


## Contactos ##

| Nome            | nº Aluno | Telemóvel | Email                                                | Skype            |
|-----------------|----------|-----------|------------------------------------------------------|------------------|
| Bruno Pereira   | a72628   | 917691726 | [pereirabruno05@gmail.com](pereirabruno05@gmail.com) | pereira.bruno.pg |
| Jéssica Pereira | a71164   | 927437760 | [a71164@alunos.uminho.pt](a71164@alunos.uminho.pt)   | rockfresh.       |
| André Santos    | a61778   | 919065240 | [andrerfcsantos@gmail.com](andrerfcsantos@gmail.com) | andre.rfcs       |
| Xavier Rodrigues| a67676   | 934070120 | [xavi.passos@gmail.com](xavi.passos@gmail.com)       | chamine007       |


## Calendário ##

| Nome   UC       | Fase       | Data Entrega |
|-----------------|------------|--------------|
|                 | Fase 1     | //2015       |
|                 | fase 2     | 30/12/2015   |


