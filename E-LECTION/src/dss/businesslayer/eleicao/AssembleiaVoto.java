package dss.businesslayer.eleicao;

import java.util.ArrayList;

public class AssembleiaVoto {
	
	private ArrayList<UrnaVoto> urnasVoto;

	/*Construtor vazio
	*/
	public AssembleiaVoto(){
		urnasVoto = new ArrayList<>();
	}

	/*Construtor parametrico
	*/
	public AssembleiaVoto(ArrayList<UrnaVoto> urnasVoto) {
		this.urnasVoto = urnasVoto;
	}


	/*Construtor de cópia
	*/
	public AssembleiaVoto(AssembleiaVoto a){

		for(UrnaVoto urna : a.geturnasVoto()){
			this.urnasVoto.add(urna.clone());
		}
	}


	public void addUrnaVoto(UrnaVoto mesa) {
		urnasVoto.add(mesa);
	}

	public ArrayList<UrnaVoto> geturnasVoto() {
		return urnasVoto;
	}


	public void seturnasVoto( ArrayList<UrnaVoto> aux){
		for(UrnaVoto mv : aux){
			this.urnasVoto.add(mv.clone());
		}
}

	
	public void iniciaUrnasVoto(){
		for(UrnaVoto mv : this.urnasVoto){
			mv.abreUrna();
		}
	}

	public void encerraUrnasVoto(){
		for(UrnaVoto mv: this.urnasVoto){
			mv.fechaUrna();
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		AssembleiaVoto that = (AssembleiaVoto) o;
		
		return this.urnasVoto.equals(that.geturnasVoto());

	}

	@Override
	public int hashCode() {
		return this.urnasVoto.hashCode();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("AssembleiaVoto{");
		sb.append("urnasVoto=").append(urnasVoto);
		sb.append('}');
		return sb.toString();
	}

	public AssembleiaVoto clone(){
		return new AssembleiaVoto(this);
	}

}