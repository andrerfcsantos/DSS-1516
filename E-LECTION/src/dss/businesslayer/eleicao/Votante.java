package dss.businesslayer.eleicao;

import dss.businesslayer.candidaturas.listas.Lista;
import dss.businesslayer.documento.DocumentoIdentificacao;

public class Votante {
	private boolean votou;
	private int idCirculo;
	public DocumentoIdentificacao docId;
	public Voto voto; // Voto do votante
	public int nrEleitor;

	// Efetua votanteo
	//TODO contar o voto no partido -> incrementar
	public void vota(Lista lista){
		this.voto.setLista(lista);
	}

	//Getters Setters nrEleitor
	public void setNrEleitor(int n){
		this.nrEleitor = n;
	}

	public int getNrEleitor(){
		return this.nrEleitor;
	}
	
	public boolean isVotou() {
		return votou;
	}

	public void setVotou(boolean votou) {
		this.votou = votou;
	}

	public int getIdCirculo() {
		return idCirculo;
	}

	public void setIdCirculo(int idCirculo) {
		this.idCirculo = idCirculo;
	}

	public DocumentoIdentificacao getDocId() {
		return docId;
	}

	public void setDocId(DocumentoIdentificacao docId) {
		this.docId = docId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Votante votante = (Votante) o;

		if (votou != votante.votou) return false;
		if (idCirculo != votante.idCirculo) return false;
		return docId.equals(votante.docId);

	}

	@Override
	public int hashCode() {
		int result = (votou ? 1 : 0);
		result = 31 * result + idCirculo;
		result = 31 * result + docId.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Votante{");
		sb.append("votou=").append(votou);
		sb.append(", idCirculo=").append(idCirculo);
		sb.append(", docId=").append(docId);
		sb.append('}');
		return sb.toString();
	}
}