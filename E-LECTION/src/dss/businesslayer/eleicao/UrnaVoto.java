package dss.businesslayer.eleicao;

public class UrnaVoto {
	private boolean aberta;


	//Construtor de cópia
	public UrnaVoto(UrnaVoto u){
		this.aberta = u.isAberta();
	}

	//Construtor parametrico
	public UrnaVoto(boolean aberta) {
		this.aberta = aberta;
	}

	//Construtor vazio
	public UrnaVoto(){
		this.aberta = false;
	}

	public boolean isAberta() {
		return aberta;
	}

	public void setAberta(boolean aberta) {
		this.aberta = aberta;
	}

	//TODO
	public void abreUrna(){
		this.setAberta(true);
	}

	public void fechaUrna(){
		this.setAberta(false);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		UrnaVoto urnaVoto = (UrnaVoto) o;

		return aberta == urnaVoto.aberta;

	}

	@Override
	public int hashCode() {
		return (aberta ? 1 : 0);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("UrnaVoto{");
		sb.append("aberta=").append(aberta);
		sb.append('}');
		return sb.toString();
	}

	//Clone
	public UrnaVoto clone(){
		return new UrnaVoto(this);
	}
}