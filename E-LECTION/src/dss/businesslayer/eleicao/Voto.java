package dss.businesslayer.eleicao;

import dss.businesslayer.candidaturas.listas.Lista;
import dss.businesslayer.circulos.CirculoEleitoral;

public class Voto {
	private CirculoEleitoral votos;
	private Lista lista;

	public Voto(CirculoEleitoral votos, Lista lista) {
		this.votos = votos;
		this.lista = lista;
	}

	public CirculoEleitoral getVotos() {
		return votos;
	}

	public void setVotos(CirculoEleitoral votos) {
		this.votos = votos;
	}

	public Lista getLista() {
		return lista;
	}

	public void setLista(Lista lista) {
		this.lista = lista;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Voto voto = (Voto) o;

		if (!votos.equals(voto.votos)) return false;
		return lista.equals(voto.lista);

	}

	@Override
	public int hashCode() {
		int result = votos.hashCode();
		result = 31 * result + lista.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Voto{");
		sb.append("votos=").append(votos);
		sb.append(", lista=").append(lista);
		sb.append('}');
		return sb.toString();
	}
}