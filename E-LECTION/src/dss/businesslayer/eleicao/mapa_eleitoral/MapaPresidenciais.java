package dss.businesslayer.eleicao.mapa_eleitoral;

public class MapaPresidenciais extends MapaEleitoral {
    public MapaPresidenciais(int totalEleitores, int votosValidos, int votosBranco, int abstencoes) {
        super(totalEleitores, votosValidos, votosBranco, abstencoes);
    }
}