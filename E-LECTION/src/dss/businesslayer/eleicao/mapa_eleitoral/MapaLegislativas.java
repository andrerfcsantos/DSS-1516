package dss.businesslayer.eleicao.mapa_eleitoral;

public class MapaLegislativas extends MapaEleitoral {
	private int nr_deputados;

	public MapaLegislativas(int totalEleitores, int votosValidos, int votosBranco, int abstencoes, int nr_deputados) {
		super(totalEleitores, votosValidos, votosBranco, abstencoes);
		this.nr_deputados = nr_deputados;
	}

	public int getNr_deputados() {
		return nr_deputados;
	}

	public void setNr_deputados(int nr_deputados) {
		this.nr_deputados = nr_deputados;
	}
	// TODO: Implementar metodos standard quando se definir melhor esta classe

}