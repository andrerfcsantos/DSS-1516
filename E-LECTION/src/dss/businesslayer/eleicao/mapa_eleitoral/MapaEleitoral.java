package dss.businesslayer.eleicao.mapa_eleitoral;

public class MapaEleitoral {
	private int totalEleitores;
	private int votosValidos;
	private int votosBranco;
	private int abstencoes;

	public MapaEleitoral(int totalEleitores, int votosValidos, int votosBranco, int abstencoes) {
		this.totalEleitores = totalEleitores;
		this.votosValidos = votosValidos;
		this.votosBranco = votosBranco;
		this.abstencoes = abstencoes;
	}

	public int getTotalEleitores() {
		return totalEleitores;
	}

	public void setTotalEleitores(int totalEleitores) {
		this.totalEleitores = totalEleitores;
	}

	public int getVotosValidos() {
		return votosValidos;
	}

	public void setVotosValidos(int votosValidos) {
		this.votosValidos = votosValidos;
	}

	public int getVotosBranco() {
		return votosBranco;
	}

	public void setVotosBranco(int votosBranco) {
		this.votosBranco = votosBranco;
	}

	public int getAbstencoes() {
		return abstencoes;
	}

	public void setAbstencoes(int abstencoes) {
		this.abstencoes = abstencoes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MapaEleitoral that = (MapaEleitoral) o;

		if (totalEleitores != that.totalEleitores) return false;
		if (votosValidos != that.votosValidos) return false;
		if (votosBranco != that.votosBranco) return false;
		return abstencoes == that.abstencoes;

	}

	@Override
	public int hashCode() {
		int result = totalEleitores;
		result = 31 * result + votosValidos;
		result = 31 * result + votosBranco;
		result = 31 * result + abstencoes;
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("MapaEleitoral{");
		sb.append("totalEleitores=").append(totalEleitores);
		sb.append(", votosValidos=").append(votosValidos);
		sb.append(", votosBranco=").append(votosBranco);
		sb.append(", abstencoes=").append(abstencoes);
		sb.append('}');
		return sb.toString();
	}
}