package dss.businesslayer.documento;

import java.util.GregorianCalendar;

public abstract class DocumentoIdentificacao {
	
	private int numero;
	private GregorianCalendar data_validade;

	public DocumentoIdentificacao(int numero, GregorianCalendar data_validade) {
		this.numero = numero;
		this.data_validade = (GregorianCalendar) data_validade.clone();
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public GregorianCalendar getData_validade() {
		return (GregorianCalendar) data_validade.clone();
	}

	public void setData_validade(GregorianCalendar data_validade) {
		this.data_validade = (GregorianCalendar) data_validade.clone();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DocumentoIdentificacao that = (DocumentoIdentificacao) o;

		if (numero != that.numero) return false;
		return data_validade.equals(that.data_validade);

	}

	@Override
	public int hashCode() {
		int result = numero;
		result = 31 * result + data_validade.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("DocumentoIdentificacao{");
		sb.append("numero=").append(numero);
		sb.append(", data_validade=").append(data_validade);
		sb.append('}');
		return sb.toString();
	}
}