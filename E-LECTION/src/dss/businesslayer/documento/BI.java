package dss.businesslayer.documento;

import java.util.GregorianCalendar;

public class BI extends DocumentoIdentificacao {
	/*TODO: Implementar encapsulamento
			- clone()
			- constructor de copia
			- actualizar getters, setters e outros metodos que exigam clone()
	*/

	private String arquivo;
	private GregorianCalendar data_emissao;

	public BI(int numero, GregorianCalendar data_validade, String arquivo, GregorianCalendar data_emissao) {
		super(numero, data_validade);
		this.arquivo = arquivo;
		this.data_emissao = (GregorianCalendar) data_emissao.clone();
	}

	public String getArquivo() {
		return arquivo;
	}

	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}

	public GregorianCalendar getData_emissao() {
		return (GregorianCalendar) data_emissao.clone();
	}

	public void setData_emissao(GregorianCalendar data_emissao) {
		this.data_emissao = data_emissao;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		BI bi = (BI) o;

		if (!arquivo.equals(bi.arquivo)) return false;
		return data_emissao.equals(bi.data_emissao);

	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + arquivo.hashCode();
		result = 31 * result + data_emissao.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("BI{");
		sb.append(super.toString());
		sb.append(", arquivo='").append(arquivo).append('\'');
		sb.append(", data_emissao=").append(data_emissao);
		sb.append('}');
		return sb.toString();
	}
}