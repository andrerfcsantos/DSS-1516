package dss.businesslayer.documento;

import java.util.GregorianCalendar;

public class CC extends DocumentoIdentificacao {
    public CC(int numero, GregorianCalendar data_validade) {
        super(numero, data_validade);
    }
}