package dss.businesslayer.candidaturas.tribunais;

import java.util.ArrayList;

import dss.businesslayer.candidaturas.listas.ListaPN;
import dss.businesslayer.candidaturas.tribunais.juizes.JuizTC;

public class TConstitucional extends Tribunal {
	public ArrayList<JuizTC> juizes = new ArrayList<JuizTC>();
	public ArrayList<ListaPN> listas = new ArrayList<ListaPN>();

	public TConstitucional(){}

	public void addJuiz(JuizTC juiz){
		juizes.add(juiz);
	}

	public void addLista(ListaPN lista){
		listas.add(lista);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TConstitucional that = (TConstitucional) o;

		if (!juizes.equals(that.juizes)) return false;
		return listas.equals(that.listas);

	}

	@Override
	public int hashCode() {
		int result = juizes.hashCode();
		result = 31 * result + listas.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("TConstitucional{");
		sb.append("juizes=").append(juizes);
		sb.append(", listas=").append(listas);
		sb.append('}');
		return sb.toString();
	}
}