package dss.businesslayer.candidaturas.tribunais.juizes;

public class Juiz {
	/*TODO: Implementar encapsulamento
			- clone()
			- constructor de copia
			- actualizar getters, setters e outros metodos que exigam clone()
	*/
	private String nome;

	public Juiz(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Juiz juiz = (Juiz) o;

		return nome.equals(juiz.nome);
	}

	@Override
	public int hashCode() {
		return nome.hashCode();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Juiz{");
		sb.append("nome='").append(nome).append('\'');
		sb.append('}');
		return sb.toString();
	}
}