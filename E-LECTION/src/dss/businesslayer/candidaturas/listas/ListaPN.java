package dss.businesslayer.candidaturas.listas;

import java.util.ArrayList;

import dss.businesslayer.candidaturas.candidatos.CandidatoAR;
import dss.businesslayer.candidaturas.mandatarios.MandatarioLP;
import dss.businesslayer.candidaturas.tribunais.TConstitucional;

public class ListaPN extends Lista {
	
	private TConstitucional listas;
	private MandatarioLP mandatario;
	private ArrayList<CandidatoAR> candidatos = new ArrayList<>();
	
	

	public ListaPN(EstadosLista estadoLista, TConstitucional listas, MandatarioLP mandatario) {
		super(estadoLista);
		this.listas = listas;
		this.mandatario = mandatario;
	}

	public ListaPN(ListaPN a){
		super(a.getEstadoLista());
		listas = a.getListas();
		mandatario = a.getMandatario();
		candidatos = a.getCandidatos();
	}

	public void addCandidato(CandidatoAR candidato) {
		candidatos.add(candidato);
	}

	public void removeCandidato(int pos) {
		candidatos.remove(pos);

	}

	public TConstitucional getListas() {
		return listas;
	}

	public void setListas(TConstitucional listas) {
		this.listas = listas;
	}

	public MandatarioLP getMandatario() {
		return mandatario;
	}

	public void setMandatario(MandatarioLP mandatario) {
		this.mandatario = mandatario;
	}

	public ArrayList<CandidatoAR> getCandidatos() {
		return candidatos;
	}

	public void setCandidatos(ArrayList<CandidatoAR> candidatos) {
		this.candidatos = candidatos;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		ListaPN listaPN = (ListaPN) o;

		if (!listas.equals(listaPN.listas))
			return false;
		if (!mandatario.equals(listaPN.mandatario))
			return false;
		return candidatos.equals(listaPN.candidatos);

	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + listas.hashCode();
		result = 31 * result + mandatario.hashCode();
		result = 31 * result + candidatos.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("ListaPN{");
		sb.append(super.toString());
		sb.append(", listas=").append(listas);
		sb.append(", mandatario=").append(mandatario);
		sb.append(", candidatos=").append(candidatos);
		sb.append('}');
		return sb.toString();
	}

	public ListaPN clone(){
		return new ListaPN(this);
	}
}