package dss.businesslayer.candidaturas.listas;

import java.util.ArrayList;

import dss.businesslayer.candidaturas.candidatos.CandidatoPR;
import dss.businesslayer.candidaturas.mandatarios.MandatarioLU;

public class ListaUN extends Lista {
	private MandatarioLU mandatario;
	private ArrayList<CandidatoPR> candidato = new ArrayList<CandidatoPR>();

	public ListaUN(EstadosLista estadoLista, MandatarioLU mandatario) {
		super(estadoLista);
		this.mandatario = mandatario;
	}

	public MandatarioLU getMandatario() {
		return mandatario;
	}

	public void setMandatario(MandatarioLU mandatario) {
		this.mandatario = mandatario;
	}

	public ArrayList<CandidatoPR> getCandidato() {
		return candidato;
	}

	public void setCandidato(ArrayList<CandidatoPR> candidato) {
		this.candidato = candidato;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		ListaUN listaUN = (ListaUN) o;

		if (!mandatario.equals(listaUN.mandatario)) return false;
		return candidato.equals(listaUN.candidato);

	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + mandatario.hashCode();
		result = 31 * result + candidato.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("ListaUN{");
		sb.append(super.toString());
		sb.append(", mandatario=").append(mandatario);
		sb.append(", candidato=").append(candidato);
		sb.append('}');
		return sb.toString();
	}
}