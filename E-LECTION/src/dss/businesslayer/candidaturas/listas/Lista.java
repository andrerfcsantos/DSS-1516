package dss.businesslayer.candidaturas.listas;

public abstract class Lista {
	private EstadosLista estadoLista;

	public Lista(EstadosLista estadoLista) {
		this.estadoLista = estadoLista;
	}

	public EstadosLista getEstadoLista() {
		return estadoLista;
	}

	public void setEstadoLista(EstadosLista estadoLista) {
		this.estadoLista = estadoLista;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Lista lista = (Lista) o;

		return estadoLista == lista.estadoLista;

	}

	@Override
	public int hashCode() {
		return estadoLista.hashCode();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Lista{");
		sb.append("estadoLista=").append(estadoLista);
		sb.append('}');
		return sb.toString();
	}
}