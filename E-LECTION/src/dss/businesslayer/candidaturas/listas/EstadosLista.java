package dss.businesslayer.candidaturas.listas;

public enum EstadosLista {
	APROVADA, 
	REJEITADA, 
	POR_APROVAR;
}