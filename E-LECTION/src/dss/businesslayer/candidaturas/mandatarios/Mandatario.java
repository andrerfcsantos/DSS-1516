package dss.businesslayer.candidaturas.mandatarios;

public abstract class Mandatario {
	private String nome;


	public Mandatario(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Mandatario that = (Mandatario) o;

		return nome.equals(that.nome);

	}

	@Override
	public int hashCode() {
		return nome.hashCode();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Mandatario{");
		sb.append("nome='").append(nome).append('\'');
		sb.append('}');
		return sb.toString();
	}


}