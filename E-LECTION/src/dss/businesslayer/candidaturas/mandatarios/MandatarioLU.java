package dss.businesslayer.candidaturas.mandatarios;

public class MandatarioLU extends Mandatario {
    public MandatarioLU(String nome) {
        super(nome);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MandatarioLU{");
        sb.append('}');
        return sb.toString();
    }
}