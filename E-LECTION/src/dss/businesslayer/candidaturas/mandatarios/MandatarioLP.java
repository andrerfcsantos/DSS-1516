package dss.businesslayer.candidaturas.mandatarios;

import dss.businesslayer.candidaturas.candidatos.Partido;

public class MandatarioLP extends Mandatario {
    Partido partido;

    public MandatarioLP(String nome, Partido partido) {
        super(nome);
        this.partido = partido;
    }

    public Partido getPartido() {
        return partido;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        MandatarioLP that = (MandatarioLP) o;

        return partido.equals(that.partido);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + partido.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MandatarioLP{");
        sb.append(super.toString());
        sb.append(", partido=").append(partido);
        sb.append('}');
        return sb.toString();
    }
}