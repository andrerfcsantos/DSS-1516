package dss.businesslayer.candidaturas.candidatos;

public class Partido {
	private String designacao;
	private String sigla;
	private String simboloPath;

	public Partido(String designacao, String sigla, String simboloPath) {
		this.designacao = designacao;
		this.sigla = sigla;
		this.simboloPath = simboloPath;
	}

	public String getDesignacao() {
		return designacao;
	}

	public void setDesignacao(String designacao) {
		this.designacao = designacao;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getSimboloPath() {
		return simboloPath;
	}

	public void setSimboloPath(String simboloPath) {
		this.simboloPath = simboloPath;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Partido partido = (Partido) o;

		if (!designacao.equals(partido.designacao)) return false;
		if (!sigla.equals(partido.sigla)) return false;
		return simboloPath.equals(partido.simboloPath);

	}

	@Override
	public int hashCode() {
		int result = designacao.hashCode();
		result = 31 * result + sigla.hashCode();
		result = 31 * result + simboloPath.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Partido{");
		sb.append("designacao='").append(designacao).append('\'');
		sb.append(", sigla='").append(sigla).append('\'');
		sb.append(", simboloPath='").append(simboloPath).append('\'');
		sb.append('}');
		return sb.toString();
	}
}