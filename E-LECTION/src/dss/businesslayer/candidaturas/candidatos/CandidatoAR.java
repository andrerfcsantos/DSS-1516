package dss.businesslayer.candidaturas.candidatos;

import java.util.GregorianCalendar;

import dss.businesslayer.documento.DocumentoIdentificacao;

public class CandidatoAR extends Candidato {

    public CandidatoAR(String nome, String naturalidade, String profissao, String nome_pai, String nome_mae, String residencia, int nr_eleitor, GregorianCalendar data_nascimento, DocumentoIdentificacao docIdentificacao) {
        super(nome, naturalidade, profissao, nome_pai, nome_mae, residencia, nr_eleitor, data_nascimento, docIdentificacao);
    }
}