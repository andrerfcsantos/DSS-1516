package dss.businesslayer.candidaturas.candidatos;

import java.util.GregorianCalendar;

import dss.businesslayer.documento.DocumentoIdentificacao;

public abstract class Candidato {
	/*TODO: Implementar encapsulamento
			- clone()
			- constructor de copia
			- actualizar getters, setters e outros metodos que exigam clone()
	*/

	private String nome;
	private String naturalidade;
	private String profissao;
	private String nome_pai;
	private String nome_mae;
	private String residencia;
	private int nr_eleitor;
	private GregorianCalendar data_nascimento;
	private DocumentoIdentificacao docIdentificacao;

	public Candidato(String nome, String naturalidade, String profissao, String nome_pai, String nome_mae, String residencia, int nr_eleitor, GregorianCalendar data_nascimento, DocumentoIdentificacao docIdentificacao) {
		this.nome = nome;
		this.naturalidade = naturalidade;
		this.profissao = profissao;
		this.nome_pai = nome_pai;
		this.nome_mae = nome_mae;
		this.residencia = residencia;
		this.nr_eleitor = nr_eleitor;
		this.data_nascimento = data_nascimento;
		this.docIdentificacao = docIdentificacao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNaturalidade() {
		return naturalidade;
	}

	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public String getNome_pai() {
		return nome_pai;
	}

	public void setNome_pai(String nome_pai) {
		this.nome_pai = nome_pai;
	}

	public String getNome_mae() {
		return nome_mae;
	}

	public void setNome_mae(String nome_mae) {
		this.nome_mae = nome_mae;
	}

	public String getResidencia() {
		return residencia;
	}

	public void setResidencia(String residencia) {
		this.residencia = residencia;
	}

	public int getNr_eleitor() {
		return nr_eleitor;
	}

	public void setNr_eleitor(int nr_eleitor) {
		this.nr_eleitor = nr_eleitor;
	}

	public GregorianCalendar getData_nascimento() {
		return data_nascimento;
	}

	public void setData_nascimento(GregorianCalendar data_nascimento) {
		this.data_nascimento = data_nascimento;
	}

	public DocumentoIdentificacao getDocIdentificacao() {
		return docIdentificacao;
	}

	public void setDocIdentificacao(DocumentoIdentificacao docIdentificacao) {
		this.docIdentificacao = docIdentificacao;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Candidato candidato = (Candidato) o;

		if (nr_eleitor != candidato.nr_eleitor) return false;
		if (!nome.equals(candidato.nome)) return false;
		if (!naturalidade.equals(candidato.naturalidade)) return false;
		if (!profissao.equals(candidato.profissao)) return false;
		if (!nome_pai.equals(candidato.nome_pai)) return false;
		if (!nome_mae.equals(candidato.nome_mae)) return false;
		if (!residencia.equals(candidato.residencia)) return false;
		if (!data_nascimento.equals(candidato.data_nascimento)) return false;
		return docIdentificacao.equals(candidato.docIdentificacao);

	}

	@Override
	public int hashCode() {
		int result = nome.hashCode();
		result = 31 * result + naturalidade.hashCode();
		result = 31 * result + profissao.hashCode();
		result = 31 * result + nome_pai.hashCode();
		result = 31 * result + nome_mae.hashCode();
		result = 31 * result + residencia.hashCode();
		result = 31 * result + nr_eleitor;
		result = 31 * result + data_nascimento.hashCode();
		result = 31 * result + docIdentificacao.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Candidato{");
		sb.append("nome='").append(nome).append('\'');
		sb.append(", naturalidade='").append(naturalidade).append('\'');
		sb.append(", profissao='").append(profissao).append('\'');
		sb.append(", nome_pai='").append(nome_pai).append('\'');
		sb.append(", nome_mae='").append(nome_mae).append('\'');
		sb.append(", residencia='").append(residencia).append('\'');
		sb.append(", nr_eleitor=").append(nr_eleitor);
		sb.append(", data_nascimento=").append(data_nascimento);
		sb.append(", docIdentificacao=").append(docIdentificacao);
		sb.append('}');
		return sb.toString();
	}
}