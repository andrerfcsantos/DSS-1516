package dss.businesslayer.utilizadores;

public class UserMesa extends User {
	public UserMesa(String username, String password, String nomeProprio, int cC) {
		super(username, password, nomeProprio, cC);
	}

	public UserMesa(UserMesa user) {
		super(user);

	}

	@Override
	public UserMesa clone() {

		return new UserMesa(this);
	}
}