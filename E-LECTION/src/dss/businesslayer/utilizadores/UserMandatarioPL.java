package dss.businesslayer.utilizadores;

public class UserMandatarioPL extends User {
	
	
	 public UserMandatarioPL() {
	        super();
	    }
	
    public UserMandatarioPL(String username, String password, String nomeProprio, int cC) {
        super(username, password, nomeProprio, cC);
    }
    
    public UserMandatarioPL(UserMandatarioPL user) {
        super(user);
    }

	@Override
	public User clone() {
		
		return new UserMandatarioPL(this);
	}
}