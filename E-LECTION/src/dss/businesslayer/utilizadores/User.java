package dss.businesslayer.utilizadores;

public abstract class User {
	private String username;
	private String password;
	private String nomeProprio;
	private int cC;
	private int neleitor; //numero de eleitor


	public User() {
		this.username = "";
		this.password = "";
		this.nomeProprio = "";
		this.cC = 0;
	}

	public User(String username, String password, String nomeProprio, int cC) {
		this.username = username;
		this.password = password;
		this.nomeProprio = nomeProprio;
		this.cC = cC;
	}
	
	public User(User user) {
		
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.nomeProprio = user.getNomeProprio();
		this.cC = user.getcC();
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNomeProprio() {
		return nomeProprio;
	}

	public void setNomeProprio(String nomeProprio) {
		this.nomeProprio = nomeProprio;
	}

	public int getcC() {
		return cC;
	}

	public void setcC(int cC) {
		this.cC = cC;
	}

	/*//TODO Verificar ask xavier... como validar os dados? ir à base de dados e ver alguma coisa? acho que assim ta bem, nao precisa de ir à bd fazer nada... ??
	public boolean validaDadosUser(int cc, int neleitor){
		return(cc = this.cC && this.neleitor == neleitor); 
	}*/



	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		User user = (User) o;

		if (cC != user.cC) return false;
		if (!username.equals(user.username)) return false;
		if (!password.equals(user.password)) return false;
		return nomeProprio.equals(user.nomeProprio);

	}

	@Override
	public int hashCode() {
		int result = username.hashCode();
		result = 31 * result + password.hashCode();
		result = 31 * result + nomeProprio.hashCode();
		result = 31 * result + cC;
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("User{");
		sb.append("username='").append(username).append('\'');
		sb.append(", password='").append(password).append('\'');
		sb.append(", nomeProprio='").append(nomeProprio).append('\'');
		sb.append(", cC=").append(cC);
		sb.append('}');
		return sb.toString();
	}
	
	@Override
	public abstract User clone();
		
	
}