package dss.businesslayer.utilizadores;

public class UserAdmin extends User {

	public UserAdmin() {
		super();
	}

	public UserAdmin(String username, String password, String nomeProprio, int cC) {
		super(username, password, nomeProprio, cC);
	}

	public UserAdmin(UserAdmin user) {
		super(user);

	}

	@Override
	public User clone() {

		return new UserAdmin(this);
	}
}