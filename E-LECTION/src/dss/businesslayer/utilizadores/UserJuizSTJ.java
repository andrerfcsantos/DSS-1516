package dss.businesslayer.utilizadores;

public class UserJuizSTJ extends User {
	
	 public UserJuizSTJ() {
	        super();
	    }
	
    public UserJuizSTJ(String username, String password, String nomeProprio, int cC) {
        super(username, password, nomeProprio, cC);
    }
    
    public UserJuizSTJ(UserJuizSTJ user) {
        super(user);
    }

	@Override
	public User clone() {
		return new UserJuizSTJ(this);
	}
}