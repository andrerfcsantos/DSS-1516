package dss.businesslayer.utilizadores;

public class UserMandatarioUN extends User {

	public UserMandatarioUN() {
		super();
	}

	public UserMandatarioUN(String username, String password, String nomeProprio, int cC) {
		super(username, password, nomeProprio, cC);
	}

	public UserMandatarioUN(UserMandatarioUN user) {
		super(user);

	}

	@Override
	public User clone() {

		return new UserMandatarioUN(this);
	}
}