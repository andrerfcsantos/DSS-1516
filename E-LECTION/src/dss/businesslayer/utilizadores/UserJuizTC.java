package dss.businesslayer.utilizadores;

public class UserJuizTC extends User {
	
	  public UserJuizTC() {
	        super();
	    }

	
    public UserJuizTC(String username, String password, String nomeProprio, int cC) {
        super(username, password, nomeProprio, cC);
    }
    
    public UserJuizTC(UserJuizTC user) {
        super(user);
    }

	@Override
	public User clone() {
		// TODO Auto-generated method stub
		return new UserJuizTC(this);
	}
}