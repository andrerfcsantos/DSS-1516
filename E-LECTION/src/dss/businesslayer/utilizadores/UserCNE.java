package dss.businesslayer.utilizadores;

public class UserCNE extends User {

	public UserCNE(UserAdmin user) {
		super(user);

	}

	public UserCNE(String username, String password, String nomeProprio, int cC) {
		super(username, password, nomeProprio, cC);
	}

	public UserCNE(UserCNE user) {
		super(user);

	}

	@Override
	public User clone() {

		return new UserCNE(this);
	}

}