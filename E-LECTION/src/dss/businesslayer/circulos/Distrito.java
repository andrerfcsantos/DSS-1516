package dss.businesslayer.circulos;

import java.util.ArrayList;

public class Distrito {
	
	private String designacao;
	private int populacao;
	private ArrayList<Concelho> concelhos;

	

	public Distrito() {
		super();
		this.designacao = "";
		this.populacao = 0;
		this.concelhos = new ArrayList<>();
	}
	
	public Distrito(Distrito d) {
		super();
		this.designacao = d.getDesignacao();
		this.populacao = d.getPopulacao();
		this.concelhos = d.getConcelhos();
	}

	public void addConcelho(Concelho concelho){
		concelhos.add(concelho);
	}

	public String getDesignacao() {
		return designacao;
	}

	public void setDesignacao(String designacao) {
		this.designacao = designacao;
	}

	public int getPopulacao() {
		return populacao;
	}

	public void setPopulacao(int populacao) {
		this.populacao = populacao;
	}

	public ArrayList<Concelho> getConcelhos() {
		return concelhos;
	}

	public void setConcelhos(ArrayList<Concelho> concelhos) {
		this.concelhos = concelhos;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Distrito distrito = (Distrito) o;

		if (populacao != distrito.populacao) return false;
		if (!designacao.equals(distrito.designacao)) return false;
		return concelhos.equals(distrito.concelhos);

	}

	@Override
	public int hashCode() {
		int result = designacao.hashCode();
		result = 31 * result + populacao;
		result = 31 * result + concelhos.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Distrito{");
		sb.append("designacao='").append(designacao).append('\'');
		sb.append(", populacao=").append(populacao);
		sb.append(", concelhos=").append(concelhos);
		sb.append('}');
		return sb.toString();
	}
	
	
	public Distrito clone() {
		
		return new Distrito(this);
	}
}