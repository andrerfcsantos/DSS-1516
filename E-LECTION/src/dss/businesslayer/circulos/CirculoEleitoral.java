package dss.businesslayer.circulos;

import java.util.ArrayList;

import dss.businesslayer.eleicao.AssembleiaVoto;
import dss.businesslayer.eleicao.Votante;
import dss.businesslayer.eleicao.Voto;

public abstract class CirculoEleitoral {
	/*TODO: Implementar encapsulamento
			
			
			- actualizar getters, setters e outros metodos que exigam clone()
	*/

	private String sede;
	private int nr_eleitores;
	private int nr_deputados;
	public Distrito distrito;
	public AssembleiaVoto assembleiaVoto;
	public ArrayList<Voto> votos = new ArrayList<Voto>();
	public ArrayList<Votante> votantes = new ArrayList<Votante>(); //TODO verificar id é o nrEleitor? ok!

	//TODO verificar -> necessário para assembleia validar eleitor dsi
	public Votante getVotanteC(int nrEleitor){
		return votantes.get(nrEleitor);
	}
 

	public CirculoEleitoral(String sede, int nr_eleitores, int nr_deputados, Distrito distrito, AssembleiaVoto assembleiaVoto) {
		this.sede = sede;
		this.nr_eleitores = nr_eleitores;
		this.nr_deputados = nr_deputados;
		this.distrito = distrito;
		this.assembleiaVoto = assembleiaVoto;
	}

	//Construtor de cópia
	public CirculoEleitoral(CirculoEleitoral c){
		sede = c.getSede();
		nr_eleitores = c.getNr_eleitores();
		nr_deputados = c.getNr_deputados();
		distrito = c.getDistrito();
		assembleiaVoto = c.getAssembleiaVoto();
		votos = c.getVotos();
	}

	public void addVoto(Voto voto){
		votos.add(voto);
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public int getNr_eleitores() {
		return nr_eleitores;
	}

	public void setNr_eleitores(int nr_eleitores) {
		this.nr_eleitores = nr_eleitores;
	}

	public int getNr_deputados() {
		return nr_deputados;
	}

	public void setNr_deputados(int nr_deputados) {
		this.nr_deputados = nr_deputados;
	}

	//TODO fazer clone na classe Distrito   
	public Distrito getDistrito() {
		return distrito.clone();
	}

	public void setDistrito(Distrito distrito) {
		this.distrito = distrito;
	}

	//TODO fazer clone na classe Assembleia de voto (ou verificar se já existe)
	public AssembleiaVoto getAssembleiaVoto() {
		return assembleiaVoto.clone();
	}

	public void setAssembleiaVoto(AssembleiaVoto assembleiaVoto) {
		this.assembleiaVoto = assembleiaVoto;
	}

	public ArrayList<Voto> getVotos() {
		return votos;
	}

	public void setVotos(ArrayList<Voto> votos) {
		this.votos = votos;
	}

	//TODO
	public void iniciaMesasVotos(){
		this.assembleiaVoto.iniciaUrnasVoto();
	}

	public void encerraUrnasVotos(){
		this.assembleiaVoto.encerraUrnasVoto();
	}


	//

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CirculoEleitoral that = (CirculoEleitoral) o;

		if (nr_eleitores != that.nr_eleitores) return false;
		if (nr_deputados != that.nr_deputados) return false;
		if (!sede.equals(that.sede)) return false;
		if (!distrito.equals(that.distrito)) return false;
		if (!assembleiaVoto.equals(that.assembleiaVoto)) return false;
		return votos.equals(that.votos);

	}

	@Override
	public int hashCode() {
		int result = sede.hashCode();
		result = 31 * result + nr_eleitores;
		result = 31 * result + nr_deputados;
		result = 31 * result + distrito.hashCode();
		result = 31 * result + assembleiaVoto.hashCode();
		result = 31 * result + votos.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CirculoEleitoral{");
		sb.append("sede='").append(sede).append('\'');
		sb.append(", nr_eleitores=").append(nr_eleitores);
		sb.append(", nr_deputados=").append(nr_deputados);
		sb.append(", distrito=").append(distrito);
		sb.append(", assembleiaVoto=").append(assembleiaVoto);
		sb.append(", votos=").append(votos);
		sb.append('}');
		return sb.toString();
	}


}