package dss.businesslayer.circulos;

import java.util.ArrayList;

public class Concelho {

	/*TODO: Implementar encapsulamento
			- clone()
			- constructor de copia
			- actualizar getters, setters e outros metodos que exigam clone()
	*/

	private String designacao;
	private int populacao;
	public ArrayList<Freguesia> freguesias = new ArrayList<>();


	public void addFreguesia(Freguesia freguesia){
		freguesias.add(freguesia);
	}

	public String getDesignacao() {
		return designacao;
	}

	public void setDesignacao(String designacao) {
		this.designacao = designacao;
	}

	public int getPopulacao() {
		return populacao;
	}

	public void setPopulacao(int populacao) {
		this.populacao = populacao;
	}

	public ArrayList<Freguesia> getFreguesias() {
		return freguesias;
	}

	public void setFreguesias(ArrayList<Freguesia> freguesias) {
		this.freguesias = freguesias;
	}
}