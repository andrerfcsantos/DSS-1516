package dss.businesslayer.circulos;

public class Freguesia {
	/*TODO: Implementar encapsulamento
			- clone()
			- constructor de copia
			- actualizar getters, setters e outros metodos que exigam clone()
	*/
	private int populacao;
	private String designacao;

	public Freguesia(int populacao, String designacao) {
		this.populacao = populacao;
		this.designacao = designacao;
	}

	public int getPopulacao() {
		return populacao;
	}

	public void setPopulacao(int populacao) {
		this.populacao = populacao;
	}

	public String getDesignacao() {
		return designacao;
	}

	public void setDesignacao(String designacao) {
		this.designacao = designacao;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Freguesia freguesia = (Freguesia) o;

		if (populacao != freguesia.populacao) return false;
		return designacao.equals(freguesia.designacao);

	}

	@Override
	public int hashCode() {
		int result = populacao;
		result = 31 * result + designacao.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Freguesia{");
		sb.append("populacao=").append(populacao);
		sb.append(", designacao='").append(designacao).append('\'');
		sb.append('}');
		return sb.toString();
	}
}