package dss.businesslayer;

import java.util.ArrayList;
import java.util.*;

import dss.businesslayer.candidaturas.candidatos.Partido;
import dss.businesslayer.candidaturas.listas.Lista;
import dss.businesslayer.candidaturas.tribunais.Tribunal;
import dss.businesslayer.circulos.CirculoEleitoral;
import dss.businesslayer.documento.DocumentoIdentificacao;
import dss.businesslayer.eleicao.Votante;
import dss.businesslayer.eleicao.mapa_eleitoral.MapaEleitoral;
import dss.businesslayer.utilizadores.User;

public class SGV {
	User userActivo;
	public ArrayList<Lista> listas;
	public HashMap<Integer, Votante> eleitores;
	public HashMap<Integer, CirculoEleitoral> circulos;
	public MapaEleitoral resultado;
	public Tribunal tribunal;
	public ArrayList<Partido> partidos;
	public int tipoVotagens; // 0 se forem presidenciais , 1 se forem
								// partidárias

	public SGV() {
		listas = new ArrayList<>();
		eleitores = new HashMap<>();
		circulos = new HashMap<>();
		partidos = new ArrayList<>();
	}

	public void addLista(Lista lista) {
		listas.add(lista);
	}

	public void addVotante(int nr_eleitor, Votante votante) {
		eleitores.put(nr_eleitor, votante);
	}

	// TODO verificar como implementar vindo da UI
	public void removeLista(int idLista) {
		this.listas.remove(idLista);
	}

	// TODO saber o votante dado o nrEleitor, ir ao circulo eleitoral, mas que
	// circulo eleitoral?
	public Votante getVotanteC(int nrEleitor) {
		CirculoEleitoral c = circulos.get(0); // get dele, é um circulo
												// específico, isto ta mal
		return c.getVotanteC(nrEleitor);
	}

	public void addCirculo(int id, CirculoEleitoral circulo) {
		circulos.put(id, circulo);
	}

	public void addPartido(Partido partido) {
		partidos.add(partido);
	}

	public User getUserActivo() {
		return userActivo;
	}

	public void setUserActivo(User userActivo) {
		this.userActivo = userActivo;
	}

	public Tribunal getTribunal() {
		return tribunal;
	}

	public void setTribunal(Tribunal tribunal) {
		this.tribunal = tribunal;
	}

	public MapaEleitoral getResultado() {
		return resultado;
	}

	public void setResultado(MapaEleitoral resultado) {
		this.resultado = resultado;
	}

	/**
	 * Devolve o tipo de votagens que estão a ser realizadas de momento.
	 * 
	 * @return int : tipo de votagens
	 */
	public int getTipoVotagens() {
		return tipoVotagens;
	}

	/**
	 * Altera o tipo de votagens que irão ser realizadas
	 *
	 */
	public void setTipoVotagens(int tipoVotagens) {
		this.tipoVotagens = tipoVotagens;
	}

	/**
	 * Código para permitir que um eleitor vote
	 */
	// TODO como encontrar a lista da UI para por aqui a que ele votou
	public Lista getLista(int id) {
		/*
		 * for(Lista : listas){ // do arraylist
		 * 
		 * }
		 */

		return this.listas.get(id);
	}

	// TODO
	public ArrayList<Lista> getListas() {
		return this.listas;
	}

	// TODO dar inicio às votagens
	public void startVotagens() {
		for (CirculoEleitoral c : circulos.values()) {
			c.iniciaMesasVotos();
		}
	}

	public void endVotagens() {
		for (CirculoEleitoral c : circulos.values()) {
			c.encerraUrnasVotos();
		}
	}

	public boolean validaDados(int nrEleitor, int nrBI) {

		Votante aux = this.eleitores.get(nrEleitor);

		if (aux != null) {
			DocumentoIdentificacao bi = aux.getDocId();
			if (bi.getNumero() == nrBI)
				return true;
			else
				return false;
		} else
			return false;
	}

}