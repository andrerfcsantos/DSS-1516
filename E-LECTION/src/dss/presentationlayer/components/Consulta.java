package dss.presentationlayer.components;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Consulta extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2331236916520284891L;
	private JList<String> listComum;
	private JTextArea textAreaDetalhes;

	/**
	 * Create the panel.
	 */
	public Consulta() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 1, 32, 135, 30, 217, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 1, 15, 225, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 0;
		gbc_horizontalStrut.gridy = 0;
		add(horizontalStrut, gbc_horizontalStrut);

		JLabel lblLista = new JLabel("Lista");
		GridBagConstraints gbc_lblLista = new GridBagConstraints();
		gbc_lblLista.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblLista.insets = new Insets(0, 0, 5, 5);
		gbc_lblLista.gridx = 2;
		gbc_lblLista.gridy = 1;
		add(lblLista, gbc_lblLista);

		JScrollPane scrollPaneLista = new JScrollPane();
		GridBagConstraints gbc_scrollPaneLista = new GridBagConstraints();
		gbc_scrollPaneLista.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneLista.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPaneLista.gridx = 2;
		gbc_scrollPaneLista.gridy = 2;
		gbc_scrollPaneLista.weightx = gbc_scrollPaneLista.weighty = 1.0;
		add(scrollPaneLista, gbc_scrollPaneLista);

		listComum = new JList<>();
		scrollPaneLista.setViewportView(listComum);
		

		JScrollPane scrollPaneDetalhes = new JScrollPane();

		GridBagConstraints gbc_scrollPaneDetalhes = new GridBagConstraints();
		gbc_scrollPaneDetalhes.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPaneDetalhes.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneDetalhes.gridx = 4;
		gbc_scrollPaneDetalhes.gridy = 2;
		gbc_scrollPaneDetalhes.weightx = gbc_scrollPaneDetalhes.weighty = 1.0;
		add(scrollPaneDetalhes, gbc_scrollPaneDetalhes);

		JLabel lblDetalhes = new JLabel("Detalhes");
		scrollPaneDetalhes.setColumnHeaderView(lblDetalhes);

		textAreaDetalhes = new JTextArea();
		scrollPaneDetalhes.setViewportView(textAreaDetalhes);

		String[] items = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7" };

		// TODO Implementar Observer e modelo tem que ser variável da classe
		DefaultListModel<String> model = new DefaultListModel<>();

		for (String string : items) {

			model.addElement(string);

		}
		listComum.setModel(model);

	}

	/**
	 * @return the listComum
	 */
	public JList<String> getListComum() {
		return listComum;
	}

	/**
	 * @return the textAreaDetalhes
	 */
	public JTextArea getTextAreaDetalhes() {
		return textAreaDetalhes;
	}

}
