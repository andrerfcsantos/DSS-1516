/**
 * 
 */
package dss.presentationlayer.components.separators;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import dss.presentationlayer.components.panels.cne.CNEAbrirAtoEleitoralPanel;
import dss.presentationlayer.components.panels.cne.CNEResultadosPanel;

/**
 * @author bpereira
 *
 */
public class CNESeparator extends JPanel {
  

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JTabbedPane tabbedPaneAssembleiaVoto;
  private CNEAbrirAtoEleitoralPanel abrirAtoEleitoralPanel;
  private JLabel lblEstadoEleicao;
  private CNEResultadosPanel resultadosPanel;

  /**
   * Create the panel.
   */
  public CNESeparator() {

    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    tabbedPaneAssembleiaVoto = new JTabbedPane(JTabbedPane.LEFT);
    GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
    gbc_tabbedPane.insets = new Insets(0, 0, 5, 0);
    gbc_tabbedPane.fill = GridBagConstraints.BOTH;
    gbc_tabbedPane.gridx = 0;
    gbc_tabbedPane.gridy = 0;
    gbc_tabbedPane.weightx = gbc_tabbedPane.weighty = 1.0;
    add(tabbedPaneAssembleiaVoto, gbc_tabbedPane);

    abrirAtoEleitoralPanel = new CNEAbrirAtoEleitoralPanel();
    tabbedPaneAssembleiaVoto.addTab("Abrir Ato Eleitoral", null, abrirAtoEleitoralPanel, null);

    resultadosPanel = new CNEResultadosPanel();
    tabbedPaneAssembleiaVoto.addTab("Gerar Resultados", null, resultadosPanel, null);

    lblEstadoEleicao = new JLabel("Ato Eleitoral Fechado");
    GridBagConstraints gbc_lblEstadoEleicao = new GridBagConstraints();
    gbc_lblEstadoEleicao.insets = new Insets(0, 0, 5, 0);
    gbc_lblEstadoEleicao.gridx = 0;
    gbc_lblEstadoEleicao.gridy = 2;
   
    add(lblEstadoEleicao, gbc_lblEstadoEleicao);

  }

  public JTabbedPane getTabbedPaneAssembleiaVoto() {
    return tabbedPaneAssembleiaVoto;
  }

  public CNEAbrirAtoEleitoralPanel getAbrirAtoEleitoralPanel() {
    return abrirAtoEleitoralPanel;
  }

  public JLabel getLblEstadoEleicao() {
    return lblEstadoEleicao;
  }

  public CNEResultadosPanel getResultadosPanel() {
    return resultadosPanel;
  }



}
