/**
 * 
 */
package dss.presentationlayer.components.separators;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JTabbedPane;
import java.awt.GridBagConstraints;
import dss.presentationlayer.components.panels.mandatario.MandatarioGerirCandidatoUninominalPanel;
import dss.presentationlayer.components.panels.mandatario.MandatarioConsultarCandidatoPanel;

/**
 * @author bpereira
 *
 */
public class MandatarioUninominalSeparator extends JPanel {


  /**
   * 
   */
  private static final long serialVersionUID = -801536921086999202L;
  private JTabbedPane tabbedPaneMandatarioUninominal;
  private MandatarioGerirCandidatoUninominalPanel mandatarioGerirCandidatoUninominalPanel;
  private MandatarioConsultarCandidatoPanel mandatarioConsultarCandidatoPanel;

  public MandatarioUninominalSeparator() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    tabbedPaneMandatarioUninominal = new JTabbedPane(JTabbedPane.LEFT);
    GridBagConstraints gbc_tabbedPaneMandatarioUninominal = new GridBagConstraints();
    gbc_tabbedPaneMandatarioUninominal.fill = GridBagConstraints.BOTH;
    gbc_tabbedPaneMandatarioUninominal.gridx = 0;
    gbc_tabbedPaneMandatarioUninominal.gridy = 0;
    gbc_tabbedPaneMandatarioUninominal.weightx = gbc_tabbedPaneMandatarioUninominal.weighty = 1.0;
    add(tabbedPaneMandatarioUninominal, gbc_tabbedPaneMandatarioUninominal);

    mandatarioGerirCandidatoUninominalPanel = new MandatarioGerirCandidatoUninominalPanel();
    tabbedPaneMandatarioUninominal.addTab("<html>Gerir<br> Candidato", null,
        mandatarioGerirCandidatoUninominalPanel, null);

    mandatarioConsultarCandidatoPanel = new MandatarioConsultarCandidatoPanel();
    tabbedPaneMandatarioUninominal.addTab("<html>Consultar<br>Candidato", null,
        mandatarioConsultarCandidatoPanel, null);
  }

  public JTabbedPane getTabbedPanemandatarioUninominal() {
    return tabbedPaneMandatarioUninominal;
  }

  public MandatarioGerirCandidatoUninominalPanel getMandatarioGerirCandidatoUninominalPanel() {
    return mandatarioGerirCandidatoUninominalPanel;
  }

  public MandatarioConsultarCandidatoPanel getMandatarioConsultarCandidatoPanel() {
    return mandatarioConsultarCandidatoPanel;
  }



}
