/**
 * 
 */
package dss.presentationlayer.components.separators;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import dss.presentationlayer.components.panels.assembleiavoto.AssembleiaVotoUrnaVirtualPanel;
import dss.presentationlayer.components.panels.assembleiavoto.AssembleiaVotoValidarEleitorPanel;

/**
 * @author bpereira
 *
 */
public class AssembleiaVotoSeparator extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = -1413650498092889432L;
  private JTabbedPane tabbedPaneAssembleiaVoto;
  private AssembleiaVotoUrnaVirtualPanel assembleiaVotoUrnaVirtualPanel;
  private AssembleiaVotoValidarEleitorPanel assembleiaVotoValidarEleitorPanel;
  private JLabel lblEstadoUrna;

  /**
   * Create the panel.
   */
  public AssembleiaVotoSeparator() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    tabbedPaneAssembleiaVoto = new JTabbedPane(JTabbedPane.LEFT);
    GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
    gbc_tabbedPane.insets = new Insets(0, 0, 5, 0);
    gbc_tabbedPane.fill = GridBagConstraints.BOTH;
    gbc_tabbedPane.gridx = 0;
    gbc_tabbedPane.gridy = 0;
    gbc_tabbedPane.weightx = gbc_tabbedPane.weighty = 1.0;
    add(tabbedPaneAssembleiaVoto, gbc_tabbedPane);

    assembleiaVotoUrnaVirtualPanel = new AssembleiaVotoUrnaVirtualPanel();
    tabbedPaneAssembleiaVoto.addTab("Urna Virtual", null, assembleiaVotoUrnaVirtualPanel, null);

    assembleiaVotoValidarEleitorPanel = new AssembleiaVotoValidarEleitorPanel();
    tabbedPaneAssembleiaVoto.addTab("Validar Eleitor", null, assembleiaVotoValidarEleitorPanel,
        null);

    lblEstadoUrna = new JLabel("Urna Virtual Fechada");
    GridBagConstraints gbc_lblEstadoUrna = new GridBagConstraints();
    gbc_lblEstadoUrna.insets = new Insets(0, 0, 5, 0);
    gbc_lblEstadoUrna.gridx = 0;
    gbc_lblEstadoUrna.gridy = 1;
   
    add(lblEstadoUrna, gbc_lblEstadoUrna);

  }

  /**
   * @return the lblEstadoUrna
   */
  public JLabel getLblEstadoUrna() {
    return lblEstadoUrna;
  }

  public JTabbedPane getTabbedPaneAssembleiaVoto() {
    return tabbedPaneAssembleiaVoto;
  }

  public AssembleiaVotoUrnaVirtualPanel getAssembleiaVotoUrnaVirtualPanel() {
    return assembleiaVotoUrnaVirtualPanel;
  }

  public AssembleiaVotoValidarEleitorPanel getAssembleiaVotoValidarEleitorPanel() {
    return assembleiaVotoValidarEleitorPanel;
  }



}
