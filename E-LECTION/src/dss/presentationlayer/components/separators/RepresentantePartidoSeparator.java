package dss.presentationlayer.components.separators;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JTabbedPane;
import java.awt.GridBagConstraints;
import dss.presentationlayer.components.panels.partido.RepresentantePartidoPanel;

public class RepresentantePartidoSeparator extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = 1823550767003349027L;
  /**
   * Create the panel.
   */
  private JTabbedPane tabbedPaneRepresentantePartido;
  private RepresentantePartidoPanel representantePartidoPanel;

  public RepresentantePartidoSeparator() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    tabbedPaneRepresentantePartido = new JTabbedPane(JTabbedPane.LEFT);
    GridBagConstraints gbc_tabbedPaneRepresentantePartido = new GridBagConstraints();
    gbc_tabbedPaneRepresentantePartido.fill = GridBagConstraints.BOTH;
    gbc_tabbedPaneRepresentantePartido.gridx = 0;
    gbc_tabbedPaneRepresentantePartido.gridy = 0;
    gbc_tabbedPaneRepresentantePartido.weightx = gbc_tabbedPaneRepresentantePartido.weighty = 1.0;
    add(tabbedPaneRepresentantePartido, gbc_tabbedPaneRepresentantePartido);

    representantePartidoPanel = new RepresentantePartidoPanel();
    tabbedPaneRepresentantePartido.addTab("Consultar", null, representantePartidoPanel, null);

  }

  public JTabbedPane getTabbedPaneRepresentantePartido() {
    return tabbedPaneRepresentantePartido;
  }

  public RepresentantePartidoPanel getRepresentantePartidoPanel() {
    return representantePartidoPanel;
  }



}
