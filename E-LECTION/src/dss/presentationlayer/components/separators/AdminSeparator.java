/**
 * 
 */
package dss.presentationlayer.components.separators;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import dss.presentationlayer.components.panels.admin.AdminAdicionarPanel;
import dss.presentationlayer.components.panels.admin.AdminConsultaPanel;
import dss.presentationlayer.components.panels.admin.AdminProcuraPanel;

/**
 * @author bpereira
 *
 */
public class AdminSeparator extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5537967955754103323L;
	private JTabbedPane tabbedPaneAssembleiaVoto;
	private AdminAdicionarPanel adminAdicionarPanel;
	private AdminProcuraPanel adminProcuraPanel;
	private AdminConsultaPanel adminConsultaPanel;

	/**
	 * Create the panel.
	 */
	public AdminSeparator() {

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		tabbedPaneAssembleiaVoto = new JTabbedPane(JTabbedPane.LEFT);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 0;
		 gbc_tabbedPane.weightx = gbc_tabbedPane.weighty = 1.0;
		add(tabbedPaneAssembleiaVoto, gbc_tabbedPane);

		adminAdicionarPanel = new AdminAdicionarPanel();
		tabbedPaneAssembleiaVoto.addTab("Adicionar", null, adminAdicionarPanel,
				null);

		adminProcuraPanel = new AdminProcuraPanel();
		tabbedPaneAssembleiaVoto.addTab("Procura", null, adminProcuraPanel,
				null);

		adminConsultaPanel = new AdminConsultaPanel();
		tabbedPaneAssembleiaVoto.addTab("Consulta", null, adminConsultaPanel,
				null);

	}

	/**
	 * @return the tabbedPaneAssembleiaVoto
	 */
	public JTabbedPane getTabbedPaneAssembleiaVoto() {
		return tabbedPaneAssembleiaVoto;
	}

	/**
	 * @return the adminAdicionarPanel
	 */
	public AdminAdicionarPanel getAdminAdicionarPanel() {
		return adminAdicionarPanel;
	}

	/**
	 * @return the adminProcuraPanel
	 */
	public AdminProcuraPanel getAdminProcuraPanel() {
		return adminProcuraPanel;
	}

	/**
	 * @return the adminConsultaPanel
	 */
	public AdminConsultaPanel getAdminConsultaPanel() {
		return adminConsultaPanel;
	}

}
