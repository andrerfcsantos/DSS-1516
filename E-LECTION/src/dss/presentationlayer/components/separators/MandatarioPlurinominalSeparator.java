/**
 * 
 */
package dss.presentationlayer.components.separators;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import dss.presentationlayer.components.panels.mandatario.MandatarioConsultarCandidatoPanel;
import dss.presentationlayer.components.panels.mandatario.MandatarioGerirCandidatoUninominalPanel;

/**
 * @author bpereira
 *
 */

public class MandatarioPlurinominalSeparator extends JPanel {



  /**
   * 
   */
  private static final long serialVersionUID = 4418665427709472859L;
  private JTabbedPane tabbedPaneMandatarioPlurinominal;
  private MandatarioGerirCandidatoUninominalPanel mandatarioGerirCandidatoUninominalPanel;
  private MandatarioConsultarCandidatoPanel mandatarioConsultarCandidatoPanel;

  public MandatarioPlurinominalSeparator() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0};
    gridBagLayout.rowHeights = new int[] {0};
    gridBagLayout.columnWeights = new double[] {Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {Double.MIN_VALUE};
    setLayout(gridBagLayout);
    tabbedPaneMandatarioPlurinominal = new JTabbedPane(JTabbedPane.LEFT);
    GridBagConstraints gbc_tabbedPaneMandatarioPlurinominal = new GridBagConstraints();
    gbc_tabbedPaneMandatarioPlurinominal.fill = GridBagConstraints.BOTH;
    gbc_tabbedPaneMandatarioPlurinominal.gridx = 0;
    gbc_tabbedPaneMandatarioPlurinominal.gridy = 0;
    gbc_tabbedPaneMandatarioPlurinominal.weightx = gbc_tabbedPaneMandatarioPlurinominal.weighty = 1.0;
    add(tabbedPaneMandatarioPlurinominal, gbc_tabbedPaneMandatarioPlurinominal);

    mandatarioGerirCandidatoUninominalPanel = new MandatarioGerirCandidatoUninominalPanel();
    tabbedPaneMandatarioPlurinominal.addTab("<html>Consultar<br>Gerar", null,
        mandatarioGerirCandidatoUninominalPanel, null);

    mandatarioConsultarCandidatoPanel = new MandatarioConsultarCandidatoPanel();
    tabbedPaneMandatarioPlurinominal.addTab("<html>Consultar<br>Candidato", null,
        mandatarioConsultarCandidatoPanel, null);

    
  }

  public JTabbedPane getTabbedPaneMandatarioPlurinominal() {
    return tabbedPaneMandatarioPlurinominal;
  }

  public MandatarioGerirCandidatoUninominalPanel getMandatarioGerirCandidatoUninominalPanel() {
    return mandatarioGerirCandidatoUninominalPanel;
  }

  public MandatarioConsultarCandidatoPanel getMandatarioConsultarCandidatoPanel() {
    return mandatarioConsultarCandidatoPanel;
  }
  
  



}
