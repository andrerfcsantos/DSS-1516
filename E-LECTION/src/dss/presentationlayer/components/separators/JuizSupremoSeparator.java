/**
 * 
 */
package dss.presentationlayer.components.separators;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import dss.presentationlayer.components.Consulta;

/**
 * @author bpereira
 *
 */
public class JuizSupremoSeparator extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = -7014245944159814428L;
  private JTabbedPane tabbedPaneTribunalSupremo;
  private Consulta consultaListaPorAprovar;
  private Consulta consultaListasRejeitadas;
  private JButton btnAprovar;
  private JButton btnRejeitar;
  private Consulta consultaListasAprovadas;

  /**
   * Create the panel.
   */
  public JuizSupremoSeparator() {

    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    tabbedPaneTribunalSupremo = new JTabbedPane(JTabbedPane.LEFT);
    GridBagConstraints gbc_tabbedPaneTribunalSupremo = new GridBagConstraints();
    gbc_tabbedPaneTribunalSupremo.fill = GridBagConstraints.BOTH;
    gbc_tabbedPaneTribunalSupremo.gridx = 0;
    gbc_tabbedPaneTribunalSupremo.gridy = 0;
    gbc_tabbedPaneTribunalSupremo.weightx = gbc_tabbedPaneTribunalSupremo.weighty = 1.0;
    add(tabbedPaneTribunalSupremo, gbc_tabbedPaneTribunalSupremo);



    consultaListasAprovadas = new Consulta();
    tabbedPaneTribunalSupremo.addTab("<html>Consultar Listas<br>Aprovadas</html>", null,
        consultaListasAprovadas, null);

    JPanel panel = new JPanel();
    tabbedPaneTribunalSupremo.addTab("<html>Consultar Listas<br>por Aprovar</html>", null, panel,
        null);
    GridBagLayout gbl_panel = new GridBagLayout();
    gbl_panel.columnWidths = new int[] {0, 0, 0, 0, 0};
    gbl_panel.rowHeights = new int[] {0, 0, 0, 0, 0};
    gbl_panel.columnWeights = new double[] {0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
    gbl_panel.rowWeights = new double[] {0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
    panel.setLayout(gbl_panel);

    consultaListaPorAprovar = new Consulta();
    GridBagConstraints gbc_consultaListaPorAprovar = new GridBagConstraints();
    gbc_consultaListaPorAprovar.gridheight = 2;
    gbc_consultaListaPorAprovar.gridwidth = 4;
    gbc_consultaListaPorAprovar.insets = new Insets(0, 0, 5, 5);
    gbc_consultaListaPorAprovar.fill = GridBagConstraints.BOTH;
    gbc_consultaListaPorAprovar.gridx = 0;
    gbc_consultaListaPorAprovar.gridy = 0;
    gbc_consultaListaPorAprovar.weightx = gbc_consultaListaPorAprovar.weighty = 1.0;
    panel.add(consultaListaPorAprovar, gbc_consultaListaPorAprovar);

    JButton btnAprovar = new JButton("Aprovar");
    GridBagConstraints gbc_btnAprovar = new GridBagConstraints();
    gbc_btnAprovar.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnAprovar.insets = new Insets(0, 0, 5, 5);
    gbc_btnAprovar.gridx = 1;
    gbc_btnAprovar.gridy = 2;
    gbc_btnAprovar.weightx =  1.0;
    panel.add(btnAprovar, gbc_btnAprovar);

    JButton btnRejeitar = new JButton("Rejeitar");
    GridBagConstraints gbc_btnRejeitar = new GridBagConstraints();
    gbc_btnRejeitar.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnRejeitar.insets = new Insets(0, 0, 5, 5);
    gbc_btnRejeitar.gridx = 2;
    gbc_btnRejeitar.gridy = 2;
    gbc_btnRejeitar.weightx = 1.0;
    panel.add(btnRejeitar, gbc_btnRejeitar);

    consultaListasRejeitadas = new Consulta();
    tabbedPaneTribunalSupremo.addTab("<html>Consultar Listas<br>rejeitadas</html>", null,
        consultaListasRejeitadas, null);

  }

  public JTabbedPane getTabbedPaneTribunalConstitucional() {
    return tabbedPaneTribunalSupremo;
  }

  public Consulta getConsultaListaPorAprovar() {
    return consultaListaPorAprovar;
  }

  public Consulta getConsultaListasRejeitadas() {
    return consultaListasRejeitadas;
  }

  public JButton getBtnAprovar() {
    return btnAprovar;
  }

  public JButton getBtnRejeitar() {
    return btnRejeitar;
  }

  public Consulta getConsultaListasAprovadas() {
    return consultaListasAprovadas;
  }

}
