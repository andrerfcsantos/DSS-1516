package dss.presentationlayer.components;

public class TextWrap {
  
  final private String html1 = "<html>";
  final private String html3 ="</html>";
  
  private String text;

  public TextWrap(String text) {
    super();
    this.text = html1+text+html3;
  }

  @Override
  public String toString() {
    return text.toString();
  }
  
  
  

}
