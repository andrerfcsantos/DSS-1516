package dss.presentationlayer.components;

import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.MutableComboBoxModel;

import dss.presentationlayer.components.panels.mandatario.BIdentidadePanel;
import dss.presentationlayer.components.panels.mandatario.CartaoCidadaoPanel;

public class Formulario extends JPanel {
	/**
   * 
   */

	private static String BI = "BI";
	private static String CC = "CC";

	private static final long serialVersionUID = -2352065025785262202L;
	private JTextField textFieldEleitor;
	private JTextField textFieldDia;
	private JTextField textFieldMes;
	private JTextField textFieldAno;
	private JTextField textFieldNaturalidade;
	private JTextField textFieldNomeProprio;
	private JTextField textFieldIdade;
	private JTextField textFieldProfissao;
	private JTextField textFieldFiliacao;
	private JComboBox<String> comboBoxIndentificacao;
	private CartaoCidadaoPanel cartaoCidadaoPanel;
	private BIdentidadePanel biIdentidadePanel;

	// private CardLayout identificacaoCard;

	/**
	 * Create the panel.
	 */
	public Formulario() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 40, 40, 40, 40, 40, 40, 40,
				40, 40, 75, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 33, 33, 33, 33, 80, 0, 33,
				68, 43, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0,
				0.0, 1.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel lblNroEleitor = new JLabel("Nro Eleitor");
		GridBagConstraints gbc_lblNroEleitor = new GridBagConstraints();
		gbc_lblNroEleitor.fill = GridBagConstraints.VERTICAL;
		gbc_lblNroEleitor.insets = new Insets(0, 0, 5, 5);
		gbc_lblNroEleitor.gridx = 1;
		gbc_lblNroEleitor.gridy = 1;
		gbc_lblNroEleitor.weightx = gbc_lblNroEleitor.weighty = 1.0;
		add(lblNroEleitor, gbc_lblNroEleitor);

		textFieldEleitor = new JTextField();
		GridBagConstraints gbc_textFieldEleitor = new GridBagConstraints();
		gbc_textFieldEleitor.gridwidth = 3;
		gbc_textFieldEleitor.fill = GridBagConstraints.BOTH;
		gbc_textFieldEleitor.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldEleitor.gridx = 2;
		gbc_textFieldEleitor.gridy = 1;
		gbc_textFieldEleitor.weightx = gbc_textFieldEleitor.weighty = 1.0;
		add(textFieldEleitor, gbc_textFieldEleitor);
		textFieldEleitor.setColumns(10);

		JLabel lblFiliacao = new JLabel("Filiação");
		GridBagConstraints gbc_lblFiliacao = new GridBagConstraints();
		gbc_lblFiliacao.fill = GridBagConstraints.BOTH;
		gbc_lblFiliacao.insets = new Insets(0, 0, 5, 5);
		gbc_lblFiliacao.gridx = 8;
		gbc_lblFiliacao.gridy = 1;
		add(lblFiliacao, gbc_lblFiliacao);

		textFieldFiliacao = new JTextField();
		GridBagConstraints gbc_textFieldFiliacao = new GridBagConstraints();
		gbc_textFieldFiliacao.fill = GridBagConstraints.BOTH;
		gbc_textFieldFiliacao.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldFiliacao.gridx = 9;
		gbc_textFieldFiliacao.gridy = 1;
		gbc_textFieldFiliacao.weightx = gbc_textFieldFiliacao.weighty = 1.0;
		add(textFieldFiliacao, gbc_textFieldFiliacao);
		textFieldFiliacao.setColumns(10);

		JLabel lblNomeProprio = new JLabel("Nome Próprio");
		GridBagConstraints gbc_lblNomeProprio = new GridBagConstraints();
		gbc_lblNomeProprio.fill = GridBagConstraints.VERTICAL;
		gbc_lblNomeProprio.insets = new Insets(0, 0, 5, 5);
		gbc_lblNomeProprio.gridx = 1;
		gbc_lblNomeProprio.gridy = 2;
		gbc_lblNomeProprio.weightx = gbc_lblNomeProprio.weighty = 1.0;
		add(lblNomeProprio, gbc_lblNomeProprio);

		textFieldNomeProprio = new JTextField();
		GridBagConstraints gbc_textFieldNomeProprio = new GridBagConstraints();
		gbc_textFieldNomeProprio.gridwidth = 3;
		gbc_textFieldNomeProprio.fill = GridBagConstraints.BOTH;
		gbc_textFieldNomeProprio.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldNomeProprio.gridx = 2;
		gbc_textFieldNomeProprio.gridy = 2;
		gbc_textFieldNomeProprio.weightx =gbc_textFieldNomeProprio.weighty = 1.0;
		add(textFieldNomeProprio, gbc_textFieldNomeProprio);
		textFieldNomeProprio.setColumns(10);

		JLabel lblIdade = new JLabel("Idade");
		GridBagConstraints gbc_lblIdade = new GridBagConstraints();
		gbc_lblIdade.fill = GridBagConstraints.BOTH;
		gbc_lblIdade.insets = new Insets(0, 0, 5, 5);
		gbc_lblIdade.gridx = 5;
		gbc_lblIdade.gridy = 2;
		gbc_lblIdade.weightx =gbc_lblIdade.weighty = 1.0;
		add(lblIdade, gbc_lblIdade);

		textFieldIdade = new JTextField();
		GridBagConstraints gbc_textFieldIdade = new GridBagConstraints();
		gbc_textFieldIdade.fill = GridBagConstraints.BOTH;
		gbc_textFieldIdade.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldIdade.gridx = 6;
		gbc_textFieldIdade.gridy = 2;
		gbc_textFieldIdade.weightx =gbc_textFieldIdade.weighty = 1.0;
		add(textFieldIdade, gbc_textFieldIdade);
		textFieldIdade.setColumns(10);

		JLabel lblProfissao = new JLabel("Profissão");
		GridBagConstraints gbc_lblProfissao = new GridBagConstraints();
		gbc_lblProfissao.fill = GridBagConstraints.BOTH;
		gbc_lblProfissao.insets = new Insets(0, 0, 5, 5);
		gbc_lblProfissao.gridx = 8;
		gbc_lblProfissao.gridy = 2;
		gbc_lblProfissao.weightx =gbc_lblProfissao.weighty = 1.0;
		add(lblProfissao, gbc_lblProfissao);

		textFieldProfissao = new JTextField();
		GridBagConstraints gbc_textFieldProfissao = new GridBagConstraints();
		gbc_textFieldProfissao.fill = GridBagConstraints.BOTH;
		gbc_textFieldProfissao.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldProfissao.gridx = 9;
		gbc_textFieldProfissao.gridy = 2;
		gbc_textFieldProfissao.weightx =gbc_lblProfissao.weighty = 1.0;
		add(textFieldProfissao, gbc_textFieldProfissao);
		textFieldProfissao.setColumns(10);

		JLabel lblDia = new JLabel("Dia");
		GridBagConstraints gbc_lblDia = new GridBagConstraints();
		gbc_lblDia.fill = GridBagConstraints.BOTH;
		gbc_lblDia.insets = new Insets(0, 0, 5, 5);
		gbc_lblDia.gridx = 2;
		gbc_lblDia.gridy = 3;
		gbc_lblDia.weightx =gbc_lblDia.weighty = 1.0;
		add(lblDia, gbc_lblDia);

		JLabel lblMes = new JLabel("Mês");
		GridBagConstraints gbc_lblMes = new GridBagConstraints();
		gbc_lblMes.fill = GridBagConstraints.BOTH;
		gbc_lblMes.insets = new Insets(0, 0, 5, 5);
		gbc_lblMes.gridx = 3;
		gbc_lblMes.gridy = 3;
		gbc_lblMes.weightx =gbc_lblMes.weighty = 1.0;
		add(lblMes, gbc_lblMes);

		JLabel lblAno = new JLabel("Ano");
		GridBagConstraints gbc_lblAno = new GridBagConstraints();
		gbc_lblAno.fill = GridBagConstraints.BOTH;
		gbc_lblAno.insets = new Insets(0, 0, 5, 5);
		gbc_lblAno.gridx = 4;
		gbc_lblAno.gridy = 3;
		gbc_lblAno.weightx =gbc_lblAno.weighty = 1.0;
		add(lblAno, gbc_lblAno);

		JLabel lblDataNasc = new JLabel("Data Nascimento");
		GridBagConstraints gbc_lblDataNasc = new GridBagConstraints();
		gbc_lblDataNasc.fill = GridBagConstraints.VERTICAL;
		gbc_lblDataNasc.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataNasc.gridx = 1;
		gbc_lblDataNasc.gridy = 4;
		gbc_lblDataNasc.weightx =gbc_lblDataNasc.weighty = 1.0;
		add(lblDataNasc, gbc_lblDataNasc);

		textFieldDia = new JTextField();
		GridBagConstraints gbc_textFieldDia = new GridBagConstraints();
		gbc_textFieldDia.fill = GridBagConstraints.BOTH;
		gbc_textFieldDia.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldDia.gridx = 2;
		gbc_textFieldDia.gridy = 4;
		gbc_textFieldDia.weightx =gbc_textFieldDia.weighty = 1.0;
		add(textFieldDia, gbc_textFieldDia);
		textFieldDia.setColumns(10);

		textFieldMes = new JTextField();
		GridBagConstraints gbc_textFieldMes = new GridBagConstraints();
		gbc_textFieldMes.fill = GridBagConstraints.BOTH;
		gbc_textFieldMes.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldMes.gridx = 3;
		gbc_textFieldMes.gridy = 4;
		gbc_textFieldMes.weightx =gbc_textFieldMes.weighty = 1.0;
		add(textFieldMes, gbc_textFieldMes);
		textFieldMes.setColumns(10);

		textFieldAno = new JTextField();
		GridBagConstraints gbc_textFieldAno = new GridBagConstraints();
		gbc_textFieldAno.fill = GridBagConstraints.BOTH;
		gbc_textFieldAno.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldAno.gridx = 4;
		gbc_textFieldAno.gridy = 4;
		gbc_textFieldAno.weightx =gbc_textFieldAno.weighty = 1.0;
		add(textFieldAno, gbc_textFieldAno);
		textFieldAno.setColumns(10);

		JLabel lblNaturalidade = new JLabel("Naturalidade");
		GridBagConstraints gbc_lblNaturalidade = new GridBagConstraints();
		gbc_lblNaturalidade.anchor = GridBagConstraints.EAST;
		gbc_lblNaturalidade.gridwidth = 3;
		gbc_lblNaturalidade.fill = GridBagConstraints.VERTICAL;
		gbc_lblNaturalidade.insets = new Insets(0, 0, 5, 5);
		gbc_lblNaturalidade.gridx = 5;
		gbc_lblNaturalidade.gridy = 4;
		gbc_lblNaturalidade.weightx =gbc_lblNaturalidade.weighty = 1.0;
		add(lblNaturalidade, gbc_lblNaturalidade);

		textFieldNaturalidade = new JTextField();
		GridBagConstraints gbc_textFieldNaturalidade = new GridBagConstraints();
		gbc_textFieldNaturalidade.gridwidth = 2;
		gbc_textFieldNaturalidade.fill = GridBagConstraints.BOTH;
		gbc_textFieldNaturalidade.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldNaturalidade.gridx = 8;
		gbc_textFieldNaturalidade.gridy = 4;
		gbc_textFieldNaturalidade.weightx =gbc_textFieldNaturalidade.weighty = 1.0;
		add(textFieldNaturalidade, gbc_textFieldNaturalidade);
		textFieldNaturalidade.setColumns(10);

		JLabel lblResidencia = new JLabel("Residência");
		GridBagConstraints gbc_lblResidencia = new GridBagConstraints();
		gbc_lblResidencia.fill = GridBagConstraints.BOTH;
		gbc_lblResidencia.insets = new Insets(0, 0, 5, 5);
		gbc_lblResidencia.gridx = 1;
		gbc_lblResidencia.gridy = 5;
		gbc_lblResidencia.weightx =gbc_lblResidencia.weighty = 1.0;
		add(lblResidencia, gbc_lblResidencia);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridheight = 2;
		gbc_scrollPane.gridwidth = 8;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.gridx = 2;
		gbc_scrollPane.gridy = 5;
		gbc_scrollPane.weightx =gbc_scrollPane.weighty = 1.0;
		add(scrollPane, gbc_scrollPane);

		JTextArea textAreaResidencia = new JTextArea();
		scrollPane.setViewportView(textAreaResidencia);

		String[] identificacaoStrings = { "Bilhete Identidade",
				"Cartão Cidadão" };
		MutableComboBoxModel<String> comboModel = new DefaultComboBoxModel<>(
				identificacaoStrings);

		comboBoxIndentificacao = new JComboBox<String>(comboModel);
		GridBagConstraints gbc_comboBoxIndentificacao = new GridBagConstraints();
		gbc_comboBoxIndentificacao.gridwidth = 4;
		gbc_comboBoxIndentificacao.fill = GridBagConstraints.BOTH;
		gbc_comboBoxIndentificacao.insets = new Insets(0, 0, 5, 5);
		gbc_comboBoxIndentificacao.gridx = 1;
		gbc_comboBoxIndentificacao.gridy = 7;
		gbc_comboBoxIndentificacao.weightx =gbc_comboBoxIndentificacao.weighty = 1.0;
		add(comboBoxIndentificacao, gbc_comboBoxIndentificacao);

		JPanel panelAux = new JPanel();
		GridBagConstraints gbc_panelAux = new GridBagConstraints();
		gbc_panelAux.gridheight = 3;
		gbc_panelAux.gridwidth = 8;
		gbc_panelAux.fill = GridBagConstraints.BOTH;
		gbc_panelAux.insets = new Insets(0, 0, 0, 5);
		gbc_panelAux.gridx = 1;
		gbc_panelAux.gridy = 8;
		gbc_panelAux.weightx =gbc_panelAux.weighty = 1.0;
		add(panelAux, gbc_panelAux);
		panelAux.setLayout(new GridLayout(1, 0, 0, 0));

		JPanel panelIdentificacao = new JPanel();
		panelAux.add(panelIdentificacao);

		biIdentidadePanel = new BIdentidadePanel();
		cartaoCidadaoPanel = new CartaoCidadaoPanel();
		GridBagConstraints gbc_identidadePanel = new GridBagConstraints();
		gbc_identidadePanel.fill = GridBagConstraints.BOTH;
		gbc_identidadePanel.gridx = 0;
		gbc_identidadePanel.gridy = 0;
		gbc_identidadePanel.weightx =gbc_identidadePanel.weighty = 1.0;

		CardLayout identificacaoCard = new CardLayout();

		panelIdentificacao.setLayout(identificacaoCard);

		panelIdentificacao.add(biIdentidadePanel, BI);

		panelIdentificacao.add(cartaoCidadaoPanel, CC);
		identificacaoCard.show(panelIdentificacao, BI);

		comboBoxIndentificacao.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				switch (comboBoxIndentificacao.getSelectedIndex()) {
				case 0:

					identificacaoCard.show(panelIdentificacao, BI);
					cartaoCidadaoPanel.limparTextFields();
					break;
				case 1:
					identificacaoCard.show(panelIdentificacao, CC);
					biIdentidadePanel.limparTextFields();
					break;

				default:
					break;
				}

			}
		});

	}

	public JTextField getTextFieldEleitor() {
		return textFieldEleitor;
	}

	public JTextField getTextFieldDia() {
		return textFieldDia;
	}

	public JTextField getTextFieldMes() {
		return textFieldMes;
	}

	public JTextField getTextFieldAno() {
		return textFieldAno;
	}

	public JTextField getTextFieldNaturalidade() {
		return textFieldNaturalidade;
	}

	public JTextField getTextFieldNomeProprio() {
		return textFieldNomeProprio;
	}

	public JTextField getTextFieldIdade() {
		return textFieldIdade;
	}

	public JTextField getTextFieldProfissao() {
		return textFieldProfissao;
	}

	public JTextField getTextFieldFiliacao() {
		return textFieldFiliacao;
	}

	public JComboBox<String> getComboBoxIndentificacao() {
		return comboBoxIndentificacao;
	}

	public void limparTextFields() {

		textFieldEleitor.setText("");
		textFieldDia.setText("");
		textFieldMes.setText("");
		textFieldAno.setText("");
		textFieldNaturalidade.setText("");
		textFieldNomeProprio.setText("");
		textFieldIdade.setText("");
		textFieldProfissao.setText("");
		textFieldFiliacao.setText("");
		cartaoCidadaoPanel.limparTextFields();
		biIdentidadePanel.limparTextFields();

	}

}
