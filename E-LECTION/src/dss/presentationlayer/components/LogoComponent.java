package dss.presentationlayer.components;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class LogoComponent extends ImageIcon {

  
  private static final long serialVersionUID = -8403871556293862198L;
  /**
   * 
   */
  

  public BufferedImage image;
  
 
  public LogoComponent() throws IOException {
    super();
    
   
    this.image = ImageIO.read(new File("img/logo.png"));
    
    
    
    this.setImage(image);
  }

  



}
