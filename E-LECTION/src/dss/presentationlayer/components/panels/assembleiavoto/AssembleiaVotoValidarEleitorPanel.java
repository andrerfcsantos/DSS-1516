/**
 * 
 */
package dss.presentationlayer.components.panels.assembleiavoto;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * @author bpereira
 *
 */
public class AssembleiaVotoValidarEleitorPanel extends JPanel {
  
  /**
   * 
   */
  private static final long serialVersionUID = -1630692797213474814L;
  private JTextField textFieldNroEleitor;
  private JButton btnConsultar;
  private JButton btnLimpar;
  private JTextArea textAreaDetalhes;

  /**
   * Create the panel.
   */
  public AssembleiaVotoValidarEleitorPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0, 0, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JLabel lblNroEleitor = new JLabel("Nro Eleitor");
    GridBagConstraints gbc_lblNroEleitor = new GridBagConstraints();
    gbc_lblNroEleitor.anchor = GridBagConstraints.WEST;
    gbc_lblNroEleitor.insets = new Insets(0, 0, 5, 5);
    gbc_lblNroEleitor.gridx = 1;
    gbc_lblNroEleitor.gridy = 1;
    gbc_lblNroEleitor.weightx = gbc_lblNroEleitor.weighty = 1.0;
    add(lblNroEleitor, gbc_lblNroEleitor);

    btnConsultar = new JButton("Consultar");
    GridBagConstraints gbc_btnConsultar = new GridBagConstraints();
    gbc_btnConsultar.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnConsultar.insets = new Insets(0, 0, 5, 5);
    gbc_btnConsultar.gridx = 2;
    gbc_btnConsultar.gridy = 1;
    gbc_btnConsultar.weightx = gbc_btnConsultar.weighty = 1.0;
    add(btnConsultar, gbc_btnConsultar);

    textFieldNroEleitor = new JTextField();
    GridBagConstraints gbc_textFieldNroEleitor = new GridBagConstraints();
    gbc_textFieldNroEleitor.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldNroEleitor.fill = GridBagConstraints.HORIZONTAL;
    gbc_textFieldNroEleitor.gridx = 1;
    gbc_textFieldNroEleitor.gridy = 2;
    gbc_textFieldNroEleitor.weightx = gbc_textFieldNroEleitor.weighty = 1.0;
    add(textFieldNroEleitor, gbc_textFieldNroEleitor);
    textFieldNroEleitor.setColumns(10);

    btnLimpar = new JButton("Limpar");
    GridBagConstraints gbc_btnLimpar = new GridBagConstraints();
    gbc_btnLimpar.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnLimpar.insets = new Insets(0, 0, 5, 5);
    gbc_btnLimpar.gridx = 2;
    gbc_btnLimpar.gridy = 2;
    gbc_btnLimpar.weightx = gbc_btnLimpar.weighty = 1.0;
    add(btnLimpar, gbc_btnLimpar);

    JScrollPane scrollPaneDetalhes = new JScrollPane();
    GridBagConstraints gbc_scrollPaneDetalhes = new GridBagConstraints();
    gbc_scrollPaneDetalhes.gridwidth = 2;
    gbc_scrollPaneDetalhes.insets = new Insets(0, 0, 5, 5);
    gbc_scrollPaneDetalhes.fill = GridBagConstraints.BOTH;
    gbc_scrollPaneDetalhes.gridx = 1;
    gbc_scrollPaneDetalhes.gridy = 3;
    gbc_scrollPaneDetalhes.weightx = gbc_scrollPaneDetalhes.weighty = 1.0;
    add(scrollPaneDetalhes, gbc_scrollPaneDetalhes);

    textAreaDetalhes = new JTextArea();
    scrollPaneDetalhes.setViewportView(textAreaDetalhes);

    JLabel lblDetalhes = new JLabel("Detalhes");
    scrollPaneDetalhes.setColumnHeaderView(lblDetalhes);

    JButton btnAbrirBoletim = new JButton("Abrir Boletim");
    GridBagConstraints gbc_btnAbrirBoletim = new GridBagConstraints();
    gbc_btnAbrirBoletim.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnAbrirBoletim.insets = new Insets(0, 0, 5, 5);
    gbc_btnAbrirBoletim.gridx = 2;
    gbc_btnAbrirBoletim.gridy = 4;
    gbc_btnAbrirBoletim.weightx = gbc_btnAbrirBoletim.weighty = 1.0;
    add(btnAbrirBoletim, gbc_btnAbrirBoletim);

  }

  public JTextField getTextFieldNroEleitor() {
    return textFieldNroEleitor;
  }

  public JButton getBtnConsultar() {
    return btnConsultar;
  }

  public JButton getBtnLimpar() {
    return btnLimpar;
  }

  public JTextArea getTextAreaDetalhes() {
    return textAreaDetalhes;
  }



}
