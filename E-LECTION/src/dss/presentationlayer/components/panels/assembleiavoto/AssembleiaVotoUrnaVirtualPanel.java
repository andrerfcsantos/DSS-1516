/**
 * 
 */
package dss.presentationlayer.components.panels.assembleiavoto;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * @author bpereira
 *
 */
public class AssembleiaVotoUrnaVirtualPanel extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = 5590519830457316683L;
  private JButton btnFecharUrna;
  private JButton btnAbrirUrna;


  public AssembleiaVotoUrnaVirtualPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {73, 325, 71, 0};
    gridBagLayout.rowHeights = new int[] {50, 50, 50, 50, 50, 50, 0};
    gridBagLayout.columnWeights = new double[] {0.0, 1.0, 0.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    btnAbrirUrna = new JButton("Abrir Urna Virtual");
    GridBagConstraints gbc_btnAbrirUrna = new GridBagConstraints();
    gbc_btnAbrirUrna.fill = GridBagConstraints.BOTH;
    gbc_btnAbrirUrna.insets = new Insets(0, 0, 5, 5);
    gbc_btnAbrirUrna.gridx = 1;
    gbc_btnAbrirUrna.gridy = 2;
    gbc_btnAbrirUrna.weightx = gbc_btnAbrirUrna.weighty = 1.0;
    add(btnAbrirUrna, gbc_btnAbrirUrna);

    btnFecharUrna = new JButton("Fechar Urna Virtual");
    GridBagConstraints gbc_btnFecharUrna = new GridBagConstraints();
    gbc_btnFecharUrna.fill = GridBagConstraints.BOTH;
    gbc_btnFecharUrna.insets = new Insets(0, 0, 5, 5);
    gbc_btnFecharUrna.gridx = 1;
    gbc_btnFecharUrna.gridy = 3;
    gbc_btnFecharUrna.weightx = gbc_btnFecharUrna.weighty = 1.0;
    add(btnFecharUrna, gbc_btnFecharUrna);

  }



  /**
   * @return the btnFecharUrna
   */
  public JButton getBtnFecharUrna() {
    return btnFecharUrna;
  }


  /**
   * @return the btnAbrirUrna
   */
  public JButton getBtnAbrirUrna() {
    return btnAbrirUrna;
  }



}
