package dss.presentationlayer.components.panels.login;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;
import javax.swing.Box;


public class Login extends JPanel {
  /**
  * 
  */
  private static final long serialVersionUID = -3309154767299939908L;
  /**
   * 
   */

  private JTextField textUsername;
  private JPasswordField textPassword;
  private JButton btnOK;
  private JButton btnCancelar;


  /**
   * Create the panel.
   */
  public Login() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {39, 70, 129, 117, 0, 0};
    gridBagLayout.rowHeights = new int[] {37, 31, 25, 147, 25, 0, 0};
    gridBagLayout.columnWeights = new double[] {0.0, 0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JLabel lblNewLabel = new JLabel("Nome Utilizador");
    GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
    gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
    gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
    gbc_lblNewLabel.gridx = 1;
    gbc_lblNewLabel.gridy = 1;
    add(lblNewLabel, gbc_lblNewLabel);

    textUsername = new JTextField();
    GridBagConstraints gbc_textUsername = new GridBagConstraints();
    gbc_textUsername.anchor = GridBagConstraints.SOUTH;
    gbc_textUsername.fill = GridBagConstraints.HORIZONTAL;
    gbc_textUsername.insets = new Insets(0, 0, 5, 5);
    gbc_textUsername.gridwidth = 2;
    gbc_textUsername.gridx = 2;
    gbc_textUsername.gridy = 1;
    gbc_textUsername.weightx = 1.0;
    add(textUsername, gbc_textUsername);
    textUsername.setColumns(10);

    JLabel lblPassword = new JLabel("Palavra-passe");
    GridBagConstraints gbc_lblPassword = new GridBagConstraints();
    gbc_lblPassword.anchor = GridBagConstraints.NORTHWEST;
    gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
    gbc_lblPassword.gridx = 1;
    gbc_lblPassword.gridy = 2;
    //gbc_lblPassword.weightx;
    add(lblPassword, gbc_lblPassword);

    textPassword = new JPasswordField();
    GridBagConstraints gbc_textPassword = new GridBagConstraints();
    gbc_textPassword.fill = GridBagConstraints.BOTH;
    gbc_textPassword.insets = new Insets(0, 0, 5, 5);
    gbc_textPassword.gridwidth = 2;
    gbc_textPassword.gridx = 2;
    gbc_textPassword.gridy = 2;
    gbc_textPassword.weightx= 1.0;
    add(textPassword, gbc_textPassword);
    textPassword.setColumns(10);

    Component verticalStrut = Box.createVerticalStrut(20);
    GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
    gbc_verticalStrut.fill = GridBagConstraints.VERTICAL;
    gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
    gbc_verticalStrut.gridx = 2;
    gbc_verticalStrut.gridy = 3;
    add(verticalStrut, gbc_verticalStrut);

    btnOK = new JButton("Ok");
    GridBagConstraints gbc_btnOK = new GridBagConstraints();
    gbc_btnOK.anchor = GridBagConstraints.NORTH;
    gbc_btnOK.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnOK.insets = new Insets(0, 0, 5, 5);
    gbc_btnOK.gridx = 2;
    gbc_btnOK.gridy = 4;
    gbc_btnOK.weightx = gbc_btnOK.weighty = 1.0;
    add(btnOK, gbc_btnOK);

    btnCancelar = new JButton("Cancelar");
    GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
    gbc_btnCancelar.insets = new Insets(0, 0, 5, 5);
    gbc_btnCancelar.anchor = GridBagConstraints.NORTH;
    gbc_btnCancelar.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnCancelar.gridx = 3;
    gbc_btnCancelar.gridy = 4;
    gbc_btnCancelar.weightx = 1.0;
    add(btnCancelar, gbc_btnCancelar);

  }


  public JTextField getTextUsername() {
    return textUsername;
  }


  public JPasswordField getTextPassword() {
    return textPassword;
  }


  public JButton getBtnOK() {
    return btnOK;
  }


  public JButton getBtnCancelar() {
    return btnCancelar;
  }


}
