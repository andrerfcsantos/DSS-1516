package dss.presentationlayer.components.panels.cne;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class CNENroVotosNulosPanel extends JPanel {
  

  /**
   * 
   */
  private static final long serialVersionUID = 8144212884495288356L;
  private JList <String>listVotosNulos;
  private JLabel lblTotalVotosNulos;

  /**
   * Create the panel.
   */
  public CNENroVotosNulosPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0, 0, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JScrollPane scrollPane = new JScrollPane();
    GridBagConstraints gbc_scrollPane = new GridBagConstraints();
    gbc_scrollPane.gridwidth = 4;
    gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
    gbc_scrollPane.fill = GridBagConstraints.BOTH;
    gbc_scrollPane.gridx = 0;
    gbc_scrollPane.gridy = 0;
    add(scrollPane, gbc_scrollPane);



    String[] items = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7"};

    // TODO Implementar Observer e modelo tem que ser variável da classe
    DefaultListModel<String> model = new DefaultListModel<>();


    for (String string : items) {

      model.addElement(string);

    }
    listVotosNulos = new JList<>();
    listVotosNulos.setModel(model);


    scrollPane.setViewportView(listVotosNulos);

    JLabel lblNewLabel = new JLabel("Circulos");
    scrollPane.setColumnHeaderView(lblNewLabel);

    JLabel lblTotal = new JLabel("Total :");
    GridBagConstraints gbc_lblTotal = new GridBagConstraints();
    gbc_lblTotal.anchor = GridBagConstraints.EAST;
    gbc_lblTotal.insets = new Insets(0, 0, 5, 5);
    gbc_lblTotal.gridx = 0;
    gbc_lblTotal.gridy = 1;
    add(lblTotal, gbc_lblTotal);

    lblTotalVotosNulos = new JLabel("0");
    GridBagConstraints gbc_lblTotalVotosNulos = new GridBagConstraints();
    gbc_lblTotalVotosNulos.anchor = GridBagConstraints.WEST;
    gbc_lblTotalVotosNulos.insets = new Insets(0, 0, 5, 5);
    gbc_lblTotalVotosNulos.gridx = 1;
    gbc_lblTotalVotosNulos.gridy = 1;
    add(lblTotalVotosNulos, gbc_lblTotalVotosNulos);

  }

  public JList<String> getListNroVotantes() {
    return listVotosNulos;
  }

  public JLabel getLblTotalDeputados() {
    return lblTotalVotosNulos;
  }
}
