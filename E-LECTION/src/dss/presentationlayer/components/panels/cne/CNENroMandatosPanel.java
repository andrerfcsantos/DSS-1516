package dss.presentationlayer.components.panels.cne;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

public class CNENroMandatosPanel extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = 2354961093902655366L;
  private JLabel lblTotalDeputados;
  private JList<String> listCirculosEleitos;
  private JTextArea textAreaPartidosEleitos;

  /**
   * Create the panel.
   */
  public CNENroMandatosPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[]{0, 104, 235, 0, 0};
    gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);
    
    JScrollPane scrollPane = new JScrollPane();
    GridBagConstraints gbc_scrollPane = new GridBagConstraints();
    gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
    gbc_scrollPane.fill = GridBagConstraints.BOTH;
    gbc_scrollPane.gridx = 1;
    gbc_scrollPane.gridy = 1;
    add(scrollPane, gbc_scrollPane);
    
    JLabel lblCirculos = new JLabel("Circulo");
    scrollPane.setColumnHeaderView(lblCirculos);
    
   
    
    String [] items = {
        "Item 1",
        "Item 2", 
        "Item 3", 
        "Item 4", 
        "Item 5", 
        "Item 6", 
        "Item 7"};
    
    //TODO Implementar Observer e modelo tem que ser variável da classe
    DefaultListModel<String> model = new DefaultListModel<>();
    
    
    for (String string : items) {
        
        model.addElement(string);
        
    }
    listCirculosEleitos = new JList<>();
    listCirculosEleitos.setModel(model);
    
    
    scrollPane.setViewportView(listCirculosEleitos);
    
    JScrollPane scrollPane_1 = new JScrollPane();
    GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
    gbc_scrollPane_1.insets = new Insets(0, 0, 5, 5);
    gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
    gbc_scrollPane_1.gridx = 2;
    gbc_scrollPane_1.gridy = 1;
    add(scrollPane_1, gbc_scrollPane_1);
    
    JLabel lblPartidos = new JLabel("Partidos/Coligações");
    scrollPane_1.setColumnHeaderView(lblPartidos);
    
    textAreaPartidosEleitos = new JTextArea();
    scrollPane_1.setViewportView(textAreaPartidosEleitos);
    
    JPanel panel = new JPanel();
    panel.setBorder(new TitledBorder(null, "Total", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    GridBagConstraints gbc_panel = new GridBagConstraints();
    gbc_panel.gridwidth = 3;
    gbc_panel.fill = GridBagConstraints.BOTH;
    gbc_panel.gridx = 1;
    gbc_panel.gridy = 2;
    add(panel, gbc_panel);
    GridBagLayout gbl_panel = new GridBagLayout();
    gbl_panel.columnWidths = new int[]{0, 0};
    gbl_panel.rowHeights = new int[]{0, 0};
    gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
    gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
    panel.setLayout(gbl_panel);
    
    lblTotalDeputados = new JLabel("0");
    GridBagConstraints gbc_lblTotalDeputados = new GridBagConstraints();
    gbc_lblTotalDeputados.gridx = 0;
    gbc_lblTotalDeputados.gridy = 0;
    panel.add(lblTotalDeputados, gbc_lblTotalDeputados);

  }

  public JLabel getLblTotalDeputados() {
    return lblTotalDeputados;
  }

  public JList<String> getListCirculosEleitos() {
    return listCirculosEleitos;
  }

  public JTextArea getTextAreaPartidosEleitos() {
    return textAreaPartidosEleitos;
  }
  
  

}
