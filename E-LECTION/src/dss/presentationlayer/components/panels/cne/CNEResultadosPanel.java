package dss.presentationlayer.components.panels.cne;

import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class CNEResultadosPanel extends JPanel {

  /**
   * 
   */

  static String E_PR = "PR";
  static String E_AR = "AR";
  static String E_BK = "BLOCKED";


  private JButton btnGerarMapaEleitoral;
  private JButton btnGuardarMapaEleitoral;
  private JButton btnGuardarDocumento;
  private CardLayout cardResultados;
  
  private CNEResultadosLegislativasPanel resultadosAR;
  private CNEResultadosPresidenciaisPanel resultadosPR;
  private JPanel panelResultados;

  /**
   * Create the panel.
   */
  public CNEResultadosPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0, 0, 0};
    gridBagLayout.rowHeights = new int[] {215, 0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, 1.0, 1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    panelResultados = new JPanel();
    GridBagConstraints gbc_panelResultados = new GridBagConstraints();
    gbc_panelResultados.gridwidth = 3;
    gbc_panelResultados.insets = new Insets(0, 0, 5, 0);
    gbc_panelResultados.fill = GridBagConstraints.BOTH;
    gbc_panelResultados.gridx = 0;
    gbc_panelResultados.gridy = 0;
    add(panelResultados, gbc_panelResultados);
    
    cardResultados = new CardLayout(0, 0);
    panelResultados.setLayout(cardResultados);

    btnGerarMapaEleitoral = new JButton("<html>Gerar Mapa<br> Eleitoral</html>");
    GridBagConstraints gbc_btnGerarMapaEleitoral = new GridBagConstraints();
    gbc_btnGerarMapaEleitoral.fill = GridBagConstraints.BOTH;
    gbc_btnGerarMapaEleitoral.insets = new Insets(0, 0, 0, 5);
    gbc_btnGerarMapaEleitoral.gridx = 0;
    gbc_btnGerarMapaEleitoral.gridy = 1;
    add(btnGerarMapaEleitoral, gbc_btnGerarMapaEleitoral);

    btnGuardarMapaEleitoral = new JButton("<html>Guardar Mapa<br> Eleitoral</html>");
    GridBagConstraints gbc_btnGuardarMapaEleitoral = new GridBagConstraints();
    gbc_btnGuardarMapaEleitoral.fill = GridBagConstraints.BOTH;
    gbc_btnGuardarMapaEleitoral.insets = new Insets(0, 0, 0, 5);
    gbc_btnGuardarMapaEleitoral.gridx = 1;
    gbc_btnGuardarMapaEleitoral.gridy = 1;
    add(btnGuardarMapaEleitoral, gbc_btnGuardarMapaEleitoral);

    btnGuardarDocumento = new JButton("<html>Guardar<br> Documento</html>");
    GridBagConstraints gbc_btnGuardarDocumento = new GridBagConstraints();
    gbc_btnGuardarDocumento.fill = GridBagConstraints.BOTH;
    gbc_btnGuardarDocumento.gridx = 2;
    gbc_btnGuardarDocumento.gridy = 1;
    add(btnGuardarDocumento, gbc_btnGuardarDocumento);

    CNEResultadosLegislativasPanel resultadosAR = new CNEResultadosLegislativasPanel();
    CNEResultadosPresidenciaisPanel resultadosPR = new CNEResultadosPresidenciaisPanel();
    JPanel empty = new JPanel();
    panelResultados.add(empty, E_BK);
    panelResultados.add(resultadosAR, E_AR);
    panelResultados.add(resultadosPR, E_PR);

  }

  public JButton getBtnGerarMapaEleitoral() {
    return btnGerarMapaEleitoral;
  }

  public JButton getBtnGuardarMapaEleitoral() {
    return btnGuardarMapaEleitoral;
  }

  public JButton getBtnGuardarDocumento() {
    return btnGuardarDocumento;
  }

  
  
  public void setEleicaoPRPorDefeito() {
    cardResultados.show(panelResultados, E_BK);
  }

  public void setEleicaoPRMaioriaAbsoluta() {
    cardResultados.show(panelResultados, E_AR);
  }

  public void setEleicaoSemMaioria() {
    cardResultados.show(panelResultados, E_PR);
  }

  public CNEResultadosLegislativasPanel getResultadosAR() {
    return resultadosAR;
  }

  public CNEResultadosPresidenciaisPanel getResultadosPR() {
    return resultadosPR;
  }

  
  



}
