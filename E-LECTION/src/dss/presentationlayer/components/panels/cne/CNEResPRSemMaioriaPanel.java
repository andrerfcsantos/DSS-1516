package dss.presentationlayer.components.panels.cne;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import dss.presentationlayer.components.TextWrap;
import java.awt.Insets;

public class CNEResPRSemMaioriaPanel extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = -8915237470536813722L;
  /**
   * 
   */
 



  private String candidato1 = "";
  private String candidato2 = "";



  private JLabel lblCandidato1;



  private JLabel lblCandidato2;

  /**
   * Create the panel.
   */
  public CNEResPRSemMaioriaPanel() {


    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JPanel panel_3 = new JPanel();
    panel_3.setBorder(new TitledBorder(null, "Não houve maioria", TitledBorder.LEADING,
        TitledBorder.TOP, null, Color.RED));
    GridBagConstraints gbc_panel_3 = new GridBagConstraints();
    gbc_panel_3.fill = GridBagConstraints.BOTH;
    gbc_panel_3.gridx = 0;
    gbc_panel_3.gridy = 0;
    add(panel_3, gbc_panel_3);
    GridBagLayout gbl_panel_3 = new GridBagLayout();
    gbl_panel_3.columnWidths = new int[] {0, 0};
    gbl_panel_3.rowHeights = new int[] {0, 0, 0};
    gbl_panel_3.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gbl_panel_3.rowWeights = new double[] {1.0, 1.0, Double.MIN_VALUE};
    panel_3.setLayout(gbl_panel_3);

    JPanel panel2Sufragio = new JPanel();
    GridBagConstraints gbc_panel2Sufragio = new GridBagConstraints();
    gbc_panel2Sufragio.fill = GridBagConstraints.BOTH;
    gbc_panel2Sufragio.gridheight = 2;
    gbc_panel2Sufragio.insets = new Insets(0, 0, 5, 0);
    gbc_panel2Sufragio.gridx = 0;
    gbc_panel2Sufragio.gridy = 0;
    panel_3.add(panel2Sufragio, gbc_panel2Sufragio);
    panel2Sufragio.setBorder(
        new TitledBorder(null, "2º Sufrágio", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    GridBagLayout gbl_panel2Sufragio = new GridBagLayout();
    gbl_panel2Sufragio.columnWidths = new int[] {0, 0};
    gbl_panel2Sufragio.rowHeights = new int[] {0, 0, 0};
    gbl_panel2Sufragio.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gbl_panel2Sufragio.rowWeights = new double[] {1.0, 1.0, Double.MIN_VALUE};
    panel2Sufragio.setLayout(gbl_panel2Sufragio);

    /**
     * 1 Candidato
     */


    JPanel panel1Candidato = new JPanel();
    panel1Candidato.setBorder(new TitledBorder(null, "1º Candidato", TitledBorder.LEADING,
        TitledBorder.TOP, null, Color.BLUE));
    GridBagConstraints gbc_panel1Candidato = new GridBagConstraints();
    gbc_panel1Candidato.fill = GridBagConstraints.BOTH;
    gbc_panel1Candidato.insets = new Insets(0, 0, 5, 0);
    gbc_panel1Candidato.gridx = 0;
    gbc_panel1Candidato.gridy = 0;
    panel2Sufragio.add(panel1Candidato, gbc_panel1Candidato);
    GridBagLayout gbl_panel1Candidato = new GridBagLayout();
    gbl_panel1Candidato.columnWidths = new int[] {0, 0};
    gbl_panel1Candidato.rowHeights = new int[] {0, 0};
    gbl_panel1Candidato.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gbl_panel1Candidato.rowWeights = new double[] {1.0, Double.MIN_VALUE};
    panel1Candidato.setLayout(gbl_panel1Candidato);

    JPanel panelNomeCandidato1 = new JPanel();
    GridBagConstraints gbc_panelNomeCandidato1 = new GridBagConstraints();
    gbc_panelNomeCandidato1.fill = GridBagConstraints.BOTH;
    gbc_panelNomeCandidato1.gridx = 0;
    gbc_panelNomeCandidato1.gridy = 0;
    panel1Candidato.add(panelNomeCandidato1, gbc_panelNomeCandidato1);
    panelNomeCandidato1.setLayout(new BoxLayout(panelNomeCandidato1, BoxLayout.X_AXIS));


    JPanel panelWraperNomeCandidato1 = new JPanel();
    panelNomeCandidato1.add(panelWraperNomeCandidato1);
    panelWraperNomeCandidato1.setLayout(new BoxLayout(panelWraperNomeCandidato1, BoxLayout.X_AXIS));

    lblCandidato1 = new JLabel(candidato1);
    panelWraperNomeCandidato1.add(lblCandidato1);


    /**
     * 2 Candidato
     */

    JPanel panel2Candidato = new JPanel();
    panel2Candidato.setBorder(new TitledBorder(null, "2º Candidato", TitledBorder.LEADING,
        TitledBorder.TOP, null, Color.BLUE));
    GridBagConstraints gbc_panel2Candidato = new GridBagConstraints();
    gbc_panel2Candidato.fill = GridBagConstraints.BOTH;
    gbc_panel2Candidato.gridx = 0;
    gbc_panel2Candidato.gridy = 1;
    panel2Sufragio.add(panel2Candidato, gbc_panel2Candidato);
    GridBagLayout gbl_panel2Candidato = new GridBagLayout();
    gbl_panel2Candidato.columnWidths = new int[] {0, 0};
    gbl_panel2Candidato.rowHeights = new int[] {0, 0};
    gbl_panel2Candidato.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gbl_panel2Candidato.rowWeights = new double[] {1.0, Double.MIN_VALUE};
    panel2Candidato.setLayout(gbl_panel2Candidato);

    JPanel panelNomeCandidato2 = new JPanel();
    GridBagConstraints gbc_panelNomeCandidato2 = new GridBagConstraints();
    gbc_panelNomeCandidato2.fill = GridBagConstraints.BOTH;
    gbc_panelNomeCandidato2.gridx = 0;
    gbc_panelNomeCandidato2.gridy = 0;
    panel2Candidato.add(panelNomeCandidato2, gbc_panelNomeCandidato2);
    panelNomeCandidato2.setLayout(new GridLayout(1, 0, 0, 0));

    JPanel panelWraperNomeCandidato2 = new JPanel();
    panelNomeCandidato2.add(panelWraperNomeCandidato2);
    panelWraperNomeCandidato2.setLayout(new BoxLayout(panelWraperNomeCandidato2, BoxLayout.X_AXIS));

    lblCandidato2 = new JLabel(candidato2);
    panelWraperNomeCandidato2.add(lblCandidato2);



  }



  public JLabel getLblCandidato1() {
    return lblCandidato1;
  }

  public JLabel getLblCandidato2() {
    return lblCandidato2;
  }

  private void setCandidato1(String candidato1) {
    this.candidato1 = new TextWrap(candidato1).toString();
  }

  private void setCandidato2(String candidato2) {
    this.candidato2 = new TextWrap(candidato2).toString();
  }

  public void adicionaCandidatos(String candidato1, String candidato2) {
    this.setCandidato1(candidato1);
    this.setCandidato2(candidato2);

  }
  
  public void limparCandidatos() {
    this.candidato1 = "";
    this.candidato2 = "";

  }



}
