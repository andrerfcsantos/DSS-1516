package dss.presentationlayer.components.panels.cne;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class CNEListaNrosTotaisPanel extends JPanel {

 
  /**
   * 
   */
  private static final long serialVersionUID = 1384301959210817675L;
  /**
   * Create the panel.
   */

  private JTextArea textAreaNroEleitores;
  private JLabel lblValorTotal;

  public CNEListaNrosTotaisPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0, 0, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JScrollPane scrollPaneNroTotais = new JScrollPane();
    GridBagConstraints gbc_scrollPaneNroTotais = new GridBagConstraints();
    gbc_scrollPaneNroTotais.gridwidth = 4;
    gbc_scrollPaneNroTotais.insets = new Insets(0, 0, 5, 0);
    gbc_scrollPaneNroTotais.fill = GridBagConstraints.BOTH;
    gbc_scrollPaneNroTotais.gridx = 0;
    gbc_scrollPaneNroTotais.gridy = 0;
    add(scrollPaneNroTotais, gbc_scrollPaneNroTotais);



    textAreaNroEleitores = new JTextArea();


    scrollPaneNroTotais.setViewportView(textAreaNroEleitores);

    JLabel lblNewLabel = new JLabel("Circulos");
    scrollPaneNroTotais.setColumnHeaderView(lblNewLabel);

    JLabel lblTotal = new JLabel("Total :");
    GridBagConstraints gbc_lblTotal = new GridBagConstraints();
    gbc_lblTotal.anchor = GridBagConstraints.EAST;
    gbc_lblTotal.insets = new Insets(0, 0, 5, 5);
    gbc_lblTotal.gridx = 0;
    gbc_lblTotal.gridy = 1;
    add(lblTotal, gbc_lblTotal);

    lblValorTotal = new JLabel("0");
    GridBagConstraints gbc_lblValorTotal = new GridBagConstraints();
    gbc_lblValorTotal.anchor = GridBagConstraints.WEST;
    gbc_lblValorTotal.insets = new Insets(0, 0, 5, 5);
    gbc_lblValorTotal.gridx = 1;
    gbc_lblValorTotal.gridy = 1;
    add(lblValorTotal, gbc_lblValorTotal);

  }


  public JTextArea getTextAreaNroEleitores() {
    return textAreaNroEleitores;
  }


  public JLabel getLblTotalEleitores() {
    return lblValorTotal;
  }

}
