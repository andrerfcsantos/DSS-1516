package dss.presentationlayer.components.panels.cne;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.MutableComboBoxModel;


public class CNENomeDeputadosEleitosPanel extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = -7110324882597913268L;
  private JList<String> listPartidosColigacoes;
  private JComboBox<String> comboBoxCirculos;
  private JTextArea textAreaNomesDeputados;

  /**
   * Create the panel.
   */
  public CNENomeDeputadosEleitosPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 243, 282, 289, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 45, 0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {0.0, 1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {0.0, 0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JScrollPane scrollPane = new JScrollPane();
    GridBagConstraints gbc_scrollPane = new GridBagConstraints();
    gbc_scrollPane.gridheight = 3;
    gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
    gbc_scrollPane.fill = GridBagConstraints.BOTH;
    gbc_scrollPane.gridx = 2;
    gbc_scrollPane.gridy = 1;
    add(scrollPane, gbc_scrollPane);
    listPartidosColigacoes = new JList<>();

    String[] items = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7"};

    // TODO Implementar Observer e modelo tem que ser variável da classe
    DefaultListModel<String> model = new DefaultListModel<>();


    for (String string : items) {

      model.addElement(string);

    }
    listPartidosColigacoes.setModel(model);



    scrollPane.setViewportView(listPartidosColigacoes);

    JLabel lblPartidosColigacoes = new JLabel("Partidos/coligações");
    scrollPane.setColumnHeaderView(lblPartidosColigacoes);

    JScrollPane scrollPane_1 = new JScrollPane();
    GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
    gbc_scrollPane_1.insets = new Insets(0, 0, 0, 5);
    gbc_scrollPane_1.gridheight = 3;
    gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
    gbc_scrollPane_1.gridx = 3;
    gbc_scrollPane_1.gridy = 1;
    add(scrollPane_1, gbc_scrollPane_1);

    textAreaNomesDeputados = new JTextArea();
    textAreaNomesDeputados.setEditable(false);
    textAreaNomesDeputados.setLineWrap(true);
    scrollPane_1.setViewportView(textAreaNomesDeputados);

    JLabel lblNomeDeputados = new JLabel("Nomes Deputados");
    scrollPane_1.setColumnHeaderView(lblNomeDeputados);

    String[] identificacaoStrings =
        {"Aveiro", "Beja", "Braga", "Bragança", "CasteloBranco", "Coimbra", "Évora", "Faro",
            "Guarda", "Leiria", "Lisboa", "Portalegre", "Porto", "Santarém", "Setúbal",
            "VianadoCastelo", "VilaReal", "Viseu", "Açores", "Madeira", "Europa", "Fora da Europa"};
    MutableComboBoxModel<String> comboModel = new DefaultComboBoxModel<>(identificacaoStrings);

    comboBoxCirculos = new JComboBox<>(comboModel);
    GridBagConstraints gbc_comboBoxCirculos = new GridBagConstraints();
    gbc_comboBoxCirculos.insets = new Insets(0, 0, 5, 5);
    gbc_comboBoxCirculos.fill = GridBagConstraints.HORIZONTAL;
    gbc_comboBoxCirculos.gridx = 1;
    gbc_comboBoxCirculos.gridy = 1;
    add(comboBoxCirculos, gbc_comboBoxCirculos);
  }

  public JList<String> getListPartidosColigacoes() {
    return listPartidosColigacoes;
  }

  public JComboBox<String> getComboBoxCirculos() {
    return comboBoxCirculos;
  }

  public JTextArea getTextAreaNomesDeputados() {
    return textAreaNomesDeputados;
  }
  
  

}
