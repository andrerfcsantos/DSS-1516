package dss.presentationlayer.components.panels.cne;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JScrollPane;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;

public class CNENroVotantesPanel extends JPanel {
  
  

  /**
   * 
   */
  private static final long serialVersionUID = 889752144587184671L;
  /**
   * 
   */
  
  private JList<String> listNroVotantes;
  private JLabel lblTotalVotantes;

  /**
   * Create the panel.
   */
  public CNENroVotantesPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0, 0, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JScrollPane scrollPane = new JScrollPane();
    GridBagConstraints gbc_scrollPane = new GridBagConstraints();
    gbc_scrollPane.gridwidth = 4;
    gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
    gbc_scrollPane.fill = GridBagConstraints.BOTH;
    gbc_scrollPane.gridx = 0;
    gbc_scrollPane.gridy = 0;
    add(scrollPane, gbc_scrollPane);



    String[] items = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7"};

    // TODO Implementar Observer e modelo tem que ser variável da classe
    DefaultListModel<String> model = new DefaultListModel<>();


    for (String string : items) {

      model.addElement(string);

    }
    listNroVotantes = new JList<>();
    listNroVotantes.setModel(model);


    scrollPane.setViewportView(listNroVotantes);

    JLabel lblNewLabel = new JLabel("Circulos");
    scrollPane.setColumnHeaderView(lblNewLabel);

    JLabel lblTotal = new JLabel("Total :");
    GridBagConstraints gbc_lblTotal = new GridBagConstraints();
    gbc_lblTotal.anchor = GridBagConstraints.EAST;
    gbc_lblTotal.insets = new Insets(0, 0, 5, 5);
    gbc_lblTotal.gridx = 0;
    gbc_lblTotal.gridy = 1;
    add(lblTotal, gbc_lblTotal);

    lblTotalVotantes = new JLabel("0");
    GridBagConstraints gbc_lblTotalVotantes = new GridBagConstraints();
    gbc_lblTotalVotantes.anchor = GridBagConstraints.WEST;
    gbc_lblTotalVotantes.insets = new Insets(0, 0, 5, 5);
    gbc_lblTotalVotantes.gridx = 1;
    gbc_lblTotalVotantes.gridy = 1;
    add(lblTotalVotantes, gbc_lblTotalVotantes);

  }

  public JList<String> getListNroVotantes() {
    return listNroVotantes;
  }

  public JLabel getLblTotalVotantes() {
    return lblTotalVotantes;
  }

 



}
