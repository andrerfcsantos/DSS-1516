package dss.presentationlayer.components.panels.cne;

import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

@SuppressWarnings("serial")
public class CNEResultadosPresidenciaisPanel extends JPanel {
  private JTable tableResultados;

  private JLabel lblEleitoresInscritos;
  private JLabel lblNroVotantes;
  private JLabel lblNroVotosBranco;

  private CardLayout cardlayoutResultados;

  private static String E_PR_EMPATE = "EMPATE";
  private static String E_PR_VITORIA = "VITORIA";
  private static String E_PR_DEFAULT = "DEFAULT";

  private CNEResPRMaioriaAbsolutaPanel maioriaAbsoluta;
  private CNEResPRSemMaioriaPanel semMaioria;
  private JPanel panelResultadosEspecifico;
  
  //TODO necessário saber que coleção será utilizada
  @SuppressWarnings("unused")
  private Object[][] dadosEleicao;

  /**
   * Create the panel.
   */
  public CNEResultadosPresidenciaisPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {120, 0, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, 1.0, 1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JPanel panel_1 = new JPanel();
    panel_1.setBorder(new TitledBorder(null, "<html> Nro eleitores <br> inscritos",
        TitledBorder.LEADING, TitledBorder.TOP, null, null));
    GridBagConstraints gbc_panel_1 = new GridBagConstraints();
    gbc_panel_1.fill = GridBagConstraints.BOTH;
    gbc_panel_1.insets = new Insets(0, 0, 5, 5);
    gbc_panel_1.gridx = 0;
    gbc_panel_1.gridy = 0;
    add(panel_1, gbc_panel_1);
    GridBagLayout gbl_panel_1 = new GridBagLayout();
    gbl_panel_1.columnWidths = new int[] {0, 0};
    gbl_panel_1.rowHeights = new int[] {0, 0};
    gbl_panel_1.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gbl_panel_1.rowWeights = new double[] {1.0, Double.MIN_VALUE};
    panel_1.setLayout(gbl_panel_1);

    lblEleitoresInscritos = new JLabel("0");
    GridBagConstraints gbc_lblEleitoresInscritos = new GridBagConstraints();
    gbc_lblEleitoresInscritos.gridx = 0;
    gbc_lblEleitoresInscritos.gridy = 0;
    panel_1.add(lblEleitoresInscritos, gbc_lblEleitoresInscritos);

    JPanel panel_2 = new JPanel();
    panel_2.setBorder(
        new TitledBorder(null, "Nro votantes", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    GridBagConstraints gbc_panel_2 = new GridBagConstraints();
    gbc_panel_2.fill = GridBagConstraints.BOTH;
    gbc_panel_2.insets = new Insets(0, 0, 5, 5);
    gbc_panel_2.gridx = 0;
    gbc_panel_2.gridy = 1;
    add(panel_2, gbc_panel_2);
    GridBagLayout gbl_panel_2 = new GridBagLayout();
    gbl_panel_2.columnWidths = new int[] {0, 0};
    gbl_panel_2.rowHeights = new int[] {0, 0};
    gbl_panel_2.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gbl_panel_2.rowWeights = new double[] {1.0, Double.MIN_VALUE};
    panel_2.setLayout(gbl_panel_2);

    lblNroVotantes = new JLabel("0");
    GridBagConstraints gbc_lblNroVotantes = new GridBagConstraints();
    gbc_lblNroVotantes.fill = GridBagConstraints.VERTICAL;
    gbc_lblNroVotantes.gridx = 0;
    gbc_lblNroVotantes.gridy = 0;
    panel_2.add(lblNroVotantes, gbc_lblNroVotantes);

    JScrollPane scrollPane = new JScrollPane();
    GridBagConstraints gbc_scrollPane = new GridBagConstraints();
    gbc_scrollPane.gridwidth = 2;
    gbc_scrollPane.gridheight = 3;
    gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
    gbc_scrollPane.fill = GridBagConstraints.BOTH;
    gbc_scrollPane.gridx = 1;
    gbc_scrollPane.gridy = 0;
    add(scrollPane, gbc_scrollPane);

    String[] colunas = {"Candidato", "Nro Votos", "% (Percentagem)"};

    Object[][] dados = {{"Kathy", new Integer(5), new Boolean(false)},
        {"John", new Integer(3), new Boolean(true)}, {"Sue", new Integer(2), new Boolean(false)},
        {"Jane", new Integer(20), new Boolean(true)}, {"Joe", new Integer(10), new Boolean(false)}};

    tableResultados = new JTable(dados, colunas);

    tableResultados.setFillsViewportHeight(true);

    scrollPane.setViewportView(tableResultados);

    JPanel panel_3 = new JPanel();
    panel_3.setBorder(new TitledBorder(null, "<html> Nro votos<br> em branco", TitledBorder.LEADING,
        TitledBorder.TOP, null, null));
    GridBagConstraints gbc_panel_3 = new GridBagConstraints();
    gbc_panel_3.fill = GridBagConstraints.BOTH;
    gbc_panel_3.insets = new Insets(0, 0, 5, 5);
    gbc_panel_3.gridx = 0;
    gbc_panel_3.gridy = 2;
    add(panel_3, gbc_panel_3);
    GridBagLayout gbl_panel_3 = new GridBagLayout();
    gbl_panel_3.columnWidths = new int[] {0, 0};
    gbl_panel_3.rowHeights = new int[] {0, 0};
    gbl_panel_3.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gbl_panel_3.rowWeights = new double[] {1.0, Double.MIN_VALUE};
    panel_3.setLayout(gbl_panel_3);

    JLabel lblNroVotosBranco = new JLabel("0");
    GridBagConstraints gbc_lblNroVotosBranco = new GridBagConstraints();
    gbc_lblNroVotosBranco.fill = GridBagConstraints.VERTICAL;
    gbc_lblNroVotosBranco.gridx = 0;
    gbc_lblNroVotosBranco.gridy = 0;
    panel_3.add(lblNroVotosBranco, gbc_lblNroVotosBranco);

    JPanel panelResultadosEspecifico = new JPanel();
    GridBagConstraints gbc_panelResultadosEspecifico = new GridBagConstraints();
    gbc_panelResultadosEspecifico.gridheight = 3;
    gbc_panelResultadosEspecifico.gridwidth = 3;
    gbc_panelResultadosEspecifico.fill = GridBagConstraints.BOTH;
    gbc_panelResultadosEspecifico.gridx = 0;
    gbc_panelResultadosEspecifico.gridy = 3;
    add(panelResultadosEspecifico, gbc_panelResultadosEspecifico);

    cardlayoutResultados = new CardLayout();

    maioriaAbsoluta = new CNEResPRMaioriaAbsolutaPanel();
    semMaioria = new CNEResPRSemMaioriaPanel();
    JPanel empty = new JPanel();
    panelResultadosEspecifico.add(empty, E_PR_DEFAULT);
    panelResultadosEspecifico.add(maioriaAbsoluta, E_PR_VITORIA);
    panelResultadosEspecifico.add(semMaioria, E_PR_EMPATE);
  }

  public JTable getTableResultados() {
    return tableResultados;
  }

  public JLabel getLblEleitoresInscritos() {
    return lblEleitoresInscritos;
  }

  public JLabel getLblNroVotantes() {
    return lblNroVotantes;
  }

  public JLabel getLblNroVotosBranco() {
    return lblNroVotosBranco;
  }

  public CardLayout getCardlayoutResultados() {
    return cardlayoutResultados;
  }

  public CNEResPRMaioriaAbsolutaPanel getMaioriaAbsoluta() {
    return maioriaAbsoluta;
  }

  public CNEResPRSemMaioriaPanel getSemMaioria() {
    return semMaioria;
  }



  public void setEleicaoPRPorDefeito() {
    cardlayoutResultados.show(panelResultadosEspecifico, E_PR_DEFAULT);
  }

  public void setEleicaoPRMaioriaAbsoluta() {
    cardlayoutResultados.show(panelResultadosEspecifico, E_PR_VITORIA);
  }

  public void setEleicaoSemMaioria() {
    cardlayoutResultados.show(panelResultadosEspecifico, E_PR_EMPATE);
  }

  public void setDadosEleicao(Object[][] dadosEleicao) {
    this.dadosEleicao = dadosEleicao;
  }
  
  



}
