package dss.presentationlayer.components.panels.cne;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import javax.swing.MutableComboBoxModel;

import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class CNEAbrirAtoEleitoralPanel extends JPanel {
  /**
   * 
   */
  private static final long serialVersionUID = -4949165211180801703L;
  private JTextField textField;
  private JButton btnFecharEleicoes;
  private JButton btnAbrirEleicoes;
  private JComboBox<String> comboBox;


  /**
   * Create the panel.
   */
  public CNEAbrirAtoEleitoralPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {0.0, 0.0, 1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights =
        new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JLabel lblAno = new JLabel("Ano");
    GridBagConstraints gbc_lblAno = new GridBagConstraints();
    gbc_lblAno.insets = new Insets(0, 0, 5, 5);
    gbc_lblAno.gridx = 2;
    gbc_lblAno.gridy = 2;
    add(lblAno, gbc_lblAno);

    textField = new JTextField();
    GridBagConstraints gbc_textField = new GridBagConstraints();
    gbc_textField.insets = new Insets(0, 0, 5, 5);
    gbc_textField.fill = GridBagConstraints.HORIZONTAL;
    gbc_textField.gridx = 2;
    gbc_textField.gridy = 3;
    add(textField, gbc_textField);
    textField.setColumns(10);

    JLabel lblTipoAtoEleitoral = new JLabel("Tipo Ato Eleitoral");
    GridBagConstraints gbc_lblTipoAtoEleitoral = new GridBagConstraints();
    gbc_lblTipoAtoEleitoral.insets = new Insets(0, 0, 5, 5);
    gbc_lblTipoAtoEleitoral.gridx = 2;
    gbc_lblTipoAtoEleitoral.gridy = 4;
    add(lblTipoAtoEleitoral, gbc_lblTipoAtoEleitoral);

    String[] identificacaoStrings = {"Legistlativas", "Presidenciais"};
    MutableComboBoxModel<String> comboModel = new DefaultComboBoxModel<>(identificacaoStrings);

    comboBox = new JComboBox<>(comboModel);
    GridBagConstraints gbc_comboBox = new GridBagConstraints();
    gbc_comboBox.insets = new Insets(0, 0, 5, 5);
    gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
    gbc_comboBox.gridx = 2;
    gbc_comboBox.gridy = 5;
    add(comboBox, gbc_comboBox);

    btnAbrirEleicoes = new JButton("Abrir Ato Eleitoral");
    GridBagConstraints gbc_btnAbrirEleicoes = new GridBagConstraints();
    gbc_btnAbrirEleicoes.gridwidth = 3;
    gbc_btnAbrirEleicoes.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnAbrirEleicoes.insets = new Insets(0, 0, 5, 5);
    gbc_btnAbrirEleicoes.gridx = 2;
    gbc_btnAbrirEleicoes.gridy = 7;
    add(btnAbrirEleicoes, gbc_btnAbrirEleicoes);

    btnFecharEleicoes = new JButton("Fechar Ato Eleitoral");
    GridBagConstraints gbc_btnFecharEleicoes = new GridBagConstraints();
    gbc_btnFecharEleicoes.gridwidth = 3;
    gbc_btnFecharEleicoes.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnFecharEleicoes.insets = new Insets(0, 0, 5, 5);
    gbc_btnFecharEleicoes.gridx = 2;
    gbc_btnFecharEleicoes.gridy = 8;
    add(btnFecharEleicoes, gbc_btnFecharEleicoes);

  }

  public JTextField getTextField() {
    return textField;
  }

  public JButton getBtnFecharEleicoes() {
    return btnFecharEleicoes;
  }

  public JButton getBtnAbrirEleicoes() {
    return btnAbrirEleicoes;
  }


  public JComboBox<String> getComboBox() {
    return comboBox;
  }



}
