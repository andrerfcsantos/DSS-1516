package dss.presentationlayer.components.panels.cne;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JTabbedPane;
import java.awt.GridBagConstraints;

public class CNEResultadosLegislativasPanel extends JPanel {

  

  
  /**
   * 
   */
  private static final long serialVersionUID = -3850579871143736659L;
  /**
   * Create the panel.
   */
  private CNEListaNrosTotaisPanel nroVotantesPanel;
  private CNEListaNrosTotaisPanel nroEleitores;
  private CNEListaNrosTotaisPanel nroVotosNulosPanel;
  private CNENroMandatosPanel nroMandatosPanel;
  private CNENomeDeputadosEleitosPanel nomeDeputadosEleitosPanel;
  
  
  public CNEResultadosLegislativasPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[]{0, 0};
    gridBagLayout.rowHeights = new int[]{0, 0};
    gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);
    
    JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
    GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
    gbc_tabbedPane.fill = GridBagConstraints.BOTH;
    gbc_tabbedPane.gridx = 0;
    gbc_tabbedPane.gridy = 0;
    add(tabbedPane, gbc_tabbedPane);
    
    nroVotantesPanel = new CNEListaNrosTotaisPanel();
    tabbedPane.addTab("Nro Votantes", null, nroVotantesPanel, null);
    
    nroEleitores = new CNEListaNrosTotaisPanel();
    tabbedPane.addTab("Nro Eleitores", null, nroEleitores, null);
    
    nroVotosNulosPanel = new CNEListaNrosTotaisPanel();
    tabbedPane.addTab("<html> Nro Votos em <br> Branco / Nulos </html>", null, nroVotosNulosPanel, null);
    
    nroMandatosPanel = new CNENroMandatosPanel();
    tabbedPane.addTab("<html> Nro Mandatos <br> atribuidos </html>", null, nroMandatosPanel, null);
    
    nomeDeputadosEleitosPanel = new CNENomeDeputadosEleitosPanel();
    tabbedPane.addTab("<html> Nome Deputados <br> Eleitos </html>", null, nomeDeputadosEleitosPanel, null);
    

  }



  


  public CNEListaNrosTotaisPanel getNroVotantesPanel() {
    return nroVotantesPanel;
  }






  public CNEListaNrosTotaisPanel getNroEleitores() {
    return nroEleitores;
  }






  public CNEListaNrosTotaisPanel getNroVotosNulosPanel() {
    return nroVotosNulosPanel;
  }






  public CNENroMandatosPanel getNroMandatosPanel() {
    return nroMandatosPanel;
  }


  public CNENomeDeputadosEleitosPanel getNomeDeputadosEleitosPanel() {
    return nomeDeputadosEleitosPanel;
  }
  
  
  
  

}
