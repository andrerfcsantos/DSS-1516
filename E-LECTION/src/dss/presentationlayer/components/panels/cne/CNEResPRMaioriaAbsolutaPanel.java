package dss.presentationlayer.components.panels.cne;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import dss.presentationlayer.components.TextWrap;

public class CNEResPRMaioriaAbsolutaPanel extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = 5968479071352551652L;

  private JLabel lblVencedorMaioria;
  
  private String vencedor = "";

  /**
   * Create the panel.
   */
  public CNEResPRMaioriaAbsolutaPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {1.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JPanel panel1 = new JPanel();
    panel1.setBorder(new TitledBorder(null, "Vencedor por maioria absoluta", TitledBorder.LEADING,
        TitledBorder.TOP, null, Color.green));
    GridBagConstraints gbc_panel1 = new GridBagConstraints();
    gbc_panel1.fill = GridBagConstraints.BOTH;
    gbc_panel1.gridx = 0;
    gbc_panel1.gridy = 0;
    add(panel1, gbc_panel1);
    GridBagLayout gbl_panel1 = new GridBagLayout();
    gbl_panel1.columnWidths = new int[] {0, 0, 0, 0};
    gbl_panel1.rowHeights = new int[] {0, 0};
    gbl_panel1.columnWeights = new double[] {1.0, 1.0, 1.0, Double.MIN_VALUE};
    gbl_panel1.rowWeights = new double[] {1.0, Double.MIN_VALUE};
    panel1.setLayout(gbl_panel1);

    JPanel panel2 = new JPanel();
    GridBagConstraints gbc_panel2 = new GridBagConstraints();
    gbc_panel2.fill = GridBagConstraints.BOTH;
    gbc_panel2.gridx = 1;
    gbc_panel2.gridy = 0;
    panel1.add(panel2, gbc_panel2);
    panel2.setLayout(new GridLayout(1, 0, 0, 0));

    JPanel panel3 = new JPanel();
    panel2.add(panel3);
    panel3.setLayout(new BoxLayout(panel3, BoxLayout.X_AXIS));

    lblVencedorMaioria =
        new JLabel(vencedor);
    panel3.add(lblVencedorMaioria);



  }

  public JLabel getLblVencedorMaioria() {
    return lblVencedorMaioria;
  }


  public void setVencedor(String vencedor) {
    this.vencedor = new TextWrap(vencedor).toString();
  }
  
  public void limparVencedor(String vencedor) {
    this.vencedor = "";
  }
  
  
  
  

}
