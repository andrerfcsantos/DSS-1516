package dss.presentationlayer.components.panels.mandatario;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

import dss.presentationlayer.components.Consulta;

public class MandatarioConsultarCandidatoPanel extends JPanel {

  

  
  /**
   * 
   */
  private static final long serialVersionUID = 2264109082192651150L;
  /**
   * Create the panel.
   */
  private Consulta consultaCandidato;
  
  public MandatarioConsultarCandidatoPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[]{0, 0};
    gridBagLayout.rowHeights = new int[]{0, 0};
    gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);
    
    consultaCandidato = new Consulta();
    GridBagConstraints gbc_panel = new GridBagConstraints();
    gbc_panel.fill = GridBagConstraints.BOTH;
    gbc_panel.gridx = 0;
    gbc_panel.gridy = 0;
    gbc_panel.weightx = gbc_panel.weighty = 1.0;
    add(consultaCandidato, gbc_panel);
    
    

  }

  public Consulta getConsultaCandidato() {
    return consultaCandidato;
  }

 
  

}
