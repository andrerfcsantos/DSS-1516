package dss.presentationlayer.components.panels.mandatario;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dss.presentationlayer.components.Formulario;

public class MandatarioGerirCandidatoUninominalPanel extends JPanel {

 
  /**
   * 
   */
 
  /**
   * 
   */
  private static final long serialVersionUID = 4762981344099818381L;
  /**
   * Create the panel.
   */
  private JButton btnAdicionar;
  private JButton btnRemover;
  private JButton btnEditar;
  private JList<String> listCandidatoUninominal;
  private Formulario formularioCandidatoUninominal;

  public MandatarioGerirCandidatoUninominalPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {602, 144, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, 1.0, 0.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {0.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    formularioCandidatoUninominal = new Formulario();
    GridBagConstraints gbc_formularioCandidatoUninominal = new GridBagConstraints();
    gbc_formularioCandidatoUninominal.gridheight = 3;
    gbc_formularioCandidatoUninominal.insets = new Insets(0, 0, 5, 5);
    gbc_formularioCandidatoUninominal.fill = GridBagConstraints.BOTH;
    gbc_formularioCandidatoUninominal.gridx = 0;
    gbc_formularioCandidatoUninominal.gridy = 1;
    gbc_formularioCandidatoUninominal.weightx = gbc_formularioCandidatoUninominal.weighty = 1.0;
    add(formularioCandidatoUninominal, gbc_formularioCandidatoUninominal);

    JScrollPane scrollPaneCandidatoUninominal = new JScrollPane();
    GridBagConstraints gbc_scrollPaneCandidatoUninominal = new GridBagConstraints();
    gbc_scrollPaneCandidatoUninominal.insets = new Insets(0, 0, 5, 5);
    gbc_scrollPaneCandidatoUninominal.fill = GridBagConstraints.BOTH;
    gbc_scrollPaneCandidatoUninominal.gridx = 1;
    gbc_scrollPaneCandidatoUninominal.gridy = 1;
    gbc_scrollPaneCandidatoUninominal.weightx = gbc_scrollPaneCandidatoUninominal.weighty = 1.0;
    add(scrollPaneCandidatoUninominal, gbc_scrollPaneCandidatoUninominal);
    JList<String> listCandidatoUninominal = new JList<>();

    String[] items = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7"};

    // TODO Implementar Observer e modelo tem que ser variável da classe
    DefaultListModel<String> model = new DefaultListModel<>();


    for (String string : items) {

      model.addElement(string);

    }
    listCandidatoUninominal.setModel(model);


    scrollPaneCandidatoUninominal.setViewportView(listCandidatoUninominal);

    JLabel lblCandidatos = new JLabel("Candidatos");
    scrollPaneCandidatoUninominal.setColumnHeaderView(lblCandidatos);

    btnAdicionar = new JButton("Adicionar");
    GridBagConstraints gbc_btnAdicionar = new GridBagConstraints();
    gbc_btnAdicionar.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnAdicionar.insets = new Insets(0, 0, 5, 5);
    gbc_btnAdicionar.gridx = 1;
    gbc_btnAdicionar.gridy = 2;
    gbc_btnAdicionar.weightx = 1.0;
    add(btnAdicionar, gbc_btnAdicionar);

    btnRemover = new JButton("Remover");
    GridBagConstraints gbc_btnRemover = new GridBagConstraints();
    gbc_btnRemover.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnRemover.insets = new Insets(0, 0, 5, 5);
    gbc_btnRemover.gridx = 1;
    gbc_btnRemover.gridy = 3;
    gbc_btnRemover.weightx = 1.0;
    add(btnRemover, gbc_btnRemover);
    
    btnEditar = new JButton("Editar");
    
    GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
    gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
    gbc_btnNewButton.gridx = 1;
    gbc_btnNewButton.gridy = 4;
    gbc_btnNewButton.weightx = 1.0;
    add(btnEditar, gbc_btnNewButton);

  }

  public JButton getBtnAdicionar() {
    return btnAdicionar;
  }

  public JButton getBtnRemover() {
    return btnRemover;
  }
  
  
  
  

  public JButton getBtnEditar() {
    return btnEditar;
  }

  public JList<String> getListCandidatoUninominal() {
    return listCandidatoUninominal;
  }


  public Formulario getFormularioCandidatoUninominal() {
    return formularioCandidatoUninominal;
  }
  
  


}
