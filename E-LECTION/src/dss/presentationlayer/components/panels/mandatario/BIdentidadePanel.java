package dss.presentationlayer.components.panels.mandatario;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BIdentidadePanel extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = 6799536046842461463L;
  private JTextField textFieldNroBI;
  private JTextField textFieldArquivo;

  private JTextField textFieldDiaEmissao;
  private JTextField textFieldMesEmissao;
  private JTextField textFieldAnoEmissao;

  private JTextField textFieldDiaValidade;
  private JTextField textFieldMesValidade;
  private JTextField textFieldAnoValidade;


  /**
   * Create the panel.
   */
  public BIdentidadePanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {40, 75, 75, 75, 75, 29, 0};
    gridBagLayout.rowHeights = new int[] {23, 23, 23, 23, 23, 23, 0};
    gridBagLayout.columnWeights = new double[] {0.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights =
        new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JLabel lblNroBI = new JLabel("Nro BI");
    GridBagConstraints gbc_lblNroBI = new GridBagConstraints();
    gbc_lblNroBI.fill = GridBagConstraints.BOTH;
    gbc_lblNroBI.insets = new Insets(0, 0, 5, 5);
    gbc_lblNroBI.gridx = 1;
    gbc_lblNroBI.gridy = 0;
    add(lblNroBI, gbc_lblNroBI);

    JLabel lblArquivo = new JLabel("Arquivo");
    GridBagConstraints gbc_lblArquivo = new GridBagConstraints();
    gbc_lblArquivo.fill = GridBagConstraints.BOTH;
    gbc_lblArquivo.insets = new Insets(0, 0, 5, 5);
    gbc_lblArquivo.gridx = 3;
    gbc_lblArquivo.gridy = 0;
    add(lblArquivo, gbc_lblArquivo);

    textFieldNroBI = new JTextField();
    GridBagConstraints gbc_textFieldNroBI = new GridBagConstraints();
    gbc_textFieldNroBI.gridwidth = 2;
    gbc_textFieldNroBI.fill = GridBagConstraints.BOTH;
    gbc_textFieldNroBI.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldNroBI.gridx = 1;
    gbc_textFieldNroBI.gridy = 1;
    gbc_textFieldNroBI.weightx = 1.0;
    add(textFieldNroBI, gbc_textFieldNroBI);
    textFieldNroBI.setColumns(10);

    textFieldArquivo = new JTextField();
    GridBagConstraints gbc_textFieldArquivo = new GridBagConstraints();
    gbc_textFieldArquivo.gridwidth = 2;
    gbc_textFieldArquivo.fill = GridBagConstraints.BOTH;
    gbc_textFieldArquivo.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldArquivo.gridx = 3;
    gbc_textFieldArquivo.gridy = 1;
    gbc_textFieldArquivo.weightx= 1.0;
    add(textFieldArquivo, gbc_textFieldArquivo);
    textFieldArquivo.setColumns(10);

    JLabel lblDiaEmissao = new JLabel("Dia");
    GridBagConstraints gbc_lblDiaEmissao = new GridBagConstraints();
    gbc_lblDiaEmissao.fill = GridBagConstraints.BOTH;
    gbc_lblDiaEmissao.insets = new Insets(0, 0, 5, 5);
    gbc_lblDiaEmissao.gridx = 2;
    gbc_lblDiaEmissao.gridy = 2;
    add(lblDiaEmissao, gbc_lblDiaEmissao);

    JLabel lblMesEmissao = new JLabel("Mês");
    GridBagConstraints gbc_lblMesEmissao = new GridBagConstraints();
    gbc_lblMesEmissao.fill = GridBagConstraints.BOTH;
    gbc_lblMesEmissao.insets = new Insets(0, 0, 5, 5);
    gbc_lblMesEmissao.gridx = 3;
    gbc_lblMesEmissao.gridy = 2;
    add(lblMesEmissao, gbc_lblMesEmissao);

    JLabel lblAnoEmissao = new JLabel("Ano");
    GridBagConstraints gbc_lblAnoEmissao = new GridBagConstraints();
    gbc_lblAnoEmissao.fill = GridBagConstraints.BOTH;
    gbc_lblAnoEmissao.insets = new Insets(0, 0, 5, 5);
    gbc_lblAnoEmissao.gridx = 4;
    gbc_lblAnoEmissao.gridy = 2;
    add(lblAnoEmissao, gbc_lblAnoEmissao);

    JLabel lblDataEmissao = new JLabel("Data Emissão");
    GridBagConstraints gbc_lblDataEmissao = new GridBagConstraints();
    gbc_lblDataEmissao.anchor = GridBagConstraints.EAST;
    gbc_lblDataEmissao.fill = GridBagConstraints.VERTICAL;
    gbc_lblDataEmissao.insets = new Insets(0, 0, 5, 5);
    gbc_lblDataEmissao.gridx = 1;
    gbc_lblDataEmissao.gridy = 3;
    add(lblDataEmissao, gbc_lblDataEmissao);

    textFieldDiaEmissao = new JTextField();
    GridBagConstraints gbc_textFieldDiaEmissao = new GridBagConstraints();
    gbc_textFieldDiaEmissao.fill = GridBagConstraints.BOTH;
    gbc_textFieldDiaEmissao.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldDiaEmissao.gridx = 2;
    gbc_textFieldDiaEmissao.gridy = 3;
    gbc_textFieldDiaEmissao.weightx = 1.0;
    add(textFieldDiaEmissao, gbc_textFieldDiaEmissao);
    textFieldDiaEmissao.setColumns(10);

    textFieldMesEmissao = new JTextField();
    GridBagConstraints gbc_textFieldMesEmissao = new GridBagConstraints();
    gbc_textFieldMesEmissao.fill = GridBagConstraints.BOTH;
    gbc_textFieldMesEmissao.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldMesEmissao.gridx = 3;
    gbc_textFieldMesEmissao.gridy = 3;
    gbc_textFieldMesEmissao.weightx =  1.0;
    add(textFieldMesEmissao, gbc_textFieldMesEmissao);
    textFieldMesEmissao.setColumns(10);



    textFieldAnoEmissao = new JTextField();
    GridBagConstraints gbc_textFieldAnoEmissao = new GridBagConstraints();
    gbc_textFieldAnoEmissao.fill = GridBagConstraints.BOTH;
    gbc_textFieldAnoEmissao.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldAnoEmissao.gridx = 4;
    gbc_textFieldAnoEmissao.gridy = 3;
    gbc_textFieldAnoEmissao.weightx = 1.0;
    add(textFieldAnoEmissao, gbc_textFieldAnoEmissao);
    textFieldAnoEmissao.setColumns(10);

    JLabel labelDiaValidade = new JLabel("Dia");
    GridBagConstraints gbc_labelDiaValidade = new GridBagConstraints();
    gbc_labelDiaValidade.fill = GridBagConstraints.BOTH;
    gbc_labelDiaValidade.insets = new Insets(0, 0, 5, 5);
    gbc_labelDiaValidade.gridx = 2;
    gbc_labelDiaValidade.gridy = 4;
    add(labelDiaValidade, gbc_labelDiaValidade);

    JLabel labelMesValidade = new JLabel("Mês");
    GridBagConstraints gbc_labelMesValidade = new GridBagConstraints();
    gbc_labelMesValidade.fill = GridBagConstraints.BOTH;
    gbc_labelMesValidade.insets = new Insets(0, 0, 5, 5);
    gbc_labelMesValidade.gridx = 3;
    gbc_labelMesValidade.gridy = 4;
    add(labelMesValidade, gbc_labelMesValidade);

    JLabel labelAnoValidade = new JLabel("Ano");
    GridBagConstraints gbc_labelAnoValidade = new GridBagConstraints();
    gbc_labelAnoValidade.fill = GridBagConstraints.BOTH;
    gbc_labelAnoValidade.insets = new Insets(0, 0, 5, 5);
    gbc_labelAnoValidade.gridx = 4;
    gbc_labelAnoValidade.gridy = 4;
    add(labelAnoValidade, gbc_labelAnoValidade);

    JLabel lblDataValidade = new JLabel("Data Validade");
    GridBagConstraints gbc_lblDataValidade = new GridBagConstraints();
    gbc_lblDataValidade.anchor = GridBagConstraints.EAST;
    gbc_lblDataValidade.fill = GridBagConstraints.VERTICAL;
    gbc_lblDataValidade.insets = new Insets(0, 0, 0, 5);
    gbc_lblDataValidade.gridx = 1;
    gbc_lblDataValidade.gridy = 5;
    add(lblDataValidade, gbc_lblDataValidade);

    textFieldDiaValidade = new JTextField();
    GridBagConstraints gbc_textFieldDiaValidade = new GridBagConstraints();
    gbc_textFieldDiaValidade.fill = GridBagConstraints.BOTH;
    gbc_textFieldDiaValidade.insets = new Insets(0, 0, 0, 5);
    gbc_textFieldDiaValidade.gridx = 2;
    gbc_textFieldDiaValidade.gridy = 5;
    gbc_textFieldDiaValidade.weightx = 1.0;
    add(textFieldDiaValidade, gbc_textFieldDiaValidade);
    textFieldDiaValidade.setColumns(10);

    textFieldMesValidade = new JTextField();
    GridBagConstraints gbc_textFieldMesValidade = new GridBagConstraints();
    gbc_textFieldMesValidade.fill = GridBagConstraints.BOTH;
    gbc_textFieldMesValidade.insets = new Insets(0, 0, 0, 5);
    gbc_textFieldMesValidade.gridx = 3;
    gbc_textFieldMesValidade.gridy = 5;
    gbc_textFieldMesValidade.weightx= 1.0;
    add(textFieldMesValidade, gbc_textFieldMesValidade);
    textFieldMesValidade.setColumns(10);

    textFieldAnoValidade = new JTextField();
    GridBagConstraints gbc_textFieldAnoValidade = new GridBagConstraints();
    gbc_textFieldAnoValidade.fill = GridBagConstraints.BOTH;
    gbc_textFieldAnoValidade.insets = new Insets(0, 0, 0, 5);
    gbc_textFieldAnoValidade.gridx = 4;
    gbc_textFieldAnoValidade.gridy = 5;
    gbc_textFieldAnoValidade.weightx = 1.0;
    add(textFieldAnoValidade, gbc_textFieldAnoValidade);
    textFieldAnoValidade.setColumns(10);



  }

  public void limparTextFields() {


    textFieldNroBI.setText("");
    textFieldArquivo.setText("");

    textFieldDiaEmissao.setText("");
    textFieldMesEmissao.setText("");
    textFieldAnoEmissao.setText("");

    textFieldDiaValidade.setText("");
    textFieldMesValidade.setText("");
    textFieldAnoValidade.setText("");

  }
}
