package dss.presentationlayer.components.panels.mandatario;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dss.presentationlayer.components.Formulario;

public class MandatarioPlurinominalPanel extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = 6119831694250146367L;
  /**
   * Create the panel.
   */
  private JButton btnAdicionar;
  private JButton btnRemover;
  private JList<String> listCandidatoPlurinominal;
  private JButton btnMBaixo;
  private JButton btnMCima;
  private Formulario formularioCandidatoPlurinominal;
  

  public MandatarioPlurinominalPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {595, 99, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 146, 146, 0, 0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, 1.0, 1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights =
        new double[] {0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    formularioCandidatoPlurinominal = new Formulario();
    GridBagConstraints gbc_formularioCandidatoPlurinominal = new GridBagConstraints();
    gbc_formularioCandidatoPlurinominal.gridheight = 7;
    gbc_formularioCandidatoPlurinominal.insets = new Insets(0, 0, 5, 5);
    gbc_formularioCandidatoPlurinominal.fill = GridBagConstraints.BOTH;
    gbc_formularioCandidatoPlurinominal.gridx = 0;
    gbc_formularioCandidatoPlurinominal.gridy = 1;
    gbc_formularioCandidatoPlurinominal.weightx = gbc_formularioCandidatoPlurinominal.weighty = 1.0;
    add(formularioCandidatoPlurinominal, gbc_formularioCandidatoPlurinominal);

    JScrollPane scrollPaneCandidatoPlurinominal = new JScrollPane();
    GridBagConstraints gbc_scrollPaneCandidatoPlurinominal = new GridBagConstraints();
    gbc_scrollPaneCandidatoPlurinominal.gridheight = 3;
    gbc_scrollPaneCandidatoPlurinominal.gridwidth = 2;
    gbc_scrollPaneCandidatoPlurinominal.insets = new Insets(0, 0, 5, 0);
    gbc_scrollPaneCandidatoPlurinominal.fill = GridBagConstraints.BOTH;
    gbc_scrollPaneCandidatoPlurinominal.gridx = 1;
    gbc_scrollPaneCandidatoPlurinominal.gridy = 1;
    gbc_scrollPaneCandidatoPlurinominal.weightx = gbc_scrollPaneCandidatoPlurinominal.weighty = 1.0;
    add(scrollPaneCandidatoPlurinominal, gbc_scrollPaneCandidatoPlurinominal);
    listCandidatoPlurinominal = new JList<>();

    String[] items = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7"};

    // TODO Implementar Observer e modelo tem que ser variável da classe
    DefaultListModel<String> model = new DefaultListModel<>();


    for (String string : items) {

      model.addElement(string);

    }
    listCandidatoPlurinominal.setModel(model);


    scrollPaneCandidatoPlurinominal.setViewportView(listCandidatoPlurinominal);

    JLabel lblCandidatos = new JLabel("Candidatos");
    scrollPaneCandidatoPlurinominal.setColumnHeaderView(lblCandidatos);

    btnMCima = new JButton("<html>Mover<br>Para Cima</html>");
    GridBagConstraints gbc_btnMCima = new GridBagConstraints();
    gbc_btnMCima.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnMCima.insets = new Insets(0, 0, 5, 5);
    gbc_btnMCima.gridx = 1;
    gbc_btnMCima.gridy = 4;
    gbc_btnMCima.weightx  = 1.0;
    add(btnMCima, gbc_btnMCima);

    btnMBaixo = new JButton("<html>Mover<br>Para Baixo</html>");
    GridBagConstraints gbc_btnMBaixo = new GridBagConstraints();
    gbc_btnMBaixo.insets = new Insets(0, 0, 5, 0);
    gbc_btnMBaixo.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnMBaixo.gridx = 2;
    gbc_btnMBaixo.gridy = 4;
    gbc_btnMBaixo.weightx =1.0;
    add(btnMBaixo, gbc_btnMBaixo);

    btnAdicionar = new JButton("Adicionar");
    GridBagConstraints gbc_btnAdicionar = new GridBagConstraints();
    gbc_btnAdicionar.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnAdicionar.insets = new Insets(0, 0, 5, 5);
    gbc_btnAdicionar.gridx = 1;
    gbc_btnAdicionar.gridy = 5;
    gbc_btnAdicionar.weightx =  1.0;
    add(btnAdicionar, gbc_btnAdicionar);

    btnRemover = new JButton("Remover");
    GridBagConstraints gbc_btnRemover = new GridBagConstraints();
    gbc_btnRemover.insets = new Insets(0, 0, 5, 0);
    gbc_btnRemover.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnRemover.gridx = 2;
    gbc_btnRemover.gridy = 5;
    gbc_btnRemover.weightx =  1.0;
    add(btnRemover, gbc_btnRemover);

  }

  public JButton getBtnAdicionar() {
    return btnAdicionar;
  }

  public JButton getBtnRemover() {
    return btnRemover;
  }

  public JList<String> getListCandidatoPlurinominal() {
    return listCandidatoPlurinominal;
  }

  public JButton getBtnMBaixo() {
    return btnMBaixo;
  }

  public JButton getBtnMCima() {
    return btnMCima;
  }

  public Formulario getFormularioCandidatoPlurinominal() {
    return formularioCandidatoPlurinominal;
  }

  public JList<String> getListCandidatoUninominal() {
    return listCandidatoPlurinominal;
  }



}
