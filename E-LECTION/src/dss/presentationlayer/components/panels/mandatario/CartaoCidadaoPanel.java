package dss.presentationlayer.components.panels.mandatario;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CartaoCidadaoPanel extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = -2893352497083768456L;

  private JTextField textFieldNroCC;

  private JTextField textFieldDiaValidade;
  private JTextField textFieldMesValidade;
  private JTextField textFieldAnoValidade;


  /**
   * Create the panel.
   */
  public CartaoCidadaoPanel() {

    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {75, 75, 75, 75, 75, 75, 0};
    gridBagLayout.rowHeights = new int[] {23, 23, 23, 23, 23, 23, 23, 0};
    gridBagLayout.columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JLabel lblNroBI = new JLabel("Nro CC");
    GridBagConstraints gbc_lblNroBI = new GridBagConstraints();
    gbc_lblNroBI.fill = GridBagConstraints.BOTH;
    gbc_lblNroBI.insets = new Insets(0, 0, 5, 5);
    gbc_lblNroBI.gridx = 1;
    gbc_lblNroBI.gridy = 1;
   
    add(lblNroBI, gbc_lblNroBI);

    textFieldNroCC = new JTextField();
    GridBagConstraints gbc_textFieldNroCC = new GridBagConstraints();
    gbc_textFieldNroCC.gridwidth = 2;
    gbc_textFieldNroCC.fill = GridBagConstraints.BOTH;
    gbc_textFieldNroCC.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldNroCC.gridx = 1;
    gbc_textFieldNroCC.gridy = 2;
    gbc_textFieldNroCC.weightx =  1.0;
    add(textFieldNroCC, gbc_textFieldNroCC);
    textFieldNroCC.setColumns(10);

    JLabel labelDiaValidade = new JLabel("Dia");
    GridBagConstraints gbc_labelDiaValidade = new GridBagConstraints();
    gbc_labelDiaValidade.fill = GridBagConstraints.BOTH;
    gbc_labelDiaValidade.insets = new Insets(0, 0, 5, 5);
    gbc_labelDiaValidade.gridx = 2;
    gbc_labelDiaValidade.gridy = 3;
   
    add(labelDiaValidade, gbc_labelDiaValidade);

    JLabel labelMesValidade = new JLabel("Mês");
    GridBagConstraints gbc_labelMesValidade = new GridBagConstraints();
    gbc_labelMesValidade.fill = GridBagConstraints.BOTH;
    gbc_labelMesValidade.insets = new Insets(0, 0, 5, 5);
    gbc_labelMesValidade.gridx = 3;
    gbc_labelMesValidade.gridy = 3;
   
    add(labelMesValidade, gbc_labelMesValidade);

    JLabel labelAnoValidade = new JLabel("Ano");
    GridBagConstraints gbc_labelAnoValidade = new GridBagConstraints();
    gbc_labelAnoValidade.fill = GridBagConstraints.BOTH;
    gbc_labelAnoValidade.insets = new Insets(0, 0, 5, 5);
    gbc_labelAnoValidade.gridx = 4;
    gbc_labelAnoValidade.gridy = 3;
   
    add(labelAnoValidade, gbc_labelAnoValidade);

    JLabel lblDataValidade = new JLabel("Data Validade");
    GridBagConstraints gbc_lblDataValidade = new GridBagConstraints();
    gbc_lblDataValidade.fill = GridBagConstraints.BOTH;
    gbc_lblDataValidade.insets = new Insets(0, 0, 5, 5);
    gbc_lblDataValidade.gridx = 1;
    gbc_lblDataValidade.gridy = 4;
    
    add(lblDataValidade, gbc_lblDataValidade);

    textFieldDiaValidade = new JTextField();
    GridBagConstraints gbc_textFieldDiaValidade = new GridBagConstraints();
    gbc_textFieldDiaValidade.fill = GridBagConstraints.BOTH;
    gbc_textFieldDiaValidade.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldDiaValidade.gridx = 2;
    gbc_textFieldDiaValidade.gridy = 4;
    gbc_textFieldDiaValidade.weightx = 1.0;
    add(textFieldDiaValidade, gbc_textFieldDiaValidade);
    textFieldDiaValidade.setColumns(10);

    textFieldMesValidade = new JTextField();
    GridBagConstraints gbc_textFieldMesValidade = new GridBagConstraints();
    gbc_textFieldMesValidade.fill = GridBagConstraints.BOTH;
    gbc_textFieldMesValidade.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldMesValidade.gridx = 3;
    gbc_textFieldMesValidade.gridy = 4;
    gbc_textFieldMesValidade.weightx =  1.0;
    add(textFieldMesValidade, gbc_textFieldMesValidade);
    textFieldMesValidade.setColumns(10);

    textFieldAnoValidade = new JTextField();
    GridBagConstraints gbc_textFieldAnoValidade = new GridBagConstraints();
    gbc_textFieldAnoValidade.fill = GridBagConstraints.BOTH;
    gbc_textFieldAnoValidade.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldAnoValidade.gridx = 4;
    gbc_textFieldAnoValidade.gridy = 4;
    gbc_textFieldAnoValidade.weightx= 1.0;
    add(textFieldAnoValidade, gbc_textFieldAnoValidade);
    textFieldAnoValidade.setColumns(10);


  }


  public JTextField getTextFieldNroCC() {
    return textFieldNroCC;
  }


  public JTextField getTextFieldDiaValidade() {
    return textFieldDiaValidade;
  }


  public JTextField getTextFieldMesValidade() {
    return textFieldMesValidade;
  }


  public JTextField getTextFieldAnoValidade() {
    return textFieldAnoValidade;
  }

  public void limparTextFields() {

    textFieldNroCC.setText("");

    textFieldDiaValidade.setText("");
    textFieldMesValidade.setText("");
    textFieldAnoValidade.setText("");

  }



}
