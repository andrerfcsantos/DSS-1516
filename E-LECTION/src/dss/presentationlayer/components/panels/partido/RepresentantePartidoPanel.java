package dss.presentationlayer.components.panels.partido;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.MutableComboBoxModel;

import dss.presentationlayer.components.Consulta;

public class RepresentantePartidoPanel extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = 4546544680655385668L;
  private JComboBox<String> comboBoxCirculoEleitoral;
  private Consulta consultaCirculoEleitoral;

  /**
   * Create the panel.
   */
  public RepresentantePartidoPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {53, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    gridBagLayout.rowHeights = new int[] {15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    gridBagLayout.columnWeights =
        new double[] {0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights =
        new double[] {0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    String[] identificacaoStrings =
        {"Aveiro", "Beja", "Braga", "Bragança", "CasteloBranco", "Coimbra", "Évora", "Faro",
            "Guarda", "Leiria", "Lisboa", "Portalegre", "Porto", "Santarém", "Setúbal",
            "VianadoCastelo", "VilaReal", "Viseu", "Açores", "Madeira", "Europa", "Fora da Europa"};
    MutableComboBoxModel<String> comboModel = new DefaultComboBoxModel<>(identificacaoStrings);

    JLabel lblCirculoEleitoral = new JLabel("Círculo Eleitoral");
    GridBagConstraints gbc_lblCirculoEleitoral = new GridBagConstraints();
    gbc_lblCirculoEleitoral.fill = GridBagConstraints.HORIZONTAL;
    gbc_lblCirculoEleitoral.gridwidth = 2;
    gbc_lblCirculoEleitoral.insets = new Insets(0, 0, 5, 5);
    gbc_lblCirculoEleitoral.gridx = 1;
    gbc_lblCirculoEleitoral.gridy = 1;
    add(lblCirculoEleitoral, gbc_lblCirculoEleitoral);

    comboBoxCirculoEleitoral = new JComboBox<>(comboModel);
    GridBagConstraints gbc_comboBoxCirculoEleitoral = new GridBagConstraints();
    gbc_comboBoxCirculoEleitoral.gridwidth = 9;
    gbc_comboBoxCirculoEleitoral.insets = new Insets(0, 0, 5, 5);
    gbc_comboBoxCirculoEleitoral.fill = GridBagConstraints.HORIZONTAL;
    gbc_comboBoxCirculoEleitoral.gridx = 1;
    gbc_comboBoxCirculoEleitoral.gridy = 2;
    gbc_comboBoxCirculoEleitoral.weightx =  1.0;
    add(comboBoxCirculoEleitoral, gbc_comboBoxCirculoEleitoral);

    consultaCirculoEleitoral = new Consulta();
    GridBagConstraints gbc_consultaCirculoEleitoral = new GridBagConstraints();
    gbc_consultaCirculoEleitoral.gridheight = 6;
    gbc_consultaCirculoEleitoral.gridwidth = 12;
    gbc_consultaCirculoEleitoral.fill = GridBagConstraints.BOTH;
    gbc_consultaCirculoEleitoral.gridx = 0;
    gbc_consultaCirculoEleitoral.gridy = 4;
    gbc_consultaCirculoEleitoral.weightx = gbc_consultaCirculoEleitoral.weighty = 1.0;
    add(consultaCirculoEleitoral, gbc_consultaCirculoEleitoral);

  }

  public JComboBox<String> getComboBoxCirculoEleitoral() {
    return comboBoxCirculoEleitoral;
  }

  public Consulta getConsultaCirculoEleitoral() {
    return consultaCirculoEleitoral;
  }



}
