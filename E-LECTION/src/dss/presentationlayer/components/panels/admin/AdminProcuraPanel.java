/**
 * 
 */
package dss.presentationlayer.components.panels.admin;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;

/**
 * @author bpereira
 *
 */
public class AdminProcuraPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4943749103197755358L;
	private JTextField textFieldNomeUtilizador;
	private JTextArea textAreaDetalhes;
	private JButton btnRemover;
	private JButton btnProcurar;

	public AdminProcuraPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 80, 78, 65, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel lblNomeUtilizador = new JLabel("Nome de utilizador");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 1;
		gbc_lblNewLabel.weightx = gbc_lblNewLabel.weighty = 1.0;
		add(lblNomeUtilizador, gbc_lblNewLabel);

		textFieldNomeUtilizador = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridwidth = 2;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 1;
		gbc_textField.weightx = gbc_textField.weighty = 1.0;
		add(textFieldNomeUtilizador, gbc_textField);
		textFieldNomeUtilizador.setColumns(10);

		JScrollPane scrollPaneDetalhes = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 3;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 3;
		gbc_scrollPane.weightx = gbc_scrollPane.weighty = 1.0;
		add(scrollPaneDetalhes, gbc_scrollPane);

		textAreaDetalhes = new JTextArea();
		scrollPaneDetalhes.setViewportView(textAreaDetalhes);

		JLabel lblDetalhes = new JLabel("Detalhes");
		scrollPaneDetalhes.setColumnHeaderView(lblDetalhes);

		btnProcurar = new JButton("Procurar");
		GridBagConstraints gbc_btnProcurar = new GridBagConstraints();
		gbc_btnProcurar.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnProcurar.insets = new Insets(0, 0, 5, 5);
		gbc_btnProcurar.gridx = 1;
		gbc_btnProcurar.gridy = 4;
		gbc_btnProcurar.weightx = gbc_btnProcurar.weighty = 1.0;
		add(btnProcurar, gbc_btnProcurar);

		btnRemover = new JButton("Remover");
		GridBagConstraints gbc_btnRemover = new GridBagConstraints();
		gbc_btnRemover.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRemover.insets = new Insets(0, 0, 5, 5);
		gbc_btnRemover.gridx = 2;
		gbc_btnRemover.gridy = 4;
		gbc_btnRemover.weightx = gbc_btnRemover.weighty = 1.0;
		add(btnRemover, gbc_btnRemover);

		JButton btnLimpar = new JButton(" Limpar ");
		GridBagConstraints gbc_btnLimpar = new GridBagConstraints();
		gbc_btnLimpar.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnLimpar.insets = new Insets(0, 0, 5, 5);
		gbc_btnLimpar.gridx = 3;
		gbc_btnLimpar.gridy = 4;
		gbc_btnLimpar.weightx = gbc_btnLimpar.weighty = 1.0;
		add(btnLimpar, gbc_btnLimpar);
	}

	/**
	 * @return the textFieldNomeUtilizador
	 */
	public JTextField getTextFieldNomeUtilizador() {
		return textFieldNomeUtilizador;
	}

	/**
	 * @return the textAreaDetalhes
	 */
	public JTextArea getTextAreaDetalhes() {
		return textAreaDetalhes;
	}

	/**
	 * @return the btnRemover
	 */
	public JButton getBtnRemover() {
		return btnRemover;
	}

	/**
	 * @return the btnProcurar
	 */
	public JButton getBtnProcurar() {
		return btnProcurar;
	}

}
