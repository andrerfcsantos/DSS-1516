/**
 * 
 */
package dss.presentationlayer.components.panels.admin;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import dss.presentationlayer.components.Consulta;

/**
 * @author bpereira
 *
 */
public class AdminConsultaPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2425523379217444357L;
	/**
	 * Create the panel.
	 */
	private JButton btnRemover;
	private JButton btnLimpar;
	private Consulta consulta;

	public AdminConsultaPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 127, 121, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		consulta = new Consulta();
		GridBagConstraints gbc_consulta = new GridBagConstraints();
		gbc_consulta.gridwidth = 2;
		gbc_consulta.gridheight = 7;
		gbc_consulta.insets = new Insets(0, 0, 5, 5);
		gbc_consulta.fill = GridBagConstraints.BOTH;
		gbc_consulta.gridx = 1;
		gbc_consulta.gridy = 1;
		gbc_consulta.weightx = gbc_consulta.weighty = 1.0;
		add(consulta, gbc_consulta);

		btnRemover = new JButton("Remover");
		GridBagConstraints gbc_btnRemover = new GridBagConstraints();
		gbc_btnRemover.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRemover.insets = new Insets(0, 0, 5, 5);
		gbc_btnRemover.gridx = 1;
		gbc_btnRemover.gridy = 8;
		gbc_btnRemover.weightx = gbc_btnRemover.weighty = 1.0;
		add(btnRemover, gbc_btnRemover);

		btnLimpar = new JButton("Limpar");
		GridBagConstraints gbc_btnLimpar = new GridBagConstraints();
		gbc_btnLimpar.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnLimpar.insets = new Insets(0, 0, 5, 5);
		gbc_btnLimpar.gridx = 2;
		gbc_btnLimpar.gridy = 8;
		gbc_btnLimpar.weightx = gbc_btnLimpar.weighty = 1.0;
		add(btnLimpar, gbc_btnLimpar);

	}

	/**
	 * @return the btnRemover
	 */
	public JButton getBtnRemover() {
		return btnRemover;
	}

	/**
	 * @return the btnLimpar
	 */
	public JButton getBtnLimpar() {
		return btnLimpar;
	}

	/**
	 * @return the consulta
	 */
	public Consulta getConsulta() {
		return consulta;
	}

}
