/**
 * 
 */
package dss.presentationlayer.components.panels.admin;

import javax.swing.JPanel;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.MutableComboBoxModel;

/**
 * @author bpereira
 *
 */
public class AdminAdicionarPanel extends JPanel {


  /**
   * 
   */
  private static final long serialVersionUID = 6565676219178393651L;
  private JTextField textFieldNomeUtilizador;
  private JTextField textFieldPalavraPasse;
  private JTextField textFieldNomeProprio;
  private JButton btnAdicionar;
  private JButton btnLimpar;
  private JComboBox<String> comboBoxUtilizador;

  /**
   * Create the panel.
   */
  public AdminAdicionarPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 167, 55, 120, 75, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {0.0, 1.0, 1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights =
        new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JLabel lblNomeUtilizador = new JLabel("Nome de Utilizador");
    GridBagConstraints gbc_lblNomeUtilizador = new GridBagConstraints();
    gbc_lblNomeUtilizador.anchor = GridBagConstraints.EAST;
    gbc_lblNomeUtilizador.insets = new Insets(0, 0, 5, 5);
    gbc_lblNomeUtilizador.gridx = 1;
    gbc_lblNomeUtilizador.gridy = 2;
    gbc_lblNomeUtilizador.weightx = gbc_lblNomeUtilizador.weighty = 1.0;
    add(lblNomeUtilizador, gbc_lblNomeUtilizador);

    textFieldNomeUtilizador = new JTextField();
    GridBagConstraints gbc_textFieldNomeUtilizador = new GridBagConstraints();
    gbc_textFieldNomeUtilizador.gridwidth = 3;
    gbc_textFieldNomeUtilizador.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldNomeUtilizador.fill = GridBagConstraints.HORIZONTAL;
    gbc_textFieldNomeUtilizador.gridx = 2;
    gbc_textFieldNomeUtilizador.gridy = 2;
    gbc_textFieldNomeUtilizador.weightx =  gbc_textFieldNomeUtilizador.weighty = 1.0;
    add(textFieldNomeUtilizador, gbc_textFieldNomeUtilizador);
    textFieldNomeUtilizador.setColumns(10);

    JLabel lblPalavraPasse = new JLabel("Palavra Passe");
    GridBagConstraints gbc_lblPalavraPasse = new GridBagConstraints();
    gbc_lblPalavraPasse.anchor = GridBagConstraints.EAST;
    gbc_lblPalavraPasse.insets = new Insets(0, 0, 5, 5);
    gbc_lblPalavraPasse.gridx = 1;
    gbc_lblPalavraPasse.gridy = 3;
    gbc_lblPalavraPasse.weightx =  gbc_lblPalavraPasse.weighty = 1.0;
    add(lblPalavraPasse, gbc_lblPalavraPasse);

    textFieldPalavraPasse = new JTextField();
    GridBagConstraints gbc_textFieldPalavraPasse = new GridBagConstraints();
    gbc_textFieldPalavraPasse.gridwidth = 3;
    gbc_textFieldPalavraPasse.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldPalavraPasse.fill = GridBagConstraints.HORIZONTAL;
    gbc_textFieldPalavraPasse.gridx = 2;
    gbc_textFieldPalavraPasse.gridy = 3;
    gbc_textFieldPalavraPasse.weightx = gbc_textFieldPalavraPasse.weighty = 1.0;
    add(textFieldPalavraPasse, gbc_textFieldPalavraPasse);
    textFieldPalavraPasse.setColumns(10);

    JLabel lblNomeProprio = new JLabel("Nome Próprio");
    GridBagConstraints gbc_lblNomeProprio = new GridBagConstraints();
    gbc_lblNomeProprio.anchor = GridBagConstraints.EAST;
    gbc_lblNomeProprio.insets = new Insets(0, 0, 5, 5);
    gbc_lblNomeProprio.gridx = 1;
    gbc_lblNomeProprio.gridy = 4;
    gbc_lblNomeProprio.weightx = gbc_lblNomeProprio.weighty = 1.0;
    add(lblNomeProprio, gbc_lblNomeProprio);

    textFieldNomeProprio = new JTextField();
    GridBagConstraints gbc_textFieldNomeProprio = new GridBagConstraints();
    gbc_textFieldNomeProprio.gridwidth = 3;
    gbc_textFieldNomeProprio.insets = new Insets(0, 0, 5, 5);
    gbc_textFieldNomeProprio.fill = GridBagConstraints.HORIZONTAL;
    gbc_textFieldNomeProprio.gridx = 2;
    gbc_textFieldNomeProprio.gridy = 4;
    gbc_textFieldNomeProprio.weightx = gbc_textFieldNomeProprio.weighty = 1.0;
    add(textFieldNomeProprio, gbc_textFieldNomeProprio);
    textFieldNomeProprio.setColumns(10);

    JLabel lblTipoUtilizador = new JLabel("Tipo de utilizador");
    GridBagConstraints gbc_lblTipoUtilizador = new GridBagConstraints();
    gbc_lblTipoUtilizador.gridwidth = 3;
    gbc_lblTipoUtilizador.anchor = GridBagConstraints.WEST;
    gbc_lblTipoUtilizador.insets = new Insets(0, 0, 5, 5);
    gbc_lblTipoUtilizador.gridx = 1;
    gbc_lblTipoUtilizador.gridy = 6;
    gbc_lblTipoUtilizador.weightx = gbc_lblTipoUtilizador.weighty = 1.0;
    add(lblTipoUtilizador, gbc_lblTipoUtilizador);

    String[] utilizadoresStrings = {"Mandatario Lista Plurinominal", "Mandatario Lista Uninominal",
        "Representante do Partido", "CNE", "Assembleia Voto", "Juiz Supremo", "Juiz Constitucional"

    };
    MutableComboBoxModel<String> comboModel = new DefaultComboBoxModel<>(utilizadoresStrings);

    comboBoxUtilizador = new JComboBox<>(comboModel);
    GridBagConstraints gbc_comboBoxUtilizador = new GridBagConstraints();
    gbc_comboBoxUtilizador.gridwidth = 3;
    gbc_comboBoxUtilizador.insets = new Insets(0, 0, 5, 5);
    gbc_comboBoxUtilizador.fill = GridBagConstraints.HORIZONTAL;
    gbc_comboBoxUtilizador.gridx = 1;
    gbc_comboBoxUtilizador.gridy = 7;
    gbc_comboBoxUtilizador.weightx = gbc_comboBoxUtilizador.weighty = 1.0;
    add(comboBoxUtilizador, gbc_comboBoxUtilizador);

    btnAdicionar = new JButton("Adicionar");
    GridBagConstraints gbc_btnAdicionar = new GridBagConstraints();
    gbc_btnAdicionar.gridwidth = 2;
    gbc_btnAdicionar.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnAdicionar.insets = new Insets(0, 0, 5, 5);
    gbc_btnAdicionar.gridx = 1;
    gbc_btnAdicionar.gridy = 9;
    gbc_btnAdicionar.weightx = gbc_btnAdicionar.weighty = 1.0;
    add(btnAdicionar, gbc_btnAdicionar);

    btnLimpar = new JButton("Limpar");
    GridBagConstraints gbc_btnLimpar = new GridBagConstraints();
    gbc_btnLimpar.gridwidth = 2;
    gbc_btnLimpar.fill = GridBagConstraints.HORIZONTAL;
    gbc_btnLimpar.insets = new Insets(0, 0, 5, 5);
    gbc_btnLimpar.gridx = 3;
    gbc_btnLimpar.gridy = 9;
    gbc_btnLimpar.weightx = gbc_btnLimpar.weighty = 1.0;
    add(btnLimpar, gbc_btnLimpar);

  }

  public JTextField getTextFieldNomeUtilizador() {
    return textFieldNomeUtilizador;
  }

  public JTextField getTextFieldPalavraPasse() {
    return textFieldPalavraPasse;
  }

  public JTextField getTextFieldNomeProprio() {
    return textFieldNomeProprio;
  }

  public JButton getBtnAdicionar() {
    return btnAdicionar;
  }

  public JButton getBtnLimpar() {
    return btnLimpar;
  }

  public JComboBox<String> getComboBoxUtilizador() {
    return comboBoxUtilizador;
  }



}
