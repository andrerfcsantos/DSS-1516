package dss.presentationlayer.designpatterns;

public interface AbstractAdministradorHelper {

	public void selecionarTipoDeListaAdministrador();

	public void adicionarUtilizadorAdministrador(Object o);

	public void limparCaixasTextoAdicionarUtilizadorAdministrador();

	public void limparCaixasTextoProcurarUtilizadorAdministrador();

	public void removerUtilizadorAdministrador(String nomeUtilizador);
	
	public void editarUtilizadorAdministrador(String nomeUtilizador);

}
