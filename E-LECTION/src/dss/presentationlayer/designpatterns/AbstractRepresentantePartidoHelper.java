package dss.presentationlayer.designpatterns;

public interface AbstractRepresentantePartidoHelper {

	public void seleccionarCirculoRepresentantePartido(int index);
	
	public void listarCandidatosRepresentantePartido();

	public void seleccionarCandidatoRepresentantePartido(int index);

	public void limparAreaTextoRepresentantePartido();
}
