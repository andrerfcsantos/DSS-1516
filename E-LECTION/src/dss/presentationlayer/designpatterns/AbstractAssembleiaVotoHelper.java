package dss.presentationlayer.designpatterns;

public interface AbstractAssembleiaVotoHelper {
	
	public void abrirUrnaVirtualAssembleiaVoto();
	public void fecharUrnaVirtualAssembleiaVoto();
	public void consultarEleitorAssembleiaVoto();
	public void abrirBoletimAssembleiaVoto();
	

}
