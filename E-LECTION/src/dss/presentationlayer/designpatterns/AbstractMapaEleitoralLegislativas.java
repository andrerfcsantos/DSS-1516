package dss.presentationlayer.designpatterns;

public interface AbstractMapaEleitoralLegislativas extends AbstractMapaEleitoralHelper {

	public void setNroMandatosPorCirculo();
	
	public void listarMandatosPorPorCirculo();

	public void  listarDeputadosPorPorCirculo(int circulo);
	
	public void listarCandidatosPorPartido(int partido);

}
