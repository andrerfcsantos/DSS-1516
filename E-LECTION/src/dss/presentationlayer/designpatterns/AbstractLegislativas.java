package dss.presentationlayer.designpatterns;

public interface AbstractLegislativas extends AbstractCNE {

	public void gerarResultadosLegisLativas();

	public void guardarResultadosLegisLativas();

	public void gerarDocumentoResultadosLegisLativas();

}
