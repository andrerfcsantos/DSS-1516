package dss.presentationlayer.designpatterns;

public interface AbstractMandatarioHelper {
	
	public void adicionarMandatario();
	public void removerMandatario(int index);
	public void editarMandatario(Object o);
	public void selecionarTipoDocumento(boolean eCartaoCidadao);
	public void limparCaixasMandatario();
	
	public void listarMandatarios();

	public void seleccionarMandatarioPartido(int index);
	

}
