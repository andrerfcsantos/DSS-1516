package dss.presentationlayer.designpatterns;

public interface AbstractTribunal {

	public void listarListaCandidatosPorAprovarTribunal();

	public void listarListaCandidatosAprovadosTribunal();

	public void listarListaCandidatosRejeitadosTribunal();

	public void selecionarItemListaCandidatosPorAprovarTribunal(Object o);

	public void selecionarItemListaCandidatosAprovadosTribunal(Object o);

	public void selecionarItemListaCandidatosRejeitadosTribunal(Object o);

	public void aprovarCandidatosPorAprovarTribunal(Object o);

	public void rejeitarCandidatosPorAprovarTribunal(Object o);

}
