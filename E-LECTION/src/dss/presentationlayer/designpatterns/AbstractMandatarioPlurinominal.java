package dss.presentationlayer.designpatterns;

public interface AbstractMandatarioPlurinominal extends AbstractMandatarioHelper {

	public void moverParaCimaCandidatoPlurinominal();

	public void moverParaBaixoCandidatoPlurinominal();

}
