package dss.presentationlayer;

import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import dss.businesslayer.SGV;
import dss.presentationlayer.components.LogoComponent;
import dss.presentationlayer.components.panels.login.Login;
import dss.presentationlayer.components.panels.mandatario.MandatarioPlurinominalPanel;
import dss.presentationlayer.components.separators.AdminSeparator;
import dss.presentationlayer.components.separators.AssembleiaVotoSeparator;
import dss.presentationlayer.components.separators.CNESeparator;
import dss.presentationlayer.components.separators.JuizConstitucionalSeparator;
import dss.presentationlayer.components.separators.JuizSupremoSeparator;
import dss.presentationlayer.components.separators.MandatarioUninominalSeparator;
import dss.presentationlayer.components.separators.RepresentantePartidoSeparator;

public class Main {

	private JFrame frame;

	static String LOGIN = "LOGIN";
	static String ASSEM = "ASSEM";
	static String ADMIN = "ADMIN";
	static String CNESE = "CNESE";
	static String JCONS = "JCONS";
	static String JSUPS = "JSUPS";
	static String MANPLURS = "MANPLURS";
	static String MANUNIS = "MANUNIS";
	static String REPPARTS = "REPPARTS";

	AdminSeparator adminSeparator;
	AssembleiaVotoSeparator assembleiaVotoSeparator;
	CNESeparator cneSeparator;
	Login login;
	JuizConstitucionalSeparator juizConstitucionalSeparator;
	JuizSupremoSeparator juizSupremoSeparator;
	MandatarioPlurinominalPanel mandatarioPlurinominalSeparator;
	MandatarioUninominalSeparator mandatarioUninominalSeparator;
	RepresentantePartidoSeparator representantePartidoSeparator;

	SGV sistema = new SGV();

	private int tipoLista = 0;

	protected int key = 0;
	private JButton btnTestas;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1000, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);

		JPanel panelLogo = new JPanel();

		GridBagConstraints gbc_panelLogo = new GridBagConstraints();
		gbc_panelLogo.insets = new Insets(0, 0, 0, 5);
		gbc_panelLogo.fill = GridBagConstraints.BOTH;
		gbc_panelLogo.gridx = 0;
		gbc_panelLogo.gridy = 0;
		frame.getContentPane().add(panelLogo, gbc_panelLogo);
		GridBagLayout gbl_panelLogo = new GridBagLayout();
		gbl_panelLogo.columnWidths = new int[] { 0, 0, 0 };
		gbl_panelLogo.rowHeights = new int[] { 0, 0, 0 };
		gbl_panelLogo.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelLogo.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		panelLogo.setLayout(gbl_panelLogo);

		LogoComponent logo = null;
		try {
			logo = new LogoComponent();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JLabel label = new JLabel(logo);

		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 0);
		gbc_label.gridx = 1;
		gbc_label.gridy = 0;
		panelLogo.add(label, gbc_label);
		
		btnTestas = new JButton("Testar");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 1;
		panelLogo.add(btnNewButton, gbc_btnNewButton);

		JPanel panelContents = new JPanel();
		GridBagConstraints gbc_panelContents = new GridBagConstraints();
		gbc_panelContents.gridwidth = 3;
		gbc_panelContents.fill = GridBagConstraints.BOTH;
		gbc_panelContents.gridx = 1;
		gbc_panelContents.gridy = 0;
		frame.getContentPane().add(panelContents, gbc_panelContents);

		login = new Login();
		adminSeparator = new AdminSeparator();
		assembleiaVotoSeparator = new AssembleiaVotoSeparator();
		cneSeparator = new CNESeparator();
		juizConstitucionalSeparator = new JuizConstitucionalSeparator();
		juizSupremoSeparator = new JuizSupremoSeparator();
		mandatarioPlurinominalSeparator = new MandatarioPlurinominalPanel();
		mandatarioUninominalSeparator = new MandatarioUninominalSeparator();
		representantePartidoSeparator = new RepresentantePartidoSeparator();
		CardLayout card = new CardLayout();
		panelContents.setLayout(card);

		panelContents.setLayout(card);
		panelContents.add(login, LOGIN);
		
		
		panelContents.add(adminSeparator, ADMIN);
		panelContents.add(assembleiaVotoSeparator, ASSEM);
		panelContents.add(cneSeparator, CNESE);
		panelContents.add(juizConstitucionalSeparator, JCONS);
		panelContents.add(juizSupremoSeparator, JSUPS);
		panelContents.add(representantePartidoSeparator, REPPARTS);

		card.show(panelContents, LOGIN);

		btnTestas.addActionListener(new ActionListener() {

			

			

			@Override
			public void actionPerformed(ActionEvent e) {
				switch (key) {
				case 0:
					card.show(panelContents, LOGIN);
					key++;
					break;

				case 1:
					card.show(panelContents, ASSEM);
					key++;
					break;
				case 2:
					card.show(panelContents, CNESE);
					key++;
					break;
				case 3:
					card.show(panelContents, JCONS);
					key++;
					break;
				case 4:
					card.show(panelContents, JSUPS);
					key++;
					break;
				case 5:
					card.show(panelContents, REPPARTS);
					key = 0;
					break;
				

				default:
					break;
				}

			}
		});

	}

	/**
	 * Administrador
	 */
	public void selecionarTipoDeListaAdministrador() {
		adminSeparator.getAdminAdicionarPanel().getComboBoxUtilizador().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				tipoLista = adminSeparator.getAdminAdicionarPanel().getComboBoxUtilizador().getSelectedIndex();
			}
		});

	}

	public void adicionarUtilizadorAdministrador() {
		String nomeProprio = adminSeparator.getAdminAdicionarPanel().getTextFieldNomeProprio().getText();
		String nomeUtilizador = adminSeparator.getAdminAdicionarPanel().getTextFieldNomeUtilizador().getText();
		String palavraPasse = adminSeparator.getAdminAdicionarPanel().getTextFieldPalavraPasse().getText();

	}

	public void limparCaixasTextoAdicionarUtilizadorAdministrador() {
		adminSeparator.getAdminAdicionarPanel().getTextFieldNomeProprio().setText("");
		adminSeparator.getAdminAdicionarPanel().getTextFieldNomeUtilizador().setText("");
		adminSeparator.getAdminAdicionarPanel().getTextFieldPalavraPasse().setText("");

		// tipo de lista = 0
		tipoLista = 0;
	}

	public void limparCaixasTextoProcurarUtilizadorAdministrador() {

	}

	public void removerUtilizadorAdministrador() {
		// TODO Auto-generated method stub

	}

	public void editarUtilizadorAdministrador() {

	}

	public void entraLogin() {
		login.getBtnOK().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String userName = login.getTextUsername().getText();
				char[] auxPass = new char[login.getTextPassword().getPassword().length];
				String password = String.valueOf(auxPass);

			}
		});

	}

}
